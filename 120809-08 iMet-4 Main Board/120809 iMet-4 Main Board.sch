<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.01" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="13" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="11" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="17" fill="1" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="26" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="InterMet">
<packages>
<package name="IMET3_CARDEDGE">
<wire x1="6.3" y1="1" x2="6.3" y2="6" width="0" layer="20"/>
<smd name="1" x="-3.81" y="3.5" dx="5" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="2" x="-1.27" y="3.5" dx="5" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="3" x="1.27" y="3.5" dx="5" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="4" x="3.81" y="3.125" dx="5.75" dy="1.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="5" x="-3.81" y="3.5" dx="5" dy="1.5" layer="16" rot="R90" stop="no" cream="no"/>
<smd name="6" x="-1.27" y="3.5" dx="5" dy="1.5" layer="16" rot="R90" stop="no" cream="no"/>
<smd name="7" x="1.27" y="3.5" dx="5" dy="1.5" layer="16" rot="R90" stop="no" cream="no"/>
<smd name="8" x="3.81" y="3.5" dx="5" dy="1.5" layer="16" rot="R90" stop="no" cream="no"/>
<text x="-3.81" y="7.62" size="1.27" layer="21" font="vector" ratio="15" align="center">1</text>
<text x="-1.27" y="7.62" size="1.27" layer="21" font="vector" ratio="15" align="center">2</text>
<text x="1.27" y="7.62" size="1.27" layer="21" font="vector" ratio="15" align="center">3</text>
<text x="3.81" y="7.62" size="1.27" layer="21" font="vector" ratio="15" align="center">4</text>
<wire x1="10.5" y1="0" x2="10.5" y2="6" width="0" layer="20"/>
<wire x1="7.5" y1="7" x2="9.5" y2="7" width="0" layer="20"/>
<wire x1="6.3" y1="6" x2="7.5" y2="7" width="0" layer="20" curve="-90"/>
<wire x1="9.5" y1="7" x2="10.5" y2="6" width="0" layer="20" curve="-90"/>
<wire x1="5.5" y1="0" x2="6.3" y2="1" width="0" layer="20"/>
<wire x1="-10.5" y1="0" x2="-10.5" y2="6" width="0" layer="20"/>
<wire x1="-6.3" y1="1" x2="-6.3" y2="6" width="0" layer="20"/>
<wire x1="-9.5" y1="7" x2="-7.5" y2="7" width="0" layer="20"/>
<wire x1="-10.5" y1="6" x2="-9.5" y2="7" width="0" layer="20" curve="-90"/>
<wire x1="-7.5" y1="7" x2="-6.3" y2="6" width="0" layer="20" curve="-90"/>
<wire x1="-6.3" y1="1" x2="-5.5" y2="0" width="0" layer="20"/>
<wire x1="-5.5" y1="0" x2="5.5" y2="0" width="0" layer="20"/>
<text x="-3.81" y="7.62" size="1.27" layer="22" font="vector" ratio="15" rot="SMR0" align="center">5</text>
<text x="-1.27" y="7.62" size="1.27" layer="22" font="vector" ratio="15" rot="SMR0" align="center">6</text>
<text x="1.27" y="7.62" size="1.27" layer="22" font="vector" ratio="15" rot="SMR0" align="center">7</text>
<text x="3.81" y="7.62" size="1.27" layer="22" font="vector" ratio="15" rot="SMR0" align="center">8</text>
<rectangle x1="-6.35" y1="0" x2="6.35" y2="6.35" layer="29"/>
<rectangle x1="-6.35" y1="0" x2="6.35" y2="6.35" layer="30"/>
</package>
<package name="WHIP">
<smd name="ANT" x="0" y="0" dx="4" dy="2.5" layer="1"/>
</package>
<package name="JST_12FPZ-SM-TF">
<smd name="1" x="5.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="4.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="3.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="2.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="1.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="0.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="-0.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="-1.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="9" x="-2.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="10" x="-3.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="11" x="-4.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="12" x="-5.5" y="-4.1" dx="2.5" dy="0.6" layer="1" rot="R90"/>
<smd name="M1" x="8.25" y="0" dx="2.5" dy="1.7" layer="1"/>
<smd name="M2" x="-8.25" y="0" dx="2.5" dy="1.7" layer="1"/>
<text x="7" y="-4" size="1.27" layer="21" font="vector" ratio="15" align="center">1</text>
<text x="0" y="0" size="1.27" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<text x="-9" y="-4" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="-7" y1="3.65" x2="7" y2="3.65" width="0.127" layer="51"/>
<wire x1="7" y1="3.65" x2="7" y2="-2" width="0.127" layer="51"/>
<wire x1="7" y1="-2" x2="-7" y2="-2" width="0.127" layer="51"/>
<wire x1="-7" y1="-2" x2="-7" y2="3.65" width="0.127" layer="51"/>
</package>
<package name="IQ_PROBE_12P">
<text x="1.56" y="5.4" size="1.27" layer="25">&gt;NAME</text>
<text x="1.61" y="3.4" size="1.27" layer="27">&gt;VALUE</text>
<smd name="1" x="0" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="2" x="1" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="3" x="2" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="4" x="3" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="5" x="4" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="6" x="5" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="7" x="6" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="8" x="7" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="9" x="8" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="10" x="9" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="11" x="10" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
<smd name="12" x="11" y="0" dx="6" dy="0.7112" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="IMET3_EDGE">
<pin name="V+" x="-15.24" y="5.08" length="middle"/>
<pin name="RX" x="-15.24" y="2.54" length="middle"/>
<pin name="TX" x="-15.24" y="0" length="middle"/>
<pin name="GND" x="-15.24" y="-2.54" length="middle"/>
<text x="0" y="10.16" size="1.778" layer="95" rot="MR0" align="center">&gt;NAME</text>
<text x="0" y="-7.62" size="1.778" layer="96" rot="MR0" align="center">&gt;VALUE</text>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="SWCLK" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="GPIO" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="SWDIO" x="15.24" y="5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="ANTENNA">
<wire x1="-1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" align="center">&gt;NAME</text>
<pin name="ANTENNA" x="0" y="0" visible="off" length="middle" direction="in" rot="R90"/>
<wire x1="0" y1="5.08" x2="0" y2="6.35" width="0.254" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
</symbol>
<symbol name="FRAME_8.5X11">
<frame x1="0" y1="0" x2="279.4" y2="215.9" columns="6" rows="5" layer="94" border-bottom="no"/>
<wire x1="172.72" y1="0" x2="243.84" y2="0" width="0.1016" layer="94"/>
<wire x1="275.59" y1="15.24" x2="260.35" y2="15.24" width="0.1016" layer="94"/>
<wire x1="172.72" y1="0" x2="172.72" y2="5.08" width="0.1016" layer="94"/>
<wire x1="172.72" y1="5.08" x2="243.84" y2="5.08" width="0.1016" layer="94"/>
<wire x1="172.72" y1="5.08" x2="172.72" y2="15.24" width="0.1016" layer="94"/>
<wire x1="275.59" y1="15.24" x2="275.59" y2="5.08" width="0.1016" layer="94"/>
<wire x1="243.84" y1="5.08" x2="243.84" y2="0" width="0.1016" layer="94"/>
<wire x1="243.84" y1="5.08" x2="260.35" y2="5.08" width="0.1016" layer="94"/>
<wire x1="243.84" y1="0" x2="275.59" y2="0" width="0.1016" layer="94"/>
<wire x1="260.35" y1="15.24" x2="260.35" y2="5.08" width="0.1016" layer="94"/>
<wire x1="260.35" y1="15.24" x2="172.72" y2="15.24" width="0.1016" layer="94"/>
<wire x1="260.35" y1="5.08" x2="275.59" y2="5.08" width="0.1016" layer="94"/>
<wire x1="275.59" y1="5.08" x2="275.59" y2="0" width="0.1016" layer="94"/>
<wire x1="172.72" y1="15.24" x2="172.72" y2="22.86" width="0.1016" layer="94"/>
<wire x1="275.59" y1="35.56" x2="172.72" y2="35.56" width="0.1016" layer="94"/>
<wire x1="275.59" y1="35.56" x2="275.59" y2="22.86" width="0.1016" layer="94"/>
<wire x1="172.72" y1="22.86" x2="275.59" y2="22.86" width="0.1016" layer="94"/>
<wire x1="172.72" y1="22.86" x2="172.72" y2="35.56" width="0.1016" layer="94"/>
<wire x1="275.59" y1="22.86" x2="275.59" y2="15.24" width="0.1016" layer="94"/>
<text x="173.99" y="1.27" size="2.54" layer="94">Date:</text>
<text x="185.42" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="245.11" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="259.08" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="261.62" y="11.43" size="2.54" layer="94">REV:</text>
<text x="173.99" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="173.99" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="190.5" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<rectangle x1="234.4039" y1="24.9047" x2="234.7087" y2="24.9809" layer="200"/>
<rectangle x1="234.4039" y1="24.9809" x2="234.7849" y2="25.0571" layer="200"/>
<rectangle x1="234.4801" y1="25.0571" x2="234.7849" y2="25.1333" layer="200"/>
<rectangle x1="214.9729" y1="25.1333" x2="215.5063" y2="25.2095" layer="200"/>
<rectangle x1="234.6325" y1="25.1333" x2="234.8611" y2="25.2095" layer="200"/>
<rectangle x1="214.5157" y1="25.2095" x2="215.9635" y2="25.2857" layer="200"/>
<rectangle x1="219.7735" y1="25.2095" x2="220.9165" y2="25.2857" layer="200"/>
<rectangle x1="220.9927" y1="25.2095" x2="221.5261" y2="25.2857" layer="200"/>
<rectangle x1="221.6023" y1="25.2095" x2="221.9833" y2="25.2857" layer="200"/>
<rectangle x1="222.2881" y1="25.2095" x2="222.7453" y2="25.2857" layer="200"/>
<rectangle x1="222.8977" y1="25.2095" x2="223.5073" y2="25.2857" layer="200"/>
<rectangle x1="223.6597" y1="25.2095" x2="224.1169" y2="25.2857" layer="200"/>
<rectangle x1="224.1931" y1="25.2095" x2="224.7265" y2="25.2857" layer="200"/>
<rectangle x1="224.8027" y1="25.2095" x2="225.1075" y2="25.2857" layer="200"/>
<rectangle x1="225.2599" y1="25.2095" x2="225.4885" y2="25.2857" layer="200"/>
<rectangle x1="225.7171" y1="25.2095" x2="226.0219" y2="25.2857" layer="200"/>
<rectangle x1="226.0981" y1="25.2095" x2="226.6315" y2="25.2857" layer="200"/>
<rectangle x1="226.8601" y1="25.2095" x2="227.3173" y2="25.2857" layer="200"/>
<rectangle x1="227.6221" y1="25.2095" x2="228.1555" y2="25.2857" layer="200"/>
<rectangle x1="228.2317" y1="25.2095" x2="228.6889" y2="25.2857" layer="200"/>
<rectangle x1="228.7651" y1="25.2095" x2="229.0699" y2="25.2857" layer="200"/>
<rectangle x1="229.2223" y1="25.2095" x2="229.4509" y2="25.2857" layer="200"/>
<rectangle x1="229.5271" y1="25.2095" x2="230.0605" y2="25.2857" layer="200"/>
<rectangle x1="230.3653" y1="25.2095" x2="230.8987" y2="25.2857" layer="200"/>
<rectangle x1="231.2797" y1="25.2095" x2="231.8893" y2="25.2857" layer="200"/>
<rectangle x1="232.0417" y1="25.2095" x2="232.4989" y2="25.2857" layer="200"/>
<rectangle x1="232.8037" y1="25.2095" x2="233.1085" y2="25.2857" layer="200"/>
<rectangle x1="233.7181" y1="25.2095" x2="234.2515" y2="25.2857" layer="200"/>
<rectangle x1="234.6325" y1="25.2095" x2="234.8611" y2="25.2857" layer="200"/>
<rectangle x1="235.2421" y1="25.2095" x2="235.6993" y2="25.2857" layer="200"/>
<rectangle x1="236.0803" y1="25.2095" x2="236.3851" y2="25.2857" layer="200"/>
<rectangle x1="236.7661" y1="25.2095" x2="237.1471" y2="25.2857" layer="200"/>
<rectangle x1="237.3757" y1="25.2095" x2="239.5093" y2="25.2857" layer="200"/>
<rectangle x1="214.2871" y1="25.2857" x2="216.2683" y2="25.3619" layer="200"/>
<rectangle x1="219.7735" y1="25.2857" x2="220.9165" y2="25.3619" layer="200"/>
<rectangle x1="220.9927" y1="25.2857" x2="222.0595" y2="25.3619" layer="200"/>
<rectangle x1="222.1357" y1="25.2857" x2="223.5073" y2="25.3619" layer="200"/>
<rectangle x1="223.6597" y1="25.2857" x2="224.1169" y2="25.3619" layer="200"/>
<rectangle x1="224.1931" y1="25.2857" x2="225.5647" y2="25.3619" layer="200"/>
<rectangle x1="225.6409" y1="25.2857" x2="226.6315" y2="25.3619" layer="200"/>
<rectangle x1="226.7077" y1="25.2857" x2="227.4697" y2="25.3619" layer="200"/>
<rectangle x1="227.6221" y1="25.2857" x2="228.1555" y2="25.3619" layer="200"/>
<rectangle x1="228.2317" y1="25.2857" x2="230.0605" y2="25.3619" layer="200"/>
<rectangle x1="230.3653" y1="25.2857" x2="230.8987" y2="25.3619" layer="200"/>
<rectangle x1="230.9749" y1="25.2857" x2="231.2035" y2="25.3619" layer="200"/>
<rectangle x1="231.2797" y1="25.2857" x2="232.6513" y2="25.3619" layer="200"/>
<rectangle x1="232.7275" y1="25.2857" x2="233.2609" y2="25.3619" layer="200"/>
<rectangle x1="233.6419" y1="25.2857" x2="234.3277" y2="25.3619" layer="200"/>
<rectangle x1="234.6325" y1="25.2857" x2="234.8611" y2="25.3619" layer="200"/>
<rectangle x1="235.1659" y1="25.2857" x2="235.8517" y2="25.3619" layer="200"/>
<rectangle x1="236.0041" y1="25.2857" x2="236.5375" y2="25.3619" layer="200"/>
<rectangle x1="236.6137" y1="25.2857" x2="237.2995" y2="25.3619" layer="200"/>
<rectangle x1="237.3757" y1="25.2857" x2="239.5855" y2="25.3619" layer="200"/>
<rectangle x1="214.0585" y1="25.3619" x2="216.4207" y2="25.4381" layer="200"/>
<rectangle x1="219.7735" y1="25.3619" x2="220.3831" y2="25.4381" layer="200"/>
<rectangle x1="220.4593" y1="25.3619" x2="220.8403" y2="25.4381" layer="200"/>
<rectangle x1="221.0689" y1="25.3619" x2="221.4499" y2="25.4381" layer="200"/>
<rectangle x1="221.5261" y1="25.3619" x2="222.0595" y2="25.4381" layer="200"/>
<rectangle x1="222.1357" y1="25.3619" x2="223.4311" y2="25.4381" layer="200"/>
<rectangle x1="223.6597" y1="25.3619" x2="224.0407" y2="25.4381" layer="200"/>
<rectangle x1="224.2693" y1="25.3619" x2="224.6503" y2="25.4381" layer="200"/>
<rectangle x1="224.7265" y1="25.3619" x2="225.5647" y2="25.4381" layer="200"/>
<rectangle x1="225.6409" y1="25.3619" x2="226.5553" y2="25.4381" layer="200"/>
<rectangle x1="226.6315" y1="25.3619" x2="227.0125" y2="25.4381" layer="200"/>
<rectangle x1="227.2411" y1="25.3619" x2="227.5459" y2="25.4381" layer="200"/>
<rectangle x1="227.6983" y1="25.3619" x2="228.0793" y2="25.4381" layer="200"/>
<rectangle x1="228.2317" y1="25.3619" x2="230.0605" y2="25.4381" layer="200"/>
<rectangle x1="230.3653" y1="25.3619" x2="230.8225" y2="25.4381" layer="200"/>
<rectangle x1="230.9749" y1="25.3619" x2="231.2035" y2="25.4381" layer="200"/>
<rectangle x1="231.2797" y1="25.3619" x2="233.2609" y2="25.4381" layer="200"/>
<rectangle x1="233.6419" y1="25.3619" x2="233.8705" y2="25.4381" layer="200"/>
<rectangle x1="234.1753" y1="25.3619" x2="234.4039" y2="25.4381" layer="200"/>
<rectangle x1="234.6325" y1="25.3619" x2="234.9373" y2="25.4381" layer="200"/>
<rectangle x1="235.1659" y1="25.3619" x2="235.4707" y2="25.4381" layer="200"/>
<rectangle x1="235.6231" y1="25.3619" x2="235.9279" y2="25.4381" layer="200"/>
<rectangle x1="236.0041" y1="25.3619" x2="237.3757" y2="25.4381" layer="200"/>
<rectangle x1="237.4519" y1="25.3619" x2="237.8329" y2="25.4381" layer="200"/>
<rectangle x1="237.9853" y1="25.3619" x2="238.3663" y2="25.4381" layer="200"/>
<rectangle x1="238.5187" y1="25.3619" x2="239.2045" y2="25.4381" layer="200"/>
<rectangle x1="239.3569" y1="25.3619" x2="239.6617" y2="25.4381" layer="200"/>
<rectangle x1="213.9061" y1="25.4381" x2="216.5731" y2="25.5143" layer="200"/>
<rectangle x1="219.9259" y1="25.4381" x2="220.3069" y2="25.5143" layer="200"/>
<rectangle x1="220.5355" y1="25.4381" x2="220.8403" y2="25.5143" layer="200"/>
<rectangle x1="221.0689" y1="25.4381" x2="221.3737" y2="25.5143" layer="200"/>
<rectangle x1="221.5261" y1="25.4381" x2="221.8309" y2="25.5143" layer="200"/>
<rectangle x1="222.0595" y1="25.4381" x2="222.4405" y2="25.5143" layer="200"/>
<rectangle x1="222.7453" y1="25.4381" x2="222.9739" y2="25.5143" layer="200"/>
<rectangle x1="223.0501" y1="25.4381" x2="223.3549" y2="25.5143" layer="200"/>
<rectangle x1="223.7359" y1="25.4381" x2="224.0407" y2="25.5143" layer="200"/>
<rectangle x1="224.3455" y1="25.4381" x2="224.5741" y2="25.5143" layer="200"/>
<rectangle x1="224.7265" y1="25.4381" x2="225.0313" y2="25.5143" layer="200"/>
<rectangle x1="225.1075" y1="25.4381" x2="225.4123" y2="25.5143" layer="200"/>
<rectangle x1="225.6409" y1="25.4381" x2="225.8695" y2="25.5143" layer="200"/>
<rectangle x1="226.1743" y1="25.4381" x2="226.4791" y2="25.5143" layer="200"/>
<rectangle x1="226.6315" y1="25.4381" x2="226.9363" y2="25.5143" layer="200"/>
<rectangle x1="227.2411" y1="25.4381" x2="227.6221" y2="25.5143" layer="200"/>
<rectangle x1="227.6983" y1="25.4381" x2="228.0031" y2="25.5143" layer="200"/>
<rectangle x1="228.3079" y1="25.4381" x2="228.6127" y2="25.5143" layer="200"/>
<rectangle x1="228.6889" y1="25.4381" x2="228.9937" y2="25.5143" layer="200"/>
<rectangle x1="229.0699" y1="25.4381" x2="229.4509" y2="25.5143" layer="200"/>
<rectangle x1="229.6033" y1="25.4381" x2="229.9081" y2="25.5143" layer="200"/>
<rectangle x1="230.5177" y1="25.4381" x2="230.6701" y2="25.5143" layer="200"/>
<rectangle x1="230.9749" y1="25.4381" x2="231.2797" y2="25.5143" layer="200"/>
<rectangle x1="231.4321" y1="25.4381" x2="231.7369" y2="25.5143" layer="200"/>
<rectangle x1="231.8131" y1="25.4381" x2="232.1941" y2="25.5143" layer="200"/>
<rectangle x1="232.5751" y1="25.4381" x2="233.0323" y2="25.5143" layer="200"/>
<rectangle x1="233.6419" y1="25.4381" x2="233.8705" y2="25.5143" layer="200"/>
<rectangle x1="234.1753" y1="25.4381" x2="234.4801" y2="25.5143" layer="200"/>
<rectangle x1="234.6325" y1="25.4381" x2="234.9373" y2="25.5143" layer="200"/>
<rectangle x1="235.1659" y1="25.4381" x2="235.3945" y2="25.5143" layer="200"/>
<rectangle x1="235.6993" y1="25.4381" x2="235.9279" y2="25.5143" layer="200"/>
<rectangle x1="236.0041" y1="25.4381" x2="236.3089" y2="25.5143" layer="200"/>
<rectangle x1="236.5375" y1="25.4381" x2="236.9185" y2="25.5143" layer="200"/>
<rectangle x1="237.2233" y1="25.4381" x2="237.3757" y2="25.5143" layer="200"/>
<rectangle x1="237.4519" y1="25.4381" x2="237.7567" y2="25.5143" layer="200"/>
<rectangle x1="237.9853" y1="25.4381" x2="238.2901" y2="25.5143" layer="200"/>
<rectangle x1="238.5949" y1="25.4381" x2="238.8235" y2="25.5143" layer="200"/>
<rectangle x1="238.8997" y1="25.4381" x2="239.1283" y2="25.5143" layer="200"/>
<rectangle x1="239.3569" y1="25.4381" x2="239.6617" y2="25.5143" layer="200"/>
<rectangle x1="213.7537" y1="25.5143" x2="216.7255" y2="25.5905" layer="200"/>
<rectangle x1="219.9259" y1="25.5143" x2="220.3069" y2="25.5905" layer="200"/>
<rectangle x1="220.5355" y1="25.5143" x2="220.8403" y2="25.5905" layer="200"/>
<rectangle x1="221.0689" y1="25.5143" x2="221.3737" y2="25.5905" layer="200"/>
<rectangle x1="221.5261" y1="25.5143" x2="221.8309" y2="25.5905" layer="200"/>
<rectangle x1="222.0595" y1="25.5143" x2="222.3643" y2="25.5905" layer="200"/>
<rectangle x1="223.0501" y1="25.5143" x2="223.3549" y2="25.5905" layer="200"/>
<rectangle x1="223.7359" y1="25.5143" x2="224.0407" y2="25.5905" layer="200"/>
<rectangle x1="224.3455" y1="25.5143" x2="224.5741" y2="25.5905" layer="200"/>
<rectangle x1="224.7265" y1="25.5143" x2="225.0313" y2="25.5905" layer="200"/>
<rectangle x1="225.1075" y1="25.5143" x2="225.4123" y2="25.5905" layer="200"/>
<rectangle x1="225.6409" y1="25.5143" x2="225.9457" y2="25.5905" layer="200"/>
<rectangle x1="226.1743" y1="25.5143" x2="226.4791" y2="25.5905" layer="200"/>
<rectangle x1="226.5553" y1="25.5143" x2="226.9363" y2="25.5905" layer="200"/>
<rectangle x1="227.3173" y1="25.5143" x2="227.6221" y2="25.5905" layer="200"/>
<rectangle x1="227.6983" y1="25.5143" x2="228.0031" y2="25.5905" layer="200"/>
<rectangle x1="228.3079" y1="25.5143" x2="228.6127" y2="25.5905" layer="200"/>
<rectangle x1="228.6889" y1="25.5143" x2="228.9937" y2="25.5905" layer="200"/>
<rectangle x1="229.1461" y1="25.5143" x2="229.4509" y2="25.5905" layer="200"/>
<rectangle x1="229.6033" y1="25.5143" x2="229.9081" y2="25.5905" layer="200"/>
<rectangle x1="230.5177" y1="25.5143" x2="230.7463" y2="25.5905" layer="200"/>
<rectangle x1="230.8987" y1="25.5143" x2="231.2797" y2="25.5905" layer="200"/>
<rectangle x1="231.4321" y1="25.5143" x2="231.7369" y2="25.5905" layer="200"/>
<rectangle x1="231.8131" y1="25.5143" x2="232.1941" y2="25.5905" layer="200"/>
<rectangle x1="232.7275" y1="25.5143" x2="233.0323" y2="25.5905" layer="200"/>
<rectangle x1="233.5657" y1="25.5143" x2="233.7943" y2="25.5905" layer="200"/>
<rectangle x1="234.1753" y1="25.5143" x2="234.4801" y2="25.5905" layer="200"/>
<rectangle x1="234.5563" y1="25.5143" x2="235.0135" y2="25.5905" layer="200"/>
<rectangle x1="235.1659" y1="25.5143" x2="235.3945" y2="25.5905" layer="200"/>
<rectangle x1="235.5469" y1="25.5143" x2="235.9279" y2="25.5905" layer="200"/>
<rectangle x1="236.0041" y1="25.5143" x2="236.3089" y2="25.5905" layer="200"/>
<rectangle x1="236.4613" y1="25.5143" x2="236.8423" y2="25.5905" layer="200"/>
<rectangle x1="237.4519" y1="25.5143" x2="237.7567" y2="25.5905" layer="200"/>
<rectangle x1="237.9853" y1="25.5143" x2="238.2901" y2="25.5905" layer="200"/>
<rectangle x1="238.5187" y1="25.5143" x2="238.8235" y2="25.5905" layer="200"/>
<rectangle x1="238.8997" y1="25.5143" x2="239.1283" y2="25.5905" layer="200"/>
<rectangle x1="239.2807" y1="25.5143" x2="239.6617" y2="25.5905" layer="200"/>
<rectangle x1="213.6775" y1="25.5905" x2="216.8779" y2="25.6667" layer="200"/>
<rectangle x1="219.9259" y1="25.5905" x2="220.3069" y2="25.6667" layer="200"/>
<rectangle x1="220.5355" y1="25.5905" x2="220.8403" y2="25.6667" layer="200"/>
<rectangle x1="221.0689" y1="25.5905" x2="221.3737" y2="25.6667" layer="200"/>
<rectangle x1="221.5261" y1="25.5905" x2="221.8309" y2="25.6667" layer="200"/>
<rectangle x1="221.9833" y1="25.5905" x2="222.3643" y2="25.6667" layer="200"/>
<rectangle x1="223.0501" y1="25.5905" x2="223.3549" y2="25.6667" layer="200"/>
<rectangle x1="223.7359" y1="25.5905" x2="224.0407" y2="25.6667" layer="200"/>
<rectangle x1="224.3455" y1="25.5905" x2="224.5741" y2="25.6667" layer="200"/>
<rectangle x1="224.8027" y1="25.5905" x2="225.4123" y2="25.6667" layer="200"/>
<rectangle x1="225.6409" y1="25.5905" x2="225.9457" y2="25.6667" layer="200"/>
<rectangle x1="226.1743" y1="25.5905" x2="226.4791" y2="25.6667" layer="200"/>
<rectangle x1="226.5553" y1="25.5905" x2="226.9363" y2="25.6667" layer="200"/>
<rectangle x1="227.3173" y1="25.5905" x2="227.6221" y2="25.6667" layer="200"/>
<rectangle x1="227.6983" y1="25.5905" x2="228.0031" y2="25.6667" layer="200"/>
<rectangle x1="228.3079" y1="25.5905" x2="228.6127" y2="25.6667" layer="200"/>
<rectangle x1="228.8413" y1="25.5905" x2="229.4509" y2="25.6667" layer="200"/>
<rectangle x1="229.6033" y1="25.5905" x2="229.9081" y2="25.6667" layer="200"/>
<rectangle x1="230.5177" y1="25.5905" x2="230.7463" y2="25.6667" layer="200"/>
<rectangle x1="230.8987" y1="25.5905" x2="231.2797" y2="25.6667" layer="200"/>
<rectangle x1="231.4321" y1="25.5905" x2="231.7369" y2="25.6667" layer="200"/>
<rectangle x1="231.8131" y1="25.5905" x2="232.1179" y2="25.6667" layer="200"/>
<rectangle x1="232.7275" y1="25.5905" x2="233.0323" y2="25.6667" layer="200"/>
<rectangle x1="233.6419" y1="25.5905" x2="233.7943" y2="25.6667" layer="200"/>
<rectangle x1="234.1753" y1="25.5905" x2="234.4801" y2="25.6667" layer="200"/>
<rectangle x1="234.5563" y1="25.5905" x2="235.0135" y2="25.6667" layer="200"/>
<rectangle x1="235.3945" y1="25.5905" x2="235.9279" y2="25.6667" layer="200"/>
<rectangle x1="236.0041" y1="25.5905" x2="236.3089" y2="25.6667" layer="200"/>
<rectangle x1="236.4613" y1="25.5905" x2="236.7661" y2="25.6667" layer="200"/>
<rectangle x1="237.4519" y1="25.5905" x2="237.7567" y2="25.6667" layer="200"/>
<rectangle x1="237.9853" y1="25.5905" x2="238.2901" y2="25.6667" layer="200"/>
<rectangle x1="238.5187" y1="25.5905" x2="238.8235" y2="25.6667" layer="200"/>
<rectangle x1="239.1283" y1="25.5905" x2="239.6617" y2="25.6667" layer="200"/>
<rectangle x1="213.5251" y1="25.6667" x2="216.9541" y2="25.7429" layer="200"/>
<rectangle x1="219.9259" y1="25.6667" x2="220.3069" y2="25.7429" layer="200"/>
<rectangle x1="220.5355" y1="25.6667" x2="220.8403" y2="25.7429" layer="200"/>
<rectangle x1="221.0689" y1="25.6667" x2="221.3737" y2="25.7429" layer="200"/>
<rectangle x1="221.5261" y1="25.6667" x2="221.8309" y2="25.7429" layer="200"/>
<rectangle x1="221.9833" y1="25.6667" x2="222.2881" y2="25.7429" layer="200"/>
<rectangle x1="223.0501" y1="25.6667" x2="223.3549" y2="25.7429" layer="200"/>
<rectangle x1="223.7359" y1="25.6667" x2="224.0407" y2="25.7429" layer="200"/>
<rectangle x1="224.3455" y1="25.6667" x2="224.5741" y2="25.7429" layer="200"/>
<rectangle x1="224.9551" y1="25.6667" x2="225.4123" y2="25.7429" layer="200"/>
<rectangle x1="225.6409" y1="25.6667" x2="225.9457" y2="25.7429" layer="200"/>
<rectangle x1="226.1743" y1="25.6667" x2="226.4791" y2="25.7429" layer="200"/>
<rectangle x1="226.5553" y1="25.6667" x2="226.9363" y2="25.7429" layer="200"/>
<rectangle x1="227.3173" y1="25.6667" x2="227.6221" y2="25.7429" layer="200"/>
<rectangle x1="227.6983" y1="25.6667" x2="228.0031" y2="25.7429" layer="200"/>
<rectangle x1="228.3079" y1="25.6667" x2="228.6127" y2="25.7429" layer="200"/>
<rectangle x1="228.9175" y1="25.6667" x2="229.4509" y2="25.7429" layer="200"/>
<rectangle x1="229.6033" y1="25.6667" x2="229.9081" y2="25.7429" layer="200"/>
<rectangle x1="230.5177" y1="25.6667" x2="230.7463" y2="25.7429" layer="200"/>
<rectangle x1="230.8225" y1="25.6667" x2="231.3559" y2="25.7429" layer="200"/>
<rectangle x1="231.4321" y1="25.6667" x2="231.7369" y2="25.7429" layer="200"/>
<rectangle x1="231.8131" y1="25.6667" x2="232.1179" y2="25.7429" layer="200"/>
<rectangle x1="232.7275" y1="25.6667" x2="233.0323" y2="25.7429" layer="200"/>
<rectangle x1="234.0991" y1="25.6667" x2="234.4801" y2="25.7429" layer="200"/>
<rectangle x1="234.5563" y1="25.6667" x2="235.0897" y2="25.7429" layer="200"/>
<rectangle x1="235.2421" y1="25.6667" x2="235.8517" y2="25.7429" layer="200"/>
<rectangle x1="236.0041" y1="25.6667" x2="236.3089" y2="25.7429" layer="200"/>
<rectangle x1="236.4613" y1="25.6667" x2="236.7661" y2="25.7429" layer="200"/>
<rectangle x1="237.4519" y1="25.6667" x2="237.7567" y2="25.7429" layer="200"/>
<rectangle x1="237.9853" y1="25.6667" x2="238.2901" y2="25.7429" layer="200"/>
<rectangle x1="238.5187" y1="25.6667" x2="238.8235" y2="25.7429" layer="200"/>
<rectangle x1="239.0521" y1="25.6667" x2="239.6617" y2="25.7429" layer="200"/>
<rectangle x1="213.4489" y1="25.7429" x2="217.1065" y2="25.8191" layer="200"/>
<rectangle x1="219.9259" y1="25.7429" x2="220.3069" y2="25.8191" layer="200"/>
<rectangle x1="220.5355" y1="25.7429" x2="220.8403" y2="25.8191" layer="200"/>
<rectangle x1="221.0689" y1="25.7429" x2="221.3737" y2="25.8191" layer="200"/>
<rectangle x1="221.5261" y1="25.7429" x2="221.8309" y2="25.8191" layer="200"/>
<rectangle x1="222.0595" y1="25.7429" x2="222.9739" y2="25.8191" layer="200"/>
<rectangle x1="223.0501" y1="25.7429" x2="223.3549" y2="25.8191" layer="200"/>
<rectangle x1="223.7359" y1="25.7429" x2="224.0407" y2="25.8191" layer="200"/>
<rectangle x1="224.3455" y1="25.7429" x2="224.5741" y2="25.8191" layer="200"/>
<rectangle x1="224.7265" y1="25.7429" x2="224.8789" y2="25.8191" layer="200"/>
<rectangle x1="225.1075" y1="25.7429" x2="225.4123" y2="25.8191" layer="200"/>
<rectangle x1="225.6409" y1="25.7429" x2="225.9457" y2="25.8191" layer="200"/>
<rectangle x1="226.1743" y1="25.7429" x2="226.4791" y2="25.8191" layer="200"/>
<rectangle x1="226.5553" y1="25.7429" x2="226.9363" y2="25.8191" layer="200"/>
<rectangle x1="227.3173" y1="25.7429" x2="227.6221" y2="25.8191" layer="200"/>
<rectangle x1="227.6983" y1="25.7429" x2="228.0031" y2="25.8191" layer="200"/>
<rectangle x1="228.3079" y1="25.7429" x2="228.6127" y2="25.8191" layer="200"/>
<rectangle x1="228.6889" y1="25.7429" x2="228.8413" y2="25.8191" layer="200"/>
<rectangle x1="229.0699" y1="25.7429" x2="229.4509" y2="25.8191" layer="200"/>
<rectangle x1="229.6033" y1="25.7429" x2="229.9081" y2="25.8191" layer="200"/>
<rectangle x1="230.5177" y1="25.7429" x2="230.7463" y2="25.8191" layer="200"/>
<rectangle x1="230.8225" y1="25.7429" x2="231.1273" y2="25.8191" layer="200"/>
<rectangle x1="231.2035" y1="25.7429" x2="231.3559" y2="25.8191" layer="200"/>
<rectangle x1="231.4321" y1="25.7429" x2="231.7369" y2="25.8191" layer="200"/>
<rectangle x1="231.8131" y1="25.7429" x2="233.0323" y2="25.8191" layer="200"/>
<rectangle x1="233.9467" y1="25.7429" x2="234.7849" y2="25.8191" layer="200"/>
<rectangle x1="234.8611" y1="25.7429" x2="235.0897" y2="25.8191" layer="200"/>
<rectangle x1="235.2421" y1="25.7429" x2="235.7755" y2="25.8191" layer="200"/>
<rectangle x1="236.0041" y1="25.7429" x2="236.3089" y2="25.8191" layer="200"/>
<rectangle x1="236.4613" y1="25.7429" x2="237.3757" y2="25.8191" layer="200"/>
<rectangle x1="237.4519" y1="25.7429" x2="237.7567" y2="25.8191" layer="200"/>
<rectangle x1="237.9853" y1="25.7429" x2="238.2901" y2="25.8191" layer="200"/>
<rectangle x1="238.5187" y1="25.7429" x2="238.8235" y2="25.8191" layer="200"/>
<rectangle x1="238.9759" y1="25.7429" x2="239.5855" y2="25.8191" layer="200"/>
<rectangle x1="213.3727" y1="25.8191" x2="217.1827" y2="25.8953" layer="200"/>
<rectangle x1="219.9259" y1="25.8191" x2="220.3069" y2="25.8953" layer="200"/>
<rectangle x1="220.5355" y1="25.8191" x2="220.8403" y2="25.8953" layer="200"/>
<rectangle x1="221.0689" y1="25.8191" x2="221.3737" y2="25.8953" layer="200"/>
<rectangle x1="221.5261" y1="25.8191" x2="221.8309" y2="25.8953" layer="200"/>
<rectangle x1="222.0595" y1="25.8191" x2="222.9739" y2="25.8953" layer="200"/>
<rectangle x1="223.0501" y1="25.8191" x2="223.3549" y2="25.8953" layer="200"/>
<rectangle x1="223.5073" y1="25.8191" x2="223.5835" y2="25.8953" layer="200"/>
<rectangle x1="223.7359" y1="25.8191" x2="224.0407" y2="25.8953" layer="200"/>
<rectangle x1="224.2693" y1="25.8191" x2="224.5741" y2="25.8953" layer="200"/>
<rectangle x1="224.7265" y1="25.8191" x2="224.9551" y2="25.8953" layer="200"/>
<rectangle x1="225.1075" y1="25.8191" x2="225.4123" y2="25.8953" layer="200"/>
<rectangle x1="225.6409" y1="25.8191" x2="225.9457" y2="25.8953" layer="200"/>
<rectangle x1="226.2505" y1="25.8191" x2="226.4791" y2="25.8953" layer="200"/>
<rectangle x1="226.6315" y1="25.8191" x2="226.9363" y2="25.8953" layer="200"/>
<rectangle x1="227.3173" y1="25.8191" x2="227.6221" y2="25.8953" layer="200"/>
<rectangle x1="227.6983" y1="25.8191" x2="228.0031" y2="25.8953" layer="200"/>
<rectangle x1="228.3079" y1="25.8191" x2="228.6127" y2="25.8953" layer="200"/>
<rectangle x1="228.6889" y1="25.8191" x2="228.9175" y2="25.8953" layer="200"/>
<rectangle x1="229.1461" y1="25.8191" x2="229.4509" y2="25.8953" layer="200"/>
<rectangle x1="229.6033" y1="25.8191" x2="229.9081" y2="25.8953" layer="200"/>
<rectangle x1="230.5177" y1="25.8191" x2="230.7463" y2="25.8953" layer="200"/>
<rectangle x1="230.8225" y1="25.8191" x2="231.1273" y2="25.8953" layer="200"/>
<rectangle x1="231.2035" y1="25.8191" x2="231.3559" y2="25.8953" layer="200"/>
<rectangle x1="231.4321" y1="25.8191" x2="231.7369" y2="25.8953" layer="200"/>
<rectangle x1="231.8131" y1="25.8191" x2="233.0323" y2="25.8953" layer="200"/>
<rectangle x1="233.7943" y1="25.8191" x2="234.4039" y2="25.8953" layer="200"/>
<rectangle x1="234.4801" y1="25.8191" x2="234.7849" y2="25.8953" layer="200"/>
<rectangle x1="234.9373" y1="25.8191" x2="235.0897" y2="25.8953" layer="200"/>
<rectangle x1="235.2421" y1="25.8191" x2="235.6993" y2="25.8953" layer="200"/>
<rectangle x1="236.0041" y1="25.8191" x2="236.3089" y2="25.8953" layer="200"/>
<rectangle x1="236.5375" y1="25.8191" x2="237.3757" y2="25.8953" layer="200"/>
<rectangle x1="237.4519" y1="25.8191" x2="237.7567" y2="25.8953" layer="200"/>
<rectangle x1="237.9853" y1="25.8191" x2="238.2901" y2="25.8953" layer="200"/>
<rectangle x1="238.5187" y1="25.8191" x2="238.8235" y2="25.8953" layer="200"/>
<rectangle x1="238.9759" y1="25.8191" x2="239.4331" y2="25.8953" layer="200"/>
<rectangle x1="213.2965" y1="25.8953" x2="217.2589" y2="25.9715" layer="200"/>
<rectangle x1="219.9259" y1="25.8953" x2="220.3069" y2="25.9715" layer="200"/>
<rectangle x1="220.3831" y1="25.8953" x2="220.8403" y2="25.9715" layer="200"/>
<rectangle x1="221.0689" y1="25.8953" x2="221.3737" y2="25.9715" layer="200"/>
<rectangle x1="221.5261" y1="25.8953" x2="221.8309" y2="25.9715" layer="200"/>
<rectangle x1="222.0595" y1="25.8953" x2="222.3643" y2="25.9715" layer="200"/>
<rectangle x1="222.5929" y1="25.8953" x2="224.0407" y2="25.9715" layer="200"/>
<rectangle x1="224.2693" y1="25.8953" x2="224.5741" y2="25.9715" layer="200"/>
<rectangle x1="224.7265" y1="25.8953" x2="225.0313" y2="25.9715" layer="200"/>
<rectangle x1="225.1075" y1="25.8953" x2="225.4123" y2="25.9715" layer="200"/>
<rectangle x1="225.6409" y1="25.8953" x2="225.8695" y2="25.9715" layer="200"/>
<rectangle x1="226.0981" y1="25.8953" x2="226.4791" y2="25.9715" layer="200"/>
<rectangle x1="226.6315" y1="25.8953" x2="226.9363" y2="25.9715" layer="200"/>
<rectangle x1="227.2411" y1="25.8953" x2="227.5459" y2="25.9715" layer="200"/>
<rectangle x1="227.6221" y1="25.8953" x2="228.0793" y2="25.9715" layer="200"/>
<rectangle x1="228.3079" y1="25.8953" x2="228.6127" y2="25.9715" layer="200"/>
<rectangle x1="228.6889" y1="25.8953" x2="228.9937" y2="25.9715" layer="200"/>
<rectangle x1="229.1461" y1="25.8953" x2="229.4509" y2="25.9715" layer="200"/>
<rectangle x1="229.6033" y1="25.8953" x2="229.9081" y2="25.9715" layer="200"/>
<rectangle x1="230.5177" y1="25.8953" x2="231.1273" y2="25.9715" layer="200"/>
<rectangle x1="231.2035" y1="25.8953" x2="231.7369" y2="25.9715" layer="200"/>
<rectangle x1="231.8893" y1="25.8953" x2="232.1179" y2="25.9715" layer="200"/>
<rectangle x1="232.3465" y1="25.8953" x2="233.0323" y2="25.9715" layer="200"/>
<rectangle x1="233.7181" y1="25.8953" x2="234.3277" y2="25.9715" layer="200"/>
<rectangle x1="234.4039" y1="25.8953" x2="234.7087" y2="25.9715" layer="200"/>
<rectangle x1="235.0135" y1="25.8953" x2="235.1659" y2="25.9715" layer="200"/>
<rectangle x1="235.2421" y1="25.8953" x2="235.5469" y2="25.9715" layer="200"/>
<rectangle x1="235.6993" y1="25.8953" x2="235.8517" y2="25.9715" layer="200"/>
<rectangle x1="236.0041" y1="25.8953" x2="236.3089" y2="25.9715" layer="200"/>
<rectangle x1="236.5375" y1="25.8953" x2="236.8423" y2="25.9715" layer="200"/>
<rectangle x1="236.9947" y1="25.8953" x2="237.8329" y2="25.9715" layer="200"/>
<rectangle x1="237.9853" y1="25.8953" x2="238.3663" y2="25.9715" layer="200"/>
<rectangle x1="238.5187" y1="25.8953" x2="238.8235" y2="25.9715" layer="200"/>
<rectangle x1="238.9759" y1="25.8953" x2="239.2807" y2="25.9715" layer="200"/>
<rectangle x1="239.4331" y1="25.8953" x2="239.5855" y2="25.9715" layer="200"/>
<rectangle x1="213.1441" y1="25.9715" x2="217.3351" y2="26.0477" layer="200"/>
<rectangle x1="219.9259" y1="25.9715" x2="220.3069" y2="26.0477" layer="200"/>
<rectangle x1="220.3831" y1="25.9715" x2="221.3737" y2="26.0477" layer="200"/>
<rectangle x1="221.4499" y1="25.9715" x2="222.0595" y2="26.0477" layer="200"/>
<rectangle x1="222.1357" y1="25.9715" x2="222.4405" y2="26.0477" layer="200"/>
<rectangle x1="222.5929" y1="25.9715" x2="222.8977" y2="26.0477" layer="200"/>
<rectangle x1="222.9739" y1="25.9715" x2="224.5741" y2="26.0477" layer="200"/>
<rectangle x1="224.7265" y1="25.9715" x2="225.4123" y2="26.0477" layer="200"/>
<rectangle x1="225.4885" y1="25.9715" x2="226.4791" y2="26.0477" layer="200"/>
<rectangle x1="226.7077" y1="25.9715" x2="227.5459" y2="26.0477" layer="200"/>
<rectangle x1="227.6221" y1="25.9715" x2="228.6127" y2="26.0477" layer="200"/>
<rectangle x1="228.6889" y1="25.9715" x2="229.4509" y2="26.0477" layer="200"/>
<rectangle x1="229.6033" y1="25.9715" x2="229.9081" y2="26.0477" layer="200"/>
<rectangle x1="230.5177" y1="25.9715" x2="231.0511" y2="26.0477" layer="200"/>
<rectangle x1="231.2797" y1="25.9715" x2="231.7369" y2="26.0477" layer="200"/>
<rectangle x1="231.8893" y1="25.9715" x2="232.2703" y2="26.0477" layer="200"/>
<rectangle x1="232.3465" y1="25.9715" x2="233.2609" y2="26.0477" layer="200"/>
<rectangle x1="233.6419" y1="25.9715" x2="234.2515" y2="26.0477" layer="200"/>
<rectangle x1="234.3277" y1="25.9715" x2="235.5469" y2="26.0477" layer="200"/>
<rectangle x1="235.6231" y1="25.9715" x2="235.8517" y2="26.0477" layer="200"/>
<rectangle x1="235.9279" y1="25.9715" x2="236.9185" y2="26.0477" layer="200"/>
<rectangle x1="236.9947" y1="25.9715" x2="237.2995" y2="26.0477" layer="200"/>
<rectangle x1="237.3757" y1="25.9715" x2="238.8235" y2="26.0477" layer="200"/>
<rectangle x1="238.9759" y1="25.9715" x2="239.2807" y2="26.0477" layer="200"/>
<rectangle x1="239.3569" y1="25.9715" x2="239.5855" y2="26.0477" layer="200"/>
<rectangle x1="213.0679" y1="26.0477" x2="217.4113" y2="26.1239" layer="200"/>
<rectangle x1="219.9259" y1="26.0477" x2="220.3069" y2="26.1239" layer="200"/>
<rectangle x1="220.5355" y1="26.0477" x2="221.2975" y2="26.1239" layer="200"/>
<rectangle x1="221.4499" y1="26.0477" x2="222.0595" y2="26.1239" layer="200"/>
<rectangle x1="222.2119" y1="26.0477" x2="222.8215" y2="26.1239" layer="200"/>
<rectangle x1="223.0501" y1="26.0477" x2="223.6597" y2="26.1239" layer="200"/>
<rectangle x1="223.7359" y1="26.0477" x2="224.4979" y2="26.1239" layer="200"/>
<rectangle x1="224.8789" y1="26.0477" x2="225.4123" y2="26.1239" layer="200"/>
<rectangle x1="225.4885" y1="26.0477" x2="226.1743" y2="26.1239" layer="200"/>
<rectangle x1="226.2505" y1="26.0477" x2="226.4791" y2="26.1239" layer="200"/>
<rectangle x1="226.7839" y1="26.0477" x2="227.3935" y2="26.1239" layer="200"/>
<rectangle x1="227.7745" y1="26.0477" x2="228.5365" y2="26.1239" layer="200"/>
<rectangle x1="228.8413" y1="26.0477" x2="229.3747" y2="26.1239" layer="200"/>
<rectangle x1="229.6033" y1="26.0477" x2="229.9081" y2="26.1239" layer="200"/>
<rectangle x1="230.5177" y1="26.0477" x2="231.0511" y2="26.1239" layer="200"/>
<rectangle x1="231.2797" y1="26.0477" x2="231.7369" y2="26.1239" layer="200"/>
<rectangle x1="231.9655" y1="26.0477" x2="232.5751" y2="26.1239" layer="200"/>
<rectangle x1="232.6513" y1="26.0477" x2="233.2609" y2="26.1239" layer="200"/>
<rectangle x1="233.6419" y1="26.0477" x2="234.0229" y2="26.1239" layer="200"/>
<rectangle x1="234.2515" y1="26.0477" x2="235.8517" y2="26.1239" layer="200"/>
<rectangle x1="235.9279" y1="26.0477" x2="236.5375" y2="26.1239" layer="200"/>
<rectangle x1="236.6899" y1="26.0477" x2="237.2995" y2="26.1239" layer="200"/>
<rectangle x1="237.5281" y1="26.0477" x2="238.2139" y2="26.1239" layer="200"/>
<rectangle x1="238.2901" y1="26.0477" x2="238.7473" y2="26.1239" layer="200"/>
<rectangle x1="239.0521" y1="26.0477" x2="239.5855" y2="26.1239" layer="200"/>
<rectangle x1="213.0679" y1="26.1239" x2="217.4875" y2="26.2001" layer="200"/>
<rectangle x1="219.9259" y1="26.1239" x2="220.3069" y2="26.2001" layer="200"/>
<rectangle x1="220.6879" y1="26.1239" x2="220.7641" y2="26.2001" layer="200"/>
<rectangle x1="220.9165" y1="26.1239" x2="221.2213" y2="26.2001" layer="200"/>
<rectangle x1="221.5261" y1="26.1239" x2="221.8309" y2="26.2001" layer="200"/>
<rectangle x1="222.3643" y1="26.1239" x2="222.6691" y2="26.2001" layer="200"/>
<rectangle x1="223.2025" y1="26.1239" x2="223.2787" y2="26.2001" layer="200"/>
<rectangle x1="223.3549" y1="26.1239" x2="223.5835" y2="26.2001" layer="200"/>
<rectangle x1="223.8883" y1="26.1239" x2="224.0407" y2="26.2001" layer="200"/>
<rectangle x1="224.1169" y1="26.1239" x2="224.4217" y2="26.2001" layer="200"/>
<rectangle x1="224.9551" y1="26.1239" x2="225.3361" y2="26.2001" layer="200"/>
<rectangle x1="225.6409" y1="26.1239" x2="225.9457" y2="26.2001" layer="200"/>
<rectangle x1="226.4029" y1="26.1239" x2="226.4791" y2="26.2001" layer="200"/>
<rectangle x1="226.9363" y1="26.1239" x2="227.3173" y2="26.2001" layer="200"/>
<rectangle x1="227.8507" y1="26.1239" x2="228.0031" y2="26.2001" layer="200"/>
<rectangle x1="228.0793" y1="26.1239" x2="228.3841" y2="26.2001" layer="200"/>
<rectangle x1="228.9937" y1="26.1239" x2="229.2985" y2="26.2001" layer="200"/>
<rectangle x1="229.6033" y1="26.1239" x2="229.9081" y2="26.2001" layer="200"/>
<rectangle x1="230.5939" y1="26.1239" x2="231.0511" y2="26.2001" layer="200"/>
<rectangle x1="231.2797" y1="26.1239" x2="231.7369" y2="26.2001" layer="200"/>
<rectangle x1="232.1179" y1="26.1239" x2="232.4989" y2="26.2001" layer="200"/>
<rectangle x1="232.7275" y1="26.1239" x2="233.0323" y2="26.2001" layer="200"/>
<rectangle x1="233.6419" y1="26.1239" x2="233.9467" y2="26.2001" layer="200"/>
<rectangle x1="234.2515" y1="26.1239" x2="234.4039" y2="26.2001" layer="200"/>
<rectangle x1="234.7087" y1="26.1239" x2="234.7849" y2="26.2001" layer="200"/>
<rectangle x1="234.8611" y1="26.1239" x2="234.9373" y2="26.2001" layer="200"/>
<rectangle x1="235.3945" y1="26.1239" x2="235.7755" y2="26.2001" layer="200"/>
<rectangle x1="236.0041" y1="26.1239" x2="236.3089" y2="26.2001" layer="200"/>
<rectangle x1="236.7661" y1="26.1239" x2="237.1471" y2="26.2001" layer="200"/>
<rectangle x1="237.6043" y1="26.1239" x2="237.7567" y2="26.2001" layer="200"/>
<rectangle x1="237.8329" y1="26.1239" x2="238.1377" y2="26.2001" layer="200"/>
<rectangle x1="238.3663" y1="26.1239" x2="238.6711" y2="26.2001" layer="200"/>
<rectangle x1="239.1283" y1="26.1239" x2="239.5093" y2="26.2001" layer="200"/>
<rectangle x1="212.9155" y1="26.2001" x2="217.5637" y2="26.2763" layer="200"/>
<rectangle x1="219.9259" y1="26.2001" x2="220.3069" y2="26.2763" layer="200"/>
<rectangle x1="221.6785" y1="26.2001" x2="221.8309" y2="26.2763" layer="200"/>
<rectangle x1="225.7171" y1="26.2001" x2="225.9457" y2="26.2763" layer="200"/>
<rectangle x1="226.2505" y1="26.2001" x2="226.4791" y2="26.2763" layer="200"/>
<rectangle x1="229.6033" y1="26.2001" x2="229.9081" y2="26.2763" layer="200"/>
<rectangle x1="230.5939" y1="26.2001" x2="230.9749" y2="26.2763" layer="200"/>
<rectangle x1="231.3559" y1="26.2001" x2="231.7369" y2="26.2763" layer="200"/>
<rectangle x1="232.8037" y1="26.2001" x2="233.0323" y2="26.2763" layer="200"/>
<rectangle x1="233.6419" y1="26.2001" x2="233.8705" y2="26.2763" layer="200"/>
<rectangle x1="234.1753" y1="26.2001" x2="234.4039" y2="26.2763" layer="200"/>
<rectangle x1="236.0803" y1="26.2001" x2="236.3089" y2="26.2763" layer="200"/>
<rectangle x1="212.9155" y1="26.2763" x2="214.3633" y2="26.3525" layer="200"/>
<rectangle x1="216.1921" y1="26.2763" x2="217.6399" y2="26.3525" layer="200"/>
<rectangle x1="219.9259" y1="26.2763" x2="220.3069" y2="26.3525" layer="200"/>
<rectangle x1="221.7547" y1="26.2763" x2="221.8309" y2="26.3525" layer="200"/>
<rectangle x1="225.7933" y1="26.2763" x2="225.9457" y2="26.3525" layer="200"/>
<rectangle x1="226.1743" y1="26.2763" x2="226.4791" y2="26.3525" layer="200"/>
<rectangle x1="229.6033" y1="26.2763" x2="229.9081" y2="26.3525" layer="200"/>
<rectangle x1="230.5939" y1="26.2763" x2="230.8987" y2="26.3525" layer="200"/>
<rectangle x1="231.3559" y1="26.2763" x2="231.7369" y2="26.3525" layer="200"/>
<rectangle x1="232.8799" y1="26.2763" x2="233.0323" y2="26.3525" layer="200"/>
<rectangle x1="233.6419" y1="26.2763" x2="233.9467" y2="26.3525" layer="200"/>
<rectangle x1="234.1753" y1="26.2763" x2="234.4039" y2="26.3525" layer="200"/>
<rectangle x1="236.1565" y1="26.2763" x2="236.3089" y2="26.3525" layer="200"/>
<rectangle x1="212.8393" y1="26.3525" x2="214.3633" y2="26.4287" layer="200"/>
<rectangle x1="216.1159" y1="26.3525" x2="217.6399" y2="26.4287" layer="200"/>
<rectangle x1="219.7735" y1="26.3525" x2="220.4593" y2="26.4287" layer="200"/>
<rectangle x1="226.1743" y1="26.3525" x2="226.4791" y2="26.4287" layer="200"/>
<rectangle x1="229.5271" y1="26.3525" x2="229.9081" y2="26.4287" layer="200"/>
<rectangle x1="230.4415" y1="26.3525" x2="230.8987" y2="26.4287" layer="200"/>
<rectangle x1="231.4321" y1="26.3525" x2="231.8893" y2="26.4287" layer="200"/>
<rectangle x1="233.7181" y1="26.3525" x2="234.4039" y2="26.4287" layer="200"/>
<rectangle x1="212.7631" y1="26.4287" x2="214.3633" y2="26.5049" layer="200"/>
<rectangle x1="216.1159" y1="26.4287" x2="217.7161" y2="26.5049" layer="200"/>
<rectangle x1="219.7735" y1="26.4287" x2="220.3831" y2="26.5049" layer="200"/>
<rectangle x1="226.2505" y1="26.4287" x2="226.4791" y2="26.5049" layer="200"/>
<rectangle x1="229.5271" y1="26.4287" x2="229.9081" y2="26.5049" layer="200"/>
<rectangle x1="230.4415" y1="26.4287" x2="230.8987" y2="26.5049" layer="200"/>
<rectangle x1="231.4321" y1="26.4287" x2="231.8893" y2="26.5049" layer="200"/>
<rectangle x1="233.7943" y1="26.4287" x2="234.4039" y2="26.5049" layer="200"/>
<rectangle x1="212.7631" y1="26.5049" x2="214.3633" y2="26.5811" layer="200"/>
<rectangle x1="216.1159" y1="26.5049" x2="217.7923" y2="26.5811" layer="200"/>
<rectangle x1="229.6033" y1="26.5049" x2="229.9081" y2="26.5811" layer="200"/>
<rectangle x1="212.6869" y1="26.5811" x2="214.3633" y2="26.6573" layer="200"/>
<rectangle x1="216.1159" y1="26.5811" x2="217.7923" y2="26.6573" layer="200"/>
<rectangle x1="229.8319" y1="26.5811" x2="229.9081" y2="26.6573" layer="200"/>
<rectangle x1="212.6107" y1="26.6573" x2="214.3633" y2="26.7335" layer="200"/>
<rectangle x1="216.1159" y1="26.6573" x2="217.8685" y2="26.7335" layer="200"/>
<rectangle x1="212.6107" y1="26.7335" x2="214.3633" y2="26.8097" layer="200"/>
<rectangle x1="216.1159" y1="26.7335" x2="217.9447" y2="26.8097" layer="200"/>
<rectangle x1="212.5345" y1="26.8097" x2="214.3633" y2="26.8859" layer="200"/>
<rectangle x1="216.1159" y1="26.8097" x2="217.9447" y2="26.8859" layer="200"/>
<rectangle x1="212.5345" y1="26.8859" x2="214.3633" y2="26.9621" layer="200"/>
<rectangle x1="216.1159" y1="26.8859" x2="217.9447" y2="26.9621" layer="200"/>
<rectangle x1="212.4583" y1="26.9621" x2="214.3633" y2="27.0383" layer="200"/>
<rectangle x1="216.1159" y1="26.9621" x2="218.0209" y2="27.0383" layer="200"/>
<rectangle x1="212.4583" y1="27.0383" x2="214.3633" y2="27.1145" layer="200"/>
<rectangle x1="216.1159" y1="27.0383" x2="218.0209" y2="27.1145" layer="200"/>
<rectangle x1="225.2599" y1="27.0383" x2="225.8695" y2="27.1145" layer="200"/>
<rectangle x1="227.3173" y1="27.0383" x2="228.2317" y2="27.1145" layer="200"/>
<rectangle x1="236.3089" y1="27.0383" x2="237.1471" y2="27.1145" layer="200"/>
<rectangle x1="238.6711" y1="27.0383" x2="239.2807" y2="27.1145" layer="200"/>
<rectangle x1="212.4583" y1="27.1145" x2="214.3633" y2="27.1907" layer="200"/>
<rectangle x1="216.1159" y1="27.1145" x2="218.0971" y2="27.1907" layer="200"/>
<rectangle x1="219.8497" y1="27.1145" x2="220.3069" y2="27.1907" layer="200"/>
<rectangle x1="220.9165" y1="27.1145" x2="221.4499" y2="27.1907" layer="200"/>
<rectangle x1="221.5261" y1="27.1145" x2="222.8977" y2="27.1907" layer="200"/>
<rectangle x1="223.3549" y1="27.1145" x2="224.7265" y2="27.1907" layer="200"/>
<rectangle x1="225.1075" y1="27.1145" x2="226.0981" y2="27.1907" layer="200"/>
<rectangle x1="227.0887" y1="27.1145" x2="228.4603" y2="27.1907" layer="200"/>
<rectangle x1="229.0699" y1="27.1145" x2="230.6701" y2="27.1907" layer="200"/>
<rectangle x1="231.0511" y1="27.1145" x2="232.3465" y2="27.1907" layer="200"/>
<rectangle x1="234.0229" y1="27.1145" x2="234.6325" y2="27.1907" layer="200"/>
<rectangle x1="234.9373" y1="27.1145" x2="235.6231" y2="27.1907" layer="200"/>
<rectangle x1="236.0803" y1="27.1145" x2="237.3757" y2="27.1907" layer="200"/>
<rectangle x1="238.5949" y1="27.1145" x2="239.5093" y2="27.1907" layer="200"/>
<rectangle x1="212.3821" y1="27.1907" x2="214.3633" y2="27.2669" layer="200"/>
<rectangle x1="216.1159" y1="27.1907" x2="218.0971" y2="27.2669" layer="200"/>
<rectangle x1="219.7735" y1="27.1907" x2="222.9739" y2="27.2669" layer="200"/>
<rectangle x1="223.3549" y1="27.1907" x2="224.8027" y2="27.2669" layer="200"/>
<rectangle x1="225.1075" y1="27.1907" x2="226.1743" y2="27.2669" layer="200"/>
<rectangle x1="227.0125" y1="27.1907" x2="228.6127" y2="27.2669" layer="200"/>
<rectangle x1="229.0699" y1="27.1907" x2="230.6701" y2="27.2669" layer="200"/>
<rectangle x1="230.9749" y1="27.1907" x2="232.4227" y2="27.2669" layer="200"/>
<rectangle x1="233.9467" y1="27.1907" x2="235.6231" y2="27.2669" layer="200"/>
<rectangle x1="235.9279" y1="27.1907" x2="237.5281" y2="27.2669" layer="200"/>
<rectangle x1="238.4425" y1="27.1907" x2="239.5855" y2="27.2669" layer="200"/>
<rectangle x1="212.3821" y1="27.2669" x2="214.3633" y2="27.3431" layer="200"/>
<rectangle x1="216.1159" y1="27.2669" x2="218.0971" y2="27.3431" layer="200"/>
<rectangle x1="219.7735" y1="27.2669" x2="222.9739" y2="27.3431" layer="200"/>
<rectangle x1="223.3549" y1="27.2669" x2="224.8027" y2="27.3431" layer="200"/>
<rectangle x1="225.0313" y1="27.2669" x2="226.2505" y2="27.3431" layer="200"/>
<rectangle x1="226.8601" y1="27.2669" x2="228.6889" y2="27.3431" layer="200"/>
<rectangle x1="229.0699" y1="27.2669" x2="230.6701" y2="27.3431" layer="200"/>
<rectangle x1="230.9749" y1="27.2669" x2="232.4227" y2="27.3431" layer="200"/>
<rectangle x1="233.0323" y1="27.2669" x2="233.3371" y2="27.3431" layer="200"/>
<rectangle x1="233.9467" y1="27.2669" x2="235.6231" y2="27.3431" layer="200"/>
<rectangle x1="235.8517" y1="27.2669" x2="237.6043" y2="27.3431" layer="200"/>
<rectangle x1="238.4425" y1="27.2669" x2="239.6617" y2="27.3431" layer="200"/>
<rectangle x1="212.3821" y1="27.3431" x2="214.3633" y2="27.4193" layer="200"/>
<rectangle x1="216.1159" y1="27.3431" x2="218.1733" y2="27.4193" layer="200"/>
<rectangle x1="219.7735" y1="27.3431" x2="221.5261" y2="27.4193" layer="200"/>
<rectangle x1="221.6023" y1="27.3431" x2="222.8215" y2="27.4193" layer="200"/>
<rectangle x1="223.4311" y1="27.3431" x2="224.6503" y2="27.4193" layer="200"/>
<rectangle x1="225.0313" y1="27.3431" x2="226.3267" y2="27.4193" layer="200"/>
<rectangle x1="226.7839" y1="27.3431" x2="228.7651" y2="27.4193" layer="200"/>
<rectangle x1="229.1461" y1="27.3431" x2="230.5939" y2="27.4193" layer="200"/>
<rectangle x1="231.0511" y1="27.3431" x2="232.3465" y2="27.4193" layer="200"/>
<rectangle x1="232.9561" y1="27.3431" x2="233.4133" y2="27.4193" layer="200"/>
<rectangle x1="234.0229" y1="27.3431" x2="235.6231" y2="27.4193" layer="200"/>
<rectangle x1="235.6993" y1="27.3431" x2="237.7567" y2="27.4193" layer="200"/>
<rectangle x1="238.3663" y1="27.3431" x2="239.7379" y2="27.4193" layer="200"/>
<rectangle x1="212.3059" y1="27.4193" x2="214.3633" y2="27.4955" layer="200"/>
<rectangle x1="216.1159" y1="27.4193" x2="218.1733" y2="27.4955" layer="200"/>
<rectangle x1="220.1545" y1="27.4193" x2="221.1451" y2="27.4955" layer="200"/>
<rectangle x1="221.8309" y1="27.4193" x2="222.5929" y2="27.4955" layer="200"/>
<rectangle x1="223.6597" y1="27.4193" x2="224.4217" y2="27.4955" layer="200"/>
<rectangle x1="224.9551" y1="27.4193" x2="226.4029" y2="27.4955" layer="200"/>
<rectangle x1="226.7077" y1="27.4193" x2="228.8413" y2="27.4955" layer="200"/>
<rectangle x1="229.3747" y1="27.4193" x2="230.2891" y2="27.4955" layer="200"/>
<rectangle x1="231.4321" y1="27.4193" x2="231.9655" y2="27.4955" layer="200"/>
<rectangle x1="232.9561" y1="27.4193" x2="233.4133" y2="27.4955" layer="200"/>
<rectangle x1="234.3277" y1="27.4193" x2="235.2421" y2="27.4955" layer="200"/>
<rectangle x1="235.6993" y1="27.4193" x2="237.8329" y2="27.4955" layer="200"/>
<rectangle x1="238.3663" y1="27.4193" x2="239.7379" y2="27.4955" layer="200"/>
<rectangle x1="212.3059" y1="27.4955" x2="214.3633" y2="27.5717" layer="200"/>
<rectangle x1="216.1159" y1="27.4955" x2="218.1733" y2="27.5717" layer="200"/>
<rectangle x1="220.2307" y1="27.4955" x2="221.0689" y2="27.5717" layer="200"/>
<rectangle x1="221.8309" y1="27.4955" x2="222.5929" y2="27.5717" layer="200"/>
<rectangle x1="223.6597" y1="27.4955" x2="224.3455" y2="27.5717" layer="200"/>
<rectangle x1="224.9551" y1="27.4955" x2="225.7171" y2="27.5717" layer="200"/>
<rectangle x1="226.0981" y1="27.4955" x2="226.4029" y2="27.5717" layer="200"/>
<rectangle x1="226.7077" y1="27.4955" x2="227.7745" y2="27.5717" layer="200"/>
<rectangle x1="228.3841" y1="27.4955" x2="228.9175" y2="27.5717" layer="200"/>
<rectangle x1="229.4509" y1="27.4955" x2="230.1367" y2="27.5717" layer="200"/>
<rectangle x1="231.4321" y1="27.4955" x2="231.8893" y2="27.5717" layer="200"/>
<rectangle x1="232.9561" y1="27.4955" x2="233.4133" y2="27.5717" layer="200"/>
<rectangle x1="234.4039" y1="27.4955" x2="235.1659" y2="27.5717" layer="200"/>
<rectangle x1="235.6231" y1="27.4955" x2="236.6899" y2="27.5717" layer="200"/>
<rectangle x1="237.3757" y1="27.4955" x2="237.9091" y2="27.5717" layer="200"/>
<rectangle x1="238.3663" y1="27.4955" x2="239.1283" y2="27.5717" layer="200"/>
<rectangle x1="239.5093" y1="27.4955" x2="239.7379" y2="27.5717" layer="200"/>
<rectangle x1="212.3059" y1="27.5717" x2="214.3633" y2="27.6479" layer="200"/>
<rectangle x1="216.1159" y1="27.5717" x2="218.1733" y2="27.6479" layer="200"/>
<rectangle x1="220.2307" y1="27.5717" x2="220.9927" y2="27.6479" layer="200"/>
<rectangle x1="221.8309" y1="27.5717" x2="222.5929" y2="27.6479" layer="200"/>
<rectangle x1="223.6597" y1="27.5717" x2="224.3455" y2="27.6479" layer="200"/>
<rectangle x1="224.9551" y1="27.5717" x2="225.6409" y2="27.6479" layer="200"/>
<rectangle x1="226.2505" y1="27.5717" x2="226.3267" y2="27.6479" layer="200"/>
<rectangle x1="226.6315" y1="27.5717" x2="227.6221" y2="27.6479" layer="200"/>
<rectangle x1="228.6127" y1="27.5717" x2="228.9937" y2="27.6479" layer="200"/>
<rectangle x1="229.4509" y1="27.5717" x2="230.1367" y2="27.6479" layer="200"/>
<rectangle x1="231.5083" y1="27.5717" x2="231.8893" y2="27.6479" layer="200"/>
<rectangle x1="232.8799" y1="27.5717" x2="233.4895" y2="27.6479" layer="200"/>
<rectangle x1="234.4039" y1="27.5717" x2="235.1659" y2="27.6479" layer="200"/>
<rectangle x1="235.5469" y1="27.5717" x2="236.5375" y2="27.6479" layer="200"/>
<rectangle x1="237.5281" y1="27.5717" x2="237.9091" y2="27.6479" layer="200"/>
<rectangle x1="238.3663" y1="27.5717" x2="239.0521" y2="27.6479" layer="200"/>
<rectangle x1="239.6617" y1="27.5717" x2="239.7379" y2="27.6479" layer="200"/>
<rectangle x1="212.3059" y1="27.6479" x2="214.3633" y2="27.7241" layer="200"/>
<rectangle x1="216.1159" y1="27.6479" x2="218.1733" y2="27.7241" layer="200"/>
<rectangle x1="220.3069" y1="27.6479" x2="220.9927" y2="27.7241" layer="200"/>
<rectangle x1="221.8309" y1="27.6479" x2="222.5929" y2="27.7241" layer="200"/>
<rectangle x1="223.6597" y1="27.6479" x2="224.3455" y2="27.7241" layer="200"/>
<rectangle x1="224.9551" y1="27.6479" x2="225.6409" y2="27.7241" layer="200"/>
<rectangle x1="226.5553" y1="27.6479" x2="227.4697" y2="27.7241" layer="200"/>
<rectangle x1="228.6889" y1="27.6479" x2="228.9937" y2="27.7241" layer="200"/>
<rectangle x1="229.4509" y1="27.6479" x2="230.1367" y2="27.7241" layer="200"/>
<rectangle x1="231.5083" y1="27.6479" x2="231.8893" y2="27.7241" layer="200"/>
<rectangle x1="232.8799" y1="27.6479" x2="233.4895" y2="27.7241" layer="200"/>
<rectangle x1="234.4039" y1="27.6479" x2="235.1659" y2="27.7241" layer="200"/>
<rectangle x1="235.5469" y1="27.6479" x2="236.3851" y2="27.7241" layer="200"/>
<rectangle x1="237.6805" y1="27.6479" x2="237.9091" y2="27.7241" layer="200"/>
<rectangle x1="238.3663" y1="27.6479" x2="239.0521" y2="27.7241" layer="200"/>
<rectangle x1="212.3059" y1="27.7241" x2="214.3633" y2="27.8003" layer="200"/>
<rectangle x1="216.1159" y1="27.7241" x2="218.2495" y2="27.8003" layer="200"/>
<rectangle x1="220.3069" y1="27.7241" x2="220.9927" y2="27.8003" layer="200"/>
<rectangle x1="221.8309" y1="27.7241" x2="222.5929" y2="27.8003" layer="200"/>
<rectangle x1="223.6597" y1="27.7241" x2="224.3455" y2="27.8003" layer="200"/>
<rectangle x1="224.9551" y1="27.7241" x2="225.6409" y2="27.8003" layer="200"/>
<rectangle x1="226.5553" y1="27.7241" x2="227.3935" y2="27.8003" layer="200"/>
<rectangle x1="228.8413" y1="27.7241" x2="228.9937" y2="27.8003" layer="200"/>
<rectangle x1="229.4509" y1="27.7241" x2="230.1367" y2="27.8003" layer="200"/>
<rectangle x1="231.5083" y1="27.7241" x2="231.8893" y2="27.8003" layer="200"/>
<rectangle x1="232.8799" y1="27.7241" x2="233.4895" y2="27.8003" layer="200"/>
<rectangle x1="234.4039" y1="27.7241" x2="235.1659" y2="27.8003" layer="200"/>
<rectangle x1="235.4707" y1="27.7241" x2="236.3851" y2="27.8003" layer="200"/>
<rectangle x1="237.7567" y1="27.7241" x2="237.9091" y2="27.8003" layer="200"/>
<rectangle x1="238.3663" y1="27.7241" x2="239.0521" y2="27.8003" layer="200"/>
<rectangle x1="212.3059" y1="27.8003" x2="214.3633" y2="27.8765" layer="200"/>
<rectangle x1="216.1159" y1="27.8003" x2="218.2495" y2="27.8765" layer="200"/>
<rectangle x1="220.3069" y1="27.8003" x2="220.9927" y2="27.8765" layer="200"/>
<rectangle x1="221.8309" y1="27.8003" x2="222.5929" y2="27.8765" layer="200"/>
<rectangle x1="223.6597" y1="27.8003" x2="224.3455" y2="27.8765" layer="200"/>
<rectangle x1="224.9551" y1="27.8003" x2="225.6409" y2="27.8765" layer="200"/>
<rectangle x1="226.4791" y1="27.8003" x2="227.3173" y2="27.8765" layer="200"/>
<rectangle x1="229.4509" y1="27.8003" x2="230.1367" y2="27.8765" layer="200"/>
<rectangle x1="231.5083" y1="27.8003" x2="231.8893" y2="27.8765" layer="200"/>
<rectangle x1="232.8037" y1="27.8003" x2="233.5657" y2="27.8765" layer="200"/>
<rectangle x1="234.4039" y1="27.8003" x2="235.1659" y2="27.8765" layer="200"/>
<rectangle x1="235.4707" y1="27.8003" x2="236.3089" y2="27.8765" layer="200"/>
<rectangle x1="238.3663" y1="27.8003" x2="239.0521" y2="27.8765" layer="200"/>
<rectangle x1="212.3059" y1="27.8765" x2="214.3633" y2="27.9527" layer="200"/>
<rectangle x1="216.1159" y1="27.8765" x2="218.2495" y2="27.9527" layer="200"/>
<rectangle x1="220.3069" y1="27.8765" x2="220.9927" y2="27.9527" layer="200"/>
<rectangle x1="221.8309" y1="27.8765" x2="222.5929" y2="27.9527" layer="200"/>
<rectangle x1="223.6597" y1="27.8765" x2="224.3455" y2="27.9527" layer="200"/>
<rectangle x1="224.9551" y1="27.8765" x2="225.6409" y2="27.9527" layer="200"/>
<rectangle x1="226.4791" y1="27.8765" x2="227.3173" y2="27.9527" layer="200"/>
<rectangle x1="229.4509" y1="27.8765" x2="230.1367" y2="27.9527" layer="200"/>
<rectangle x1="231.5083" y1="27.8765" x2="231.8893" y2="27.9527" layer="200"/>
<rectangle x1="232.8037" y1="27.8765" x2="233.5657" y2="27.9527" layer="200"/>
<rectangle x1="234.4039" y1="27.8765" x2="235.1659" y2="27.9527" layer="200"/>
<rectangle x1="235.3945" y1="27.8765" x2="236.2327" y2="27.9527" layer="200"/>
<rectangle x1="238.3663" y1="27.8765" x2="239.0521" y2="27.9527" layer="200"/>
<rectangle x1="212.3059" y1="27.9527" x2="214.3633" y2="28.0289" layer="200"/>
<rectangle x1="216.1159" y1="27.9527" x2="218.2495" y2="28.0289" layer="200"/>
<rectangle x1="220.3069" y1="27.9527" x2="220.9927" y2="28.0289" layer="200"/>
<rectangle x1="221.8309" y1="27.9527" x2="222.5929" y2="28.0289" layer="200"/>
<rectangle x1="223.6597" y1="27.9527" x2="224.3455" y2="28.0289" layer="200"/>
<rectangle x1="224.9551" y1="27.9527" x2="225.6409" y2="28.0289" layer="200"/>
<rectangle x1="226.4791" y1="27.9527" x2="227.2411" y2="28.0289" layer="200"/>
<rectangle x1="229.4509" y1="27.9527" x2="230.1367" y2="28.0289" layer="200"/>
<rectangle x1="231.5083" y1="27.9527" x2="231.8893" y2="28.0289" layer="200"/>
<rectangle x1="232.7275" y1="27.9527" x2="233.6419" y2="28.0289" layer="200"/>
<rectangle x1="234.4039" y1="27.9527" x2="235.1659" y2="28.0289" layer="200"/>
<rectangle x1="235.3945" y1="27.9527" x2="236.1565" y2="28.0289" layer="200"/>
<rectangle x1="238.3663" y1="27.9527" x2="239.0521" y2="28.0289" layer="200"/>
<rectangle x1="212.3059" y1="28.0289" x2="214.3633" y2="28.1051" layer="200"/>
<rectangle x1="216.1159" y1="28.0289" x2="218.2495" y2="28.1051" layer="200"/>
<rectangle x1="220.3069" y1="28.0289" x2="220.9927" y2="28.1051" layer="200"/>
<rectangle x1="221.8309" y1="28.0289" x2="222.5929" y2="28.1051" layer="200"/>
<rectangle x1="223.6597" y1="28.0289" x2="224.3455" y2="28.1051" layer="200"/>
<rectangle x1="224.9551" y1="28.0289" x2="225.6409" y2="28.1051" layer="200"/>
<rectangle x1="226.4791" y1="28.0289" x2="227.2411" y2="28.1051" layer="200"/>
<rectangle x1="229.4509" y1="28.0289" x2="230.1367" y2="28.1051" layer="200"/>
<rectangle x1="231.5083" y1="28.0289" x2="231.8893" y2="28.1051" layer="200"/>
<rectangle x1="232.7275" y1="28.0289" x2="233.6419" y2="28.1051" layer="200"/>
<rectangle x1="234.4039" y1="28.0289" x2="235.1659" y2="28.1051" layer="200"/>
<rectangle x1="235.3945" y1="28.0289" x2="236.1565" y2="28.1051" layer="200"/>
<rectangle x1="238.3663" y1="28.0289" x2="239.0521" y2="28.1051" layer="200"/>
<rectangle x1="212.3059" y1="28.1051" x2="214.3633" y2="28.1813" layer="200"/>
<rectangle x1="216.1159" y1="28.1051" x2="218.2495" y2="28.1813" layer="200"/>
<rectangle x1="220.3069" y1="28.1051" x2="220.9927" y2="28.1813" layer="200"/>
<rectangle x1="221.8309" y1="28.1051" x2="222.5929" y2="28.1813" layer="200"/>
<rectangle x1="223.6597" y1="28.1051" x2="224.3455" y2="28.1813" layer="200"/>
<rectangle x1="224.9551" y1="28.1051" x2="225.6409" y2="28.1813" layer="200"/>
<rectangle x1="226.4029" y1="28.1051" x2="227.2411" y2="28.1813" layer="200"/>
<rectangle x1="229.4509" y1="28.1051" x2="230.1367" y2="28.1813" layer="200"/>
<rectangle x1="231.5083" y1="28.1051" x2="231.8893" y2="28.1813" layer="200"/>
<rectangle x1="232.7275" y1="28.1051" x2="233.6419" y2="28.1813" layer="200"/>
<rectangle x1="234.4039" y1="28.1051" x2="235.1659" y2="28.1813" layer="200"/>
<rectangle x1="235.3183" y1="28.1051" x2="236.1565" y2="28.1813" layer="200"/>
<rectangle x1="238.3663" y1="28.1051" x2="239.0521" y2="28.1813" layer="200"/>
<rectangle x1="212.3059" y1="28.1813" x2="214.3633" y2="28.2575" layer="200"/>
<rectangle x1="216.1159" y1="28.1813" x2="218.2495" y2="28.2575" layer="200"/>
<rectangle x1="220.3069" y1="28.1813" x2="220.9927" y2="28.2575" layer="200"/>
<rectangle x1="221.8309" y1="28.1813" x2="222.5929" y2="28.2575" layer="200"/>
<rectangle x1="223.6597" y1="28.1813" x2="224.3455" y2="28.2575" layer="200"/>
<rectangle x1="224.9551" y1="28.1813" x2="225.6409" y2="28.2575" layer="200"/>
<rectangle x1="226.4029" y1="28.1813" x2="227.1649" y2="28.2575" layer="200"/>
<rectangle x1="229.4509" y1="28.1813" x2="230.1367" y2="28.2575" layer="200"/>
<rectangle x1="231.5083" y1="28.1813" x2="231.8893" y2="28.2575" layer="200"/>
<rectangle x1="232.6513" y1="28.1813" x2="233.7181" y2="28.2575" layer="200"/>
<rectangle x1="234.4039" y1="28.1813" x2="235.1659" y2="28.2575" layer="200"/>
<rectangle x1="235.3183" y1="28.1813" x2="236.1565" y2="28.2575" layer="200"/>
<rectangle x1="238.3663" y1="28.1813" x2="239.0521" y2="28.2575" layer="200"/>
<rectangle x1="212.3059" y1="28.2575" x2="214.3633" y2="28.3337" layer="200"/>
<rectangle x1="216.1159" y1="28.2575" x2="218.2495" y2="28.3337" layer="200"/>
<rectangle x1="220.3069" y1="28.2575" x2="220.9927" y2="28.3337" layer="200"/>
<rectangle x1="221.8309" y1="28.2575" x2="222.5929" y2="28.3337" layer="200"/>
<rectangle x1="223.6597" y1="28.2575" x2="224.3455" y2="28.3337" layer="200"/>
<rectangle x1="224.9551" y1="28.2575" x2="225.6409" y2="28.3337" layer="200"/>
<rectangle x1="226.4029" y1="28.2575" x2="227.1649" y2="28.3337" layer="200"/>
<rectangle x1="229.4509" y1="28.2575" x2="230.1367" y2="28.3337" layer="200"/>
<rectangle x1="231.5845" y1="28.2575" x2="231.8893" y2="28.3337" layer="200"/>
<rectangle x1="232.6513" y1="28.2575" x2="233.7181" y2="28.3337" layer="200"/>
<rectangle x1="234.4039" y1="28.2575" x2="235.1659" y2="28.3337" layer="200"/>
<rectangle x1="235.3183" y1="28.2575" x2="236.0803" y2="28.3337" layer="200"/>
<rectangle x1="238.3663" y1="28.2575" x2="239.0521" y2="28.3337" layer="200"/>
<rectangle x1="212.3059" y1="28.3337" x2="214.3633" y2="28.4099" layer="200"/>
<rectangle x1="216.1159" y1="28.3337" x2="218.2495" y2="28.4099" layer="200"/>
<rectangle x1="220.3069" y1="28.3337" x2="220.9927" y2="28.4099" layer="200"/>
<rectangle x1="221.8309" y1="28.3337" x2="222.5929" y2="28.4099" layer="200"/>
<rectangle x1="223.6597" y1="28.3337" x2="224.3455" y2="28.4099" layer="200"/>
<rectangle x1="224.9551" y1="28.3337" x2="225.6409" y2="28.4099" layer="200"/>
<rectangle x1="226.4029" y1="28.3337" x2="227.1649" y2="28.4099" layer="200"/>
<rectangle x1="229.4509" y1="28.3337" x2="230.1367" y2="28.4099" layer="200"/>
<rectangle x1="231.5845" y1="28.3337" x2="231.8893" y2="28.4099" layer="200"/>
<rectangle x1="232.5751" y1="28.3337" x2="233.7943" y2="28.4099" layer="200"/>
<rectangle x1="234.4039" y1="28.3337" x2="235.1659" y2="28.4099" layer="200"/>
<rectangle x1="235.3183" y1="28.3337" x2="236.0803" y2="28.4099" layer="200"/>
<rectangle x1="238.3663" y1="28.3337" x2="239.0521" y2="28.4099" layer="200"/>
<rectangle x1="212.3059" y1="28.4099" x2="214.3633" y2="28.4861" layer="200"/>
<rectangle x1="216.1159" y1="28.4099" x2="218.2495" y2="28.4861" layer="200"/>
<rectangle x1="220.3069" y1="28.4099" x2="220.9927" y2="28.4861" layer="200"/>
<rectangle x1="221.8309" y1="28.4099" x2="222.5929" y2="28.4861" layer="200"/>
<rectangle x1="223.6597" y1="28.4099" x2="224.3455" y2="28.4861" layer="200"/>
<rectangle x1="224.9551" y1="28.4099" x2="225.6409" y2="28.4861" layer="200"/>
<rectangle x1="226.4029" y1="28.4099" x2="227.1649" y2="28.4861" layer="200"/>
<rectangle x1="229.4509" y1="28.4099" x2="230.1367" y2="28.4861" layer="200"/>
<rectangle x1="231.5845" y1="28.4099" x2="231.8893" y2="28.4861" layer="200"/>
<rectangle x1="232.5751" y1="28.4099" x2="233.3371" y2="28.4861" layer="200"/>
<rectangle x1="233.4133" y1="28.4099" x2="233.7943" y2="28.4861" layer="200"/>
<rectangle x1="234.4039" y1="28.4099" x2="235.1659" y2="28.4861" layer="200"/>
<rectangle x1="235.3183" y1="28.4099" x2="236.0803" y2="28.4861" layer="200"/>
<rectangle x1="238.3663" y1="28.4099" x2="239.0521" y2="28.4861" layer="200"/>
<rectangle x1="212.3059" y1="28.4861" x2="214.3633" y2="28.5623" layer="200"/>
<rectangle x1="216.1159" y1="28.4861" x2="218.2495" y2="28.5623" layer="200"/>
<rectangle x1="220.3069" y1="28.4861" x2="220.9927" y2="28.5623" layer="200"/>
<rectangle x1="221.8309" y1="28.4861" x2="222.5929" y2="28.5623" layer="200"/>
<rectangle x1="223.6597" y1="28.4861" x2="224.3455" y2="28.5623" layer="200"/>
<rectangle x1="224.9551" y1="28.4861" x2="225.6409" y2="28.5623" layer="200"/>
<rectangle x1="226.4029" y1="28.4861" x2="227.1649" y2="28.5623" layer="200"/>
<rectangle x1="229.4509" y1="28.4861" x2="230.1367" y2="28.5623" layer="200"/>
<rectangle x1="231.5845" y1="28.4861" x2="231.8893" y2="28.5623" layer="200"/>
<rectangle x1="232.4989" y1="28.4861" x2="233.3371" y2="28.5623" layer="200"/>
<rectangle x1="233.4133" y1="28.4861" x2="233.8705" y2="28.5623" layer="200"/>
<rectangle x1="234.4039" y1="28.4861" x2="235.0897" y2="28.5623" layer="200"/>
<rectangle x1="235.3183" y1="28.4861" x2="236.0803" y2="28.5623" layer="200"/>
<rectangle x1="238.3663" y1="28.4861" x2="239.0521" y2="28.5623" layer="200"/>
<rectangle x1="212.3059" y1="28.5623" x2="214.3633" y2="28.6385" layer="200"/>
<rectangle x1="216.1159" y1="28.5623" x2="218.1733" y2="28.6385" layer="200"/>
<rectangle x1="220.3069" y1="28.5623" x2="220.9927" y2="28.6385" layer="200"/>
<rectangle x1="221.8309" y1="28.5623" x2="222.5929" y2="28.6385" layer="200"/>
<rectangle x1="223.6597" y1="28.5623" x2="224.3455" y2="28.6385" layer="200"/>
<rectangle x1="224.9551" y1="28.5623" x2="225.6409" y2="28.6385" layer="200"/>
<rectangle x1="226.4029" y1="28.5623" x2="227.1649" y2="28.6385" layer="200"/>
<rectangle x1="229.4509" y1="28.5623" x2="230.1367" y2="28.6385" layer="200"/>
<rectangle x1="231.5845" y1="28.5623" x2="231.8893" y2="28.6385" layer="200"/>
<rectangle x1="232.4989" y1="28.5623" x2="233.2609" y2="28.6385" layer="200"/>
<rectangle x1="233.4133" y1="28.5623" x2="233.8705" y2="28.6385" layer="200"/>
<rectangle x1="234.4039" y1="28.5623" x2="235.0897" y2="28.6385" layer="200"/>
<rectangle x1="235.3183" y1="28.5623" x2="236.0803" y2="28.6385" layer="200"/>
<rectangle x1="238.3663" y1="28.5623" x2="239.0521" y2="28.6385" layer="200"/>
<rectangle x1="212.3059" y1="28.6385" x2="214.3633" y2="28.7147" layer="200"/>
<rectangle x1="216.1159" y1="28.6385" x2="218.1733" y2="28.7147" layer="200"/>
<rectangle x1="220.3069" y1="28.6385" x2="220.9927" y2="28.7147" layer="200"/>
<rectangle x1="221.8309" y1="28.6385" x2="222.5929" y2="28.7147" layer="200"/>
<rectangle x1="223.6597" y1="28.6385" x2="224.3455" y2="28.7147" layer="200"/>
<rectangle x1="224.9551" y1="28.6385" x2="225.6409" y2="28.7147" layer="200"/>
<rectangle x1="226.4029" y1="28.6385" x2="227.1649" y2="28.7147" layer="200"/>
<rectangle x1="229.4509" y1="28.6385" x2="230.1367" y2="28.7147" layer="200"/>
<rectangle x1="231.5845" y1="28.6385" x2="231.8893" y2="28.7147" layer="200"/>
<rectangle x1="232.4989" y1="28.6385" x2="233.2609" y2="28.7147" layer="200"/>
<rectangle x1="233.4895" y1="28.6385" x2="233.8705" y2="28.7147" layer="200"/>
<rectangle x1="234.4039" y1="28.6385" x2="235.0897" y2="28.7147" layer="200"/>
<rectangle x1="235.3183" y1="28.6385" x2="236.0803" y2="28.7147" layer="200"/>
<rectangle x1="238.3663" y1="28.6385" x2="239.0521" y2="28.7147" layer="200"/>
<rectangle x1="212.3059" y1="28.7147" x2="214.3633" y2="28.7909" layer="200"/>
<rectangle x1="216.1159" y1="28.7147" x2="218.1733" y2="28.7909" layer="200"/>
<rectangle x1="220.3069" y1="28.7147" x2="220.9927" y2="28.7909" layer="200"/>
<rectangle x1="221.8309" y1="28.7147" x2="222.5929" y2="28.7909" layer="200"/>
<rectangle x1="223.6597" y1="28.7147" x2="224.3455" y2="28.7909" layer="200"/>
<rectangle x1="224.9551" y1="28.7147" x2="225.6409" y2="28.7909" layer="200"/>
<rectangle x1="226.4791" y1="28.7147" x2="228.9175" y2="28.7909" layer="200"/>
<rectangle x1="229.4509" y1="28.7147" x2="230.1367" y2="28.7909" layer="200"/>
<rectangle x1="231.5845" y1="28.7147" x2="231.8893" y2="28.7909" layer="200"/>
<rectangle x1="232.4227" y1="28.7147" x2="233.2609" y2="28.7909" layer="200"/>
<rectangle x1="233.4895" y1="28.7147" x2="233.9467" y2="28.7909" layer="200"/>
<rectangle x1="234.4039" y1="28.7147" x2="235.0897" y2="28.7909" layer="200"/>
<rectangle x1="235.3945" y1="28.7147" x2="237.8329" y2="28.7909" layer="200"/>
<rectangle x1="238.3663" y1="28.7147" x2="239.0521" y2="28.7909" layer="200"/>
<rectangle x1="212.3059" y1="28.7909" x2="214.3633" y2="28.8671" layer="200"/>
<rectangle x1="216.1159" y1="28.7909" x2="218.1733" y2="28.8671" layer="200"/>
<rectangle x1="220.3069" y1="28.7909" x2="220.9927" y2="28.8671" layer="200"/>
<rectangle x1="221.8309" y1="28.7909" x2="222.5929" y2="28.8671" layer="200"/>
<rectangle x1="223.6597" y1="28.7909" x2="224.3455" y2="28.8671" layer="200"/>
<rectangle x1="224.9551" y1="28.7909" x2="225.6409" y2="28.8671" layer="200"/>
<rectangle x1="226.4791" y1="28.7909" x2="228.9937" y2="28.8671" layer="200"/>
<rectangle x1="229.4509" y1="28.7909" x2="230.1367" y2="28.8671" layer="200"/>
<rectangle x1="231.5845" y1="28.7909" x2="231.8893" y2="28.8671" layer="200"/>
<rectangle x1="232.4227" y1="28.7909" x2="233.1847" y2="28.8671" layer="200"/>
<rectangle x1="233.5657" y1="28.7909" x2="233.9467" y2="28.8671" layer="200"/>
<rectangle x1="234.4039" y1="28.7909" x2="235.0897" y2="28.8671" layer="200"/>
<rectangle x1="235.3945" y1="28.7909" x2="237.9853" y2="28.8671" layer="200"/>
<rectangle x1="238.3663" y1="28.7909" x2="239.0521" y2="28.8671" layer="200"/>
<rectangle x1="212.3821" y1="28.8671" x2="214.3633" y2="28.9433" layer="200"/>
<rectangle x1="216.1159" y1="28.8671" x2="218.1733" y2="28.9433" layer="200"/>
<rectangle x1="220.3069" y1="28.8671" x2="220.9927" y2="28.9433" layer="200"/>
<rectangle x1="221.8309" y1="28.8671" x2="222.5929" y2="28.9433" layer="200"/>
<rectangle x1="223.6597" y1="28.8671" x2="224.3455" y2="28.9433" layer="200"/>
<rectangle x1="224.9551" y1="28.8671" x2="225.6409" y2="28.9433" layer="200"/>
<rectangle x1="226.4791" y1="28.8671" x2="228.9937" y2="28.9433" layer="200"/>
<rectangle x1="229.4509" y1="28.8671" x2="230.1367" y2="28.9433" layer="200"/>
<rectangle x1="231.5845" y1="28.8671" x2="231.9655" y2="28.9433" layer="200"/>
<rectangle x1="232.4227" y1="28.8671" x2="233.1847" y2="28.9433" layer="200"/>
<rectangle x1="233.5657" y1="28.8671" x2="233.9467" y2="28.9433" layer="200"/>
<rectangle x1="234.4039" y1="28.8671" x2="235.0897" y2="28.9433" layer="200"/>
<rectangle x1="235.3945" y1="28.8671" x2="237.9853" y2="28.9433" layer="200"/>
<rectangle x1="238.3663" y1="28.8671" x2="239.0521" y2="28.9433" layer="200"/>
<rectangle x1="212.3821" y1="28.9433" x2="214.3633" y2="29.0195" layer="200"/>
<rectangle x1="216.1159" y1="28.9433" x2="218.1733" y2="29.0195" layer="200"/>
<rectangle x1="220.3069" y1="28.9433" x2="220.9927" y2="29.0195" layer="200"/>
<rectangle x1="221.8309" y1="28.9433" x2="222.5929" y2="29.0195" layer="200"/>
<rectangle x1="223.6597" y1="28.9433" x2="224.3455" y2="29.0195" layer="200"/>
<rectangle x1="224.9551" y1="28.9433" x2="225.6409" y2="29.0195" layer="200"/>
<rectangle x1="226.4791" y1="28.9433" x2="228.9937" y2="29.0195" layer="200"/>
<rectangle x1="229.4509" y1="28.9433" x2="230.2129" y2="29.0195" layer="200"/>
<rectangle x1="231.5845" y1="28.9433" x2="231.9655" y2="29.0195" layer="200"/>
<rectangle x1="232.3465" y1="28.9433" x2="233.1847" y2="29.0195" layer="200"/>
<rectangle x1="233.6419" y1="28.9433" x2="234.0229" y2="29.0195" layer="200"/>
<rectangle x1="234.4039" y1="28.9433" x2="235.0897" y2="29.0195" layer="200"/>
<rectangle x1="235.4707" y1="28.9433" x2="237.9853" y2="29.0195" layer="200"/>
<rectangle x1="238.3663" y1="28.9433" x2="239.0521" y2="29.0195" layer="200"/>
<rectangle x1="212.3821" y1="29.0195" x2="214.3633" y2="29.0957" layer="200"/>
<rectangle x1="216.1159" y1="29.0195" x2="218.0971" y2="29.0957" layer="200"/>
<rectangle x1="220.3069" y1="29.0195" x2="220.9927" y2="29.0957" layer="200"/>
<rectangle x1="221.8309" y1="29.0195" x2="222.5929" y2="29.0957" layer="200"/>
<rectangle x1="223.5835" y1="29.0195" x2="224.3455" y2="29.0957" layer="200"/>
<rectangle x1="224.9551" y1="29.0195" x2="225.6409" y2="29.0957" layer="200"/>
<rectangle x1="226.5553" y1="29.0195" x2="227.1649" y2="29.0957" layer="200"/>
<rectangle x1="228.0793" y1="29.0195" x2="228.9937" y2="29.0957" layer="200"/>
<rectangle x1="229.4509" y1="29.0195" x2="230.2129" y2="29.0957" layer="200"/>
<rectangle x1="231.5845" y1="29.0195" x2="231.9655" y2="29.0957" layer="200"/>
<rectangle x1="232.3465" y1="29.0195" x2="233.1085" y2="29.0957" layer="200"/>
<rectangle x1="233.6419" y1="29.0195" x2="234.0229" y2="29.0957" layer="200"/>
<rectangle x1="234.4039" y1="29.0195" x2="235.0897" y2="29.0957" layer="200"/>
<rectangle x1="235.4707" y1="29.0195" x2="236.1565" y2="29.0957" layer="200"/>
<rectangle x1="236.9947" y1="29.0195" x2="237.9853" y2="29.0957" layer="200"/>
<rectangle x1="238.3663" y1="29.0195" x2="239.0521" y2="29.0957" layer="200"/>
<rectangle x1="212.3821" y1="29.0957" x2="214.3633" y2="29.1719" layer="200"/>
<rectangle x1="216.1159" y1="29.0957" x2="218.0971" y2="29.1719" layer="200"/>
<rectangle x1="220.3069" y1="29.0957" x2="220.9927" y2="29.1719" layer="200"/>
<rectangle x1="221.8309" y1="29.0957" x2="222.5929" y2="29.1719" layer="200"/>
<rectangle x1="223.5835" y1="29.0957" x2="224.3455" y2="29.1719" layer="200"/>
<rectangle x1="224.9551" y1="29.0957" x2="225.6409" y2="29.1719" layer="200"/>
<rectangle x1="226.5553" y1="29.0957" x2="227.2411" y2="29.1719" layer="200"/>
<rectangle x1="228.2317" y1="29.0957" x2="228.9937" y2="29.1719" layer="200"/>
<rectangle x1="229.4509" y1="29.0957" x2="230.2129" y2="29.1719" layer="200"/>
<rectangle x1="230.6701" y1="29.0957" x2="231.1273" y2="29.1719" layer="200"/>
<rectangle x1="231.5845" y1="29.0957" x2="231.9655" y2="29.1719" layer="200"/>
<rectangle x1="232.2703" y1="29.0957" x2="233.1085" y2="29.1719" layer="200"/>
<rectangle x1="233.6419" y1="29.0957" x2="234.0991" y2="29.1719" layer="200"/>
<rectangle x1="234.4039" y1="29.0957" x2="235.0897" y2="29.1719" layer="200"/>
<rectangle x1="235.4707" y1="29.0957" x2="236.1565" y2="29.1719" layer="200"/>
<rectangle x1="237.1471" y1="29.0957" x2="237.9091" y2="29.1719" layer="200"/>
<rectangle x1="238.3663" y1="29.0957" x2="239.0521" y2="29.1719" layer="200"/>
<rectangle x1="212.4583" y1="29.1719" x2="214.3633" y2="29.2481" layer="200"/>
<rectangle x1="216.1159" y1="29.1719" x2="218.0209" y2="29.2481" layer="200"/>
<rectangle x1="220.3069" y1="29.1719" x2="220.9927" y2="29.2481" layer="200"/>
<rectangle x1="221.5261" y1="29.1719" x2="222.6691" y2="29.2481" layer="200"/>
<rectangle x1="223.5835" y1="29.1719" x2="224.3455" y2="29.2481" layer="200"/>
<rectangle x1="224.9551" y1="29.1719" x2="225.6409" y2="29.2481" layer="200"/>
<rectangle x1="226.6315" y1="29.1719" x2="227.2411" y2="29.2481" layer="200"/>
<rectangle x1="228.3079" y1="29.1719" x2="228.9937" y2="29.2481" layer="200"/>
<rectangle x1="229.1461" y1="29.1719" x2="230.2129" y2="29.2481" layer="200"/>
<rectangle x1="230.5939" y1="29.1719" x2="231.2797" y2="29.2481" layer="200"/>
<rectangle x1="231.5845" y1="29.1719" x2="231.9655" y2="29.2481" layer="200"/>
<rectangle x1="232.2703" y1="29.1719" x2="233.0323" y2="29.2481" layer="200"/>
<rectangle x1="233.7181" y1="29.1719" x2="234.0991" y2="29.2481" layer="200"/>
<rectangle x1="234.4039" y1="29.1719" x2="235.0897" y2="29.2481" layer="200"/>
<rectangle x1="235.5469" y1="29.1719" x2="236.1565" y2="29.2481" layer="200"/>
<rectangle x1="237.2233" y1="29.1719" x2="237.9091" y2="29.2481" layer="200"/>
<rectangle x1="238.3663" y1="29.1719" x2="239.0521" y2="29.2481" layer="200"/>
<rectangle x1="212.4583" y1="29.2481" x2="214.3633" y2="29.3243" layer="200"/>
<rectangle x1="216.1159" y1="29.2481" x2="218.0209" y2="29.3243" layer="200"/>
<rectangle x1="220.3069" y1="29.2481" x2="220.9927" y2="29.3243" layer="200"/>
<rectangle x1="221.5261" y1="29.2481" x2="222.7453" y2="29.3243" layer="200"/>
<rectangle x1="223.4311" y1="29.2481" x2="224.2693" y2="29.3243" layer="200"/>
<rectangle x1="224.9551" y1="29.2481" x2="225.6409" y2="29.3243" layer="200"/>
<rectangle x1="226.6315" y1="29.2481" x2="227.3173" y2="29.3243" layer="200"/>
<rectangle x1="228.3079" y1="29.2481" x2="228.9175" y2="29.3243" layer="200"/>
<rectangle x1="229.1461" y1="29.2481" x2="230.2891" y2="29.3243" layer="200"/>
<rectangle x1="230.4415" y1="29.2481" x2="231.2797" y2="29.3243" layer="200"/>
<rectangle x1="231.5845" y1="29.2481" x2="231.9655" y2="29.3243" layer="200"/>
<rectangle x1="232.1941" y1="29.2481" x2="233.0323" y2="29.3243" layer="200"/>
<rectangle x1="233.7181" y1="29.2481" x2="234.1753" y2="29.3243" layer="200"/>
<rectangle x1="234.4039" y1="29.2481" x2="235.0897" y2="29.3243" layer="200"/>
<rectangle x1="235.6231" y1="29.2481" x2="236.2327" y2="29.3243" layer="200"/>
<rectangle x1="237.2233" y1="29.2481" x2="237.9091" y2="29.3243" layer="200"/>
<rectangle x1="238.3663" y1="29.2481" x2="239.0521" y2="29.3243" layer="200"/>
<rectangle x1="212.5345" y1="29.3243" x2="214.3633" y2="29.4005" layer="200"/>
<rectangle x1="216.1159" y1="29.3243" x2="218.0209" y2="29.4005" layer="200"/>
<rectangle x1="220.3069" y1="29.3243" x2="220.9927" y2="29.4005" layer="200"/>
<rectangle x1="221.5261" y1="29.3243" x2="222.8977" y2="29.4005" layer="200"/>
<rectangle x1="223.2787" y1="29.3243" x2="224.2693" y2="29.4005" layer="200"/>
<rectangle x1="224.9551" y1="29.3243" x2="225.6409" y2="29.4005" layer="200"/>
<rectangle x1="226.7077" y1="29.3243" x2="227.3173" y2="29.4005" layer="200"/>
<rectangle x1="228.3079" y1="29.3243" x2="228.9175" y2="29.4005" layer="200"/>
<rectangle x1="229.1461" y1="29.3243" x2="231.2797" y2="29.4005" layer="200"/>
<rectangle x1="231.5845" y1="29.3243" x2="231.9655" y2="29.4005" layer="200"/>
<rectangle x1="232.1941" y1="29.3243" x2="232.9561" y2="29.4005" layer="200"/>
<rectangle x1="233.7943" y1="29.3243" x2="234.1753" y2="29.4005" layer="200"/>
<rectangle x1="234.4039" y1="29.3243" x2="235.0897" y2="29.4005" layer="200"/>
<rectangle x1="235.6993" y1="29.3243" x2="236.3089" y2="29.4005" layer="200"/>
<rectangle x1="237.2233" y1="29.3243" x2="237.8329" y2="29.4005" layer="200"/>
<rectangle x1="238.3663" y1="29.3243" x2="239.0521" y2="29.4005" layer="200"/>
<rectangle x1="212.5345" y1="29.4005" x2="214.3633" y2="29.4767" layer="200"/>
<rectangle x1="216.1159" y1="29.4005" x2="217.9447" y2="29.4767" layer="200"/>
<rectangle x1="220.3069" y1="29.4005" x2="220.9927" y2="29.4767" layer="200"/>
<rectangle x1="221.6785" y1="29.4005" x2="224.1931" y2="29.4767" layer="200"/>
<rectangle x1="224.5741" y1="29.4005" x2="226.3267" y2="29.4767" layer="200"/>
<rectangle x1="226.7839" y1="29.4005" x2="227.3935" y2="29.4767" layer="200"/>
<rectangle x1="228.2317" y1="29.4005" x2="228.8413" y2="29.4767" layer="200"/>
<rectangle x1="229.2985" y1="29.4005" x2="231.2797" y2="29.4767" layer="200"/>
<rectangle x1="231.5845" y1="29.4005" x2="231.9655" y2="29.4767" layer="200"/>
<rectangle x1="232.1941" y1="29.4005" x2="232.9561" y2="29.4767" layer="200"/>
<rectangle x1="233.7943" y1="29.4005" x2="234.1753" y2="29.4767" layer="200"/>
<rectangle x1="234.4039" y1="29.4005" x2="235.0897" y2="29.4767" layer="200"/>
<rectangle x1="235.6993" y1="29.4005" x2="236.3851" y2="29.4767" layer="200"/>
<rectangle x1="237.1471" y1="29.4005" x2="237.7567" y2="29.4767" layer="200"/>
<rectangle x1="237.9853" y1="29.4005" x2="239.7379" y2="29.4767" layer="200"/>
<rectangle x1="212.6107" y1="29.4767" x2="214.3633" y2="29.5529" layer="200"/>
<rectangle x1="216.1159" y1="29.4767" x2="217.9447" y2="29.5529" layer="200"/>
<rectangle x1="220.3069" y1="29.4767" x2="220.9927" y2="29.5529" layer="200"/>
<rectangle x1="221.8309" y1="29.4767" x2="222.5167" y2="29.5529" layer="200"/>
<rectangle x1="222.5929" y1="29.4767" x2="224.1931" y2="29.5529" layer="200"/>
<rectangle x1="224.5741" y1="29.4767" x2="226.3267" y2="29.5529" layer="200"/>
<rectangle x1="226.8601" y1="29.4767" x2="227.5459" y2="29.5529" layer="200"/>
<rectangle x1="228.1555" y1="29.4767" x2="228.7651" y2="29.5529" layer="200"/>
<rectangle x1="229.4509" y1="29.4767" x2="230.1367" y2="29.5529" layer="200"/>
<rectangle x1="230.2129" y1="29.4767" x2="231.2797" y2="29.5529" layer="200"/>
<rectangle x1="231.6607" y1="29.4767" x2="231.9655" y2="29.5529" layer="200"/>
<rectangle x1="232.1179" y1="29.4767" x2="232.9561" y2="29.5529" layer="200"/>
<rectangle x1="233.8705" y1="29.4767" x2="234.2515" y2="29.5529" layer="200"/>
<rectangle x1="234.4039" y1="29.4767" x2="235.0897" y2="29.5529" layer="200"/>
<rectangle x1="235.7755" y1="29.4767" x2="236.4613" y2="29.5529" layer="200"/>
<rectangle x1="237.0709" y1="29.4767" x2="237.6805" y2="29.5529" layer="200"/>
<rectangle x1="237.9853" y1="29.4767" x2="239.7379" y2="29.5529" layer="200"/>
<rectangle x1="212.6107" y1="29.5529" x2="214.3633" y2="29.6291" layer="200"/>
<rectangle x1="216.1159" y1="29.5529" x2="217.8685" y2="29.6291" layer="200"/>
<rectangle x1="220.3069" y1="29.5529" x2="220.9927" y2="29.6291" layer="200"/>
<rectangle x1="221.9833" y1="29.5529" x2="222.5167" y2="29.6291" layer="200"/>
<rectangle x1="222.6691" y1="29.5529" x2="224.1169" y2="29.6291" layer="200"/>
<rectangle x1="224.6503" y1="29.5529" x2="226.3267" y2="29.6291" layer="200"/>
<rectangle x1="227.0125" y1="29.5529" x2="227.7745" y2="29.6291" layer="200"/>
<rectangle x1="227.9269" y1="29.5529" x2="228.6889" y2="29.6291" layer="200"/>
<rectangle x1="229.6033" y1="29.5529" x2="230.1367" y2="29.6291" layer="200"/>
<rectangle x1="230.2891" y1="29.5529" x2="231.2797" y2="29.6291" layer="200"/>
<rectangle x1="231.6607" y1="29.5529" x2="231.9655" y2="29.6291" layer="200"/>
<rectangle x1="232.1179" y1="29.5529" x2="232.8799" y2="29.6291" layer="200"/>
<rectangle x1="233.8705" y1="29.5529" x2="234.2515" y2="29.6291" layer="200"/>
<rectangle x1="234.4039" y1="29.5529" x2="235.0897" y2="29.6291" layer="200"/>
<rectangle x1="235.9279" y1="29.5529" x2="236.6899" y2="29.6291" layer="200"/>
<rectangle x1="236.8423" y1="29.5529" x2="237.6043" y2="29.6291" layer="200"/>
<rectangle x1="237.9853" y1="29.5529" x2="239.7379" y2="29.6291" layer="200"/>
<rectangle x1="212.6869" y1="29.6291" x2="214.3633" y2="29.7053" layer="200"/>
<rectangle x1="216.1159" y1="29.6291" x2="217.8685" y2="29.7053" layer="200"/>
<rectangle x1="220.3069" y1="29.6291" x2="220.9927" y2="29.7053" layer="200"/>
<rectangle x1="222.1357" y1="29.6291" x2="222.5167" y2="29.7053" layer="200"/>
<rectangle x1="222.7453" y1="29.6291" x2="223.9645" y2="29.7053" layer="200"/>
<rectangle x1="224.7265" y1="29.6291" x2="226.3267" y2="29.7053" layer="200"/>
<rectangle x1="227.0887" y1="29.6291" x2="228.5365" y2="29.7053" layer="200"/>
<rectangle x1="229.6795" y1="29.6291" x2="230.1367" y2="29.7053" layer="200"/>
<rectangle x1="230.3653" y1="29.6291" x2="231.2035" y2="29.7053" layer="200"/>
<rectangle x1="231.6607" y1="29.6291" x2="231.9655" y2="29.7053" layer="200"/>
<rectangle x1="232.1179" y1="29.6291" x2="232.8799" y2="29.7053" layer="200"/>
<rectangle x1="233.8705" y1="29.6291" x2="234.2515" y2="29.7053" layer="200"/>
<rectangle x1="234.4039" y1="29.6291" x2="235.0897" y2="29.7053" layer="200"/>
<rectangle x1="236.0041" y1="29.6291" x2="237.5281" y2="29.7053" layer="200"/>
<rectangle x1="238.1377" y1="29.6291" x2="239.7379" y2="29.7053" layer="200"/>
<rectangle x1="212.6869" y1="29.7053" x2="214.3633" y2="29.7815" layer="200"/>
<rectangle x1="216.1159" y1="29.7053" x2="217.7923" y2="29.7815" layer="200"/>
<rectangle x1="220.3069" y1="29.7053" x2="220.9927" y2="29.7815" layer="200"/>
<rectangle x1="222.2119" y1="29.7053" x2="222.5167" y2="29.7815" layer="200"/>
<rectangle x1="222.9739" y1="29.7053" x2="223.8883" y2="29.7815" layer="200"/>
<rectangle x1="224.8789" y1="29.7053" x2="225.6409" y2="29.7815" layer="200"/>
<rectangle x1="227.3173" y1="29.7053" x2="228.3841" y2="29.7815" layer="200"/>
<rectangle x1="229.8319" y1="29.7053" x2="230.1367" y2="29.7815" layer="200"/>
<rectangle x1="230.4415" y1="29.7053" x2="231.1273" y2="29.7815" layer="200"/>
<rectangle x1="231.6607" y1="29.7053" x2="231.9655" y2="29.7815" layer="200"/>
<rectangle x1="232.0417" y1="29.7053" x2="232.8037" y2="29.7815" layer="200"/>
<rectangle x1="233.9467" y1="29.7053" x2="234.3277" y2="29.7815" layer="200"/>
<rectangle x1="234.4039" y1="29.7053" x2="235.0897" y2="29.7815" layer="200"/>
<rectangle x1="236.2327" y1="29.7053" x2="237.2995" y2="29.7815" layer="200"/>
<rectangle x1="238.2901" y1="29.7053" x2="239.0521" y2="29.7815" layer="200"/>
<rectangle x1="212.7631" y1="29.7815" x2="214.3633" y2="29.8577" layer="200"/>
<rectangle x1="216.1159" y1="29.7815" x2="217.7161" y2="29.8577" layer="200"/>
<rectangle x1="220.3069" y1="29.7815" x2="220.9927" y2="29.8577" layer="200"/>
<rectangle x1="222.3643" y1="29.7815" x2="222.5167" y2="29.8577" layer="200"/>
<rectangle x1="223.2025" y1="29.7815" x2="223.6597" y2="29.8577" layer="200"/>
<rectangle x1="225.0313" y1="29.7815" x2="225.6409" y2="29.8577" layer="200"/>
<rectangle x1="227.5459" y1="29.7815" x2="228.0793" y2="29.8577" layer="200"/>
<rectangle x1="229.9081" y1="29.7815" x2="230.1367" y2="29.8577" layer="200"/>
<rectangle x1="230.5939" y1="29.7815" x2="230.9749" y2="29.8577" layer="200"/>
<rectangle x1="231.6607" y1="29.7815" x2="231.9655" y2="29.8577" layer="200"/>
<rectangle x1="232.0417" y1="29.7815" x2="232.8037" y2="29.8577" layer="200"/>
<rectangle x1="233.9467" y1="29.7815" x2="234.3277" y2="29.8577" layer="200"/>
<rectangle x1="234.4039" y1="29.7815" x2="235.0897" y2="29.8577" layer="200"/>
<rectangle x1="236.4613" y1="29.7815" x2="237.0709" y2="29.8577" layer="200"/>
<rectangle x1="238.3663" y1="29.7815" x2="239.0521" y2="29.8577" layer="200"/>
<rectangle x1="212.8393" y1="29.8577" x2="214.3633" y2="29.9339" layer="200"/>
<rectangle x1="216.1159" y1="29.8577" x2="217.7161" y2="29.9339" layer="200"/>
<rectangle x1="220.3069" y1="29.8577" x2="220.9927" y2="29.9339" layer="200"/>
<rectangle x1="225.1075" y1="29.8577" x2="225.6409" y2="29.9339" layer="200"/>
<rectangle x1="231.6607" y1="29.8577" x2="232.8037" y2="29.9339" layer="200"/>
<rectangle x1="234.0229" y1="29.8577" x2="235.0897" y2="29.9339" layer="200"/>
<rectangle x1="238.4425" y1="29.8577" x2="239.0521" y2="29.9339" layer="200"/>
<rectangle x1="212.8393" y1="29.9339" x2="214.3633" y2="30.0101" layer="200"/>
<rectangle x1="216.1159" y1="29.9339" x2="217.6399" y2="30.0101" layer="200"/>
<rectangle x1="220.3069" y1="29.9339" x2="220.9927" y2="30.0101" layer="200"/>
<rectangle x1="225.1837" y1="29.9339" x2="225.6409" y2="30.0101" layer="200"/>
<rectangle x1="231.6607" y1="29.9339" x2="232.7275" y2="30.0101" layer="200"/>
<rectangle x1="234.0229" y1="29.9339" x2="235.0897" y2="30.0101" layer="200"/>
<rectangle x1="238.5949" y1="29.9339" x2="239.0521" y2="30.0101" layer="200"/>
<rectangle x1="212.9155" y1="30.0101" x2="214.3633" y2="30.0863" layer="200"/>
<rectangle x1="216.1159" y1="30.0101" x2="217.5637" y2="30.0863" layer="200"/>
<rectangle x1="220.3069" y1="30.0101" x2="220.9927" y2="30.0863" layer="200"/>
<rectangle x1="225.2599" y1="30.0101" x2="225.6409" y2="30.0863" layer="200"/>
<rectangle x1="231.6607" y1="30.0101" x2="232.7275" y2="30.0863" layer="200"/>
<rectangle x1="234.0229" y1="30.0101" x2="235.0897" y2="30.0863" layer="200"/>
<rectangle x1="238.6711" y1="30.0101" x2="239.0521" y2="30.0863" layer="200"/>
<rectangle x1="212.9917" y1="30.0863" x2="214.3633" y2="30.1625" layer="200"/>
<rectangle x1="216.1159" y1="30.0863" x2="217.4875" y2="30.1625" layer="200"/>
<rectangle x1="220.3069" y1="30.0863" x2="220.9927" y2="30.1625" layer="200"/>
<rectangle x1="225.2599" y1="30.0863" x2="225.6409" y2="30.1625" layer="200"/>
<rectangle x1="231.6607" y1="30.0863" x2="232.6513" y2="30.1625" layer="200"/>
<rectangle x1="234.0991" y1="30.0863" x2="235.0897" y2="30.1625" layer="200"/>
<rectangle x1="238.6711" y1="30.0863" x2="239.0521" y2="30.1625" layer="200"/>
<rectangle x1="213.0679" y1="30.1625" x2="214.3633" y2="30.2387" layer="200"/>
<rectangle x1="216.1159" y1="30.1625" x2="217.4113" y2="30.2387" layer="200"/>
<rectangle x1="220.3069" y1="30.1625" x2="220.9927" y2="30.2387" layer="200"/>
<rectangle x1="225.3361" y1="30.1625" x2="225.6409" y2="30.2387" layer="200"/>
<rectangle x1="231.6607" y1="30.1625" x2="232.6513" y2="30.2387" layer="200"/>
<rectangle x1="234.0991" y1="30.1625" x2="235.0897" y2="30.2387" layer="200"/>
<rectangle x1="238.7473" y1="30.1625" x2="239.0521" y2="30.2387" layer="200"/>
<rectangle x1="213.1441" y1="30.2387" x2="214.3633" y2="30.3149" layer="200"/>
<rectangle x1="216.1159" y1="30.2387" x2="217.3351" y2="30.3149" layer="200"/>
<rectangle x1="220.3069" y1="30.2387" x2="220.9927" y2="30.3149" layer="200"/>
<rectangle x1="225.4885" y1="30.2387" x2="225.5647" y2="30.3149" layer="200"/>
<rectangle x1="231.6607" y1="30.2387" x2="232.5751" y2="30.3149" layer="200"/>
<rectangle x1="234.1753" y1="30.2387" x2="235.0897" y2="30.3149" layer="200"/>
<rectangle x1="238.8997" y1="30.2387" x2="238.9759" y2="30.3149" layer="200"/>
<rectangle x1="213.2203" y1="30.3149" x2="214.3633" y2="30.3911" layer="200"/>
<rectangle x1="216.1159" y1="30.3149" x2="217.2589" y2="30.3911" layer="200"/>
<rectangle x1="220.3069" y1="30.3149" x2="220.9927" y2="30.3911" layer="200"/>
<rectangle x1="231.6607" y1="30.3149" x2="232.5751" y2="30.3911" layer="200"/>
<rectangle x1="234.1753" y1="30.3149" x2="235.0897" y2="30.3911" layer="200"/>
<rectangle x1="213.2965" y1="30.3911" x2="214.3633" y2="30.4673" layer="200"/>
<rectangle x1="216.1159" y1="30.3911" x2="217.1827" y2="30.4673" layer="200"/>
<rectangle x1="220.2307" y1="30.3911" x2="220.9927" y2="30.4673" layer="200"/>
<rectangle x1="231.6607" y1="30.3911" x2="232.5751" y2="30.4673" layer="200"/>
<rectangle x1="234.1753" y1="30.3911" x2="235.0897" y2="30.4673" layer="200"/>
<rectangle x1="213.3727" y1="30.4673" x2="214.3633" y2="30.5435" layer="200"/>
<rectangle x1="216.1159" y1="30.4673" x2="217.1065" y2="30.5435" layer="200"/>
<rectangle x1="220.2307" y1="30.4673" x2="221.0689" y2="30.5435" layer="200"/>
<rectangle x1="231.5845" y1="30.4673" x2="232.4989" y2="30.5435" layer="200"/>
<rectangle x1="234.2515" y1="30.4673" x2="235.1659" y2="30.5435" layer="200"/>
<rectangle x1="213.5251" y1="30.5435" x2="214.3633" y2="30.6197" layer="200"/>
<rectangle x1="216.1159" y1="30.5435" x2="217.0303" y2="30.6197" layer="200"/>
<rectangle x1="219.8497" y1="30.5435" x2="221.4499" y2="30.6197" layer="200"/>
<rectangle x1="231.2035" y1="30.5435" x2="232.4989" y2="30.6197" layer="200"/>
<rectangle x1="234.2515" y1="30.5435" x2="235.5469" y2="30.6197" layer="200"/>
<rectangle x1="213.6013" y1="30.6197" x2="214.3633" y2="30.6959" layer="200"/>
<rectangle x1="216.1159" y1="30.6197" x2="216.8779" y2="30.6959" layer="200"/>
<rectangle x1="219.7735" y1="30.6197" x2="221.5261" y2="30.6959" layer="200"/>
<rectangle x1="231.2035" y1="30.6197" x2="232.4989" y2="30.6959" layer="200"/>
<rectangle x1="234.3277" y1="30.6197" x2="235.5469" y2="30.6959" layer="200"/>
<rectangle x1="213.7537" y1="30.6959" x2="214.3633" y2="30.7721" layer="200"/>
<rectangle x1="216.1159" y1="30.6959" x2="216.8017" y2="30.7721" layer="200"/>
<rectangle x1="219.7735" y1="30.6959" x2="221.5261" y2="30.7721" layer="200"/>
<rectangle x1="231.2035" y1="30.6959" x2="232.4227" y2="30.7721" layer="200"/>
<rectangle x1="234.3277" y1="30.6959" x2="235.5469" y2="30.7721" layer="200"/>
<rectangle x1="213.8299" y1="30.7721" x2="214.3633" y2="30.8483" layer="200"/>
<rectangle x1="216.1159" y1="30.7721" x2="216.6493" y2="30.8483" layer="200"/>
<rectangle x1="219.8497" y1="30.7721" x2="220.0783" y2="30.8483" layer="200"/>
<rectangle x1="221.2213" y1="30.7721" x2="221.4499" y2="30.8483" layer="200"/>
<rectangle x1="231.3559" y1="30.7721" x2="232.3465" y2="30.8483" layer="200"/>
<rectangle x1="234.4801" y1="30.7721" x2="235.3945" y2="30.8483" layer="200"/>
<rectangle x1="214.0585" y1="30.8483" x2="214.3633" y2="30.9245" layer="200"/>
<rectangle x1="216.1159" y1="30.8483" x2="216.4969" y2="30.9245" layer="200"/>
<rectangle x1="214.2109" y1="30.9245" x2="214.3633" y2="31.0007" layer="200"/>
<rectangle x1="216.1159" y1="30.9245" x2="216.3445" y2="31.0007" layer="200"/>
<rectangle x1="214.8967" y1="31.2293" x2="215.5825" y2="31.3055" layer="200"/>
<rectangle x1="214.6681" y1="31.3055" x2="215.8111" y2="31.3817" layer="200"/>
<rectangle x1="214.5919" y1="31.3817" x2="215.8873" y2="31.4579" layer="200"/>
<rectangle x1="214.5157" y1="31.4579" x2="216.0397" y2="31.5341" layer="200"/>
<rectangle x1="214.3633" y1="31.5341" x2="216.1159" y2="31.6103" layer="200"/>
<rectangle x1="214.3633" y1="31.6103" x2="216.1159" y2="31.6865" layer="200"/>
<rectangle x1="214.2871" y1="31.6865" x2="216.1921" y2="31.7627" layer="200"/>
<rectangle x1="214.2109" y1="31.7627" x2="216.2683" y2="31.8389" layer="200"/>
<rectangle x1="214.2109" y1="31.8389" x2="216.2683" y2="31.9151" layer="200"/>
<rectangle x1="214.1347" y1="31.9151" x2="216.3445" y2="31.9913" layer="200"/>
<rectangle x1="214.1347" y1="31.9913" x2="216.3445" y2="32.0675" layer="200"/>
<rectangle x1="214.1347" y1="32.0675" x2="216.4207" y2="32.1437" layer="200"/>
<rectangle x1="214.1347" y1="32.1437" x2="216.4207" y2="32.2199" layer="200"/>
<rectangle x1="214.0585" y1="32.2199" x2="216.4207" y2="32.2961" layer="200"/>
<rectangle x1="214.0585" y1="32.2961" x2="216.4207" y2="32.3723" layer="200"/>
<rectangle x1="214.0585" y1="32.3723" x2="216.4207" y2="32.4485" layer="200"/>
<rectangle x1="214.0585" y1="32.4485" x2="216.4207" y2="32.5247" layer="200"/>
<rectangle x1="214.1347" y1="32.5247" x2="216.4207" y2="32.6009" layer="200"/>
<rectangle x1="214.1347" y1="32.6009" x2="216.4207" y2="32.6771" layer="200"/>
<rectangle x1="214.1347" y1="32.6771" x2="216.3445" y2="32.7533" layer="200"/>
<rectangle x1="214.1347" y1="32.7533" x2="216.3445" y2="32.8295" layer="200"/>
<rectangle x1="214.2109" y1="32.8295" x2="216.3445" y2="32.9057" layer="200"/>
<rectangle x1="214.2109" y1="32.9057" x2="216.2683" y2="32.9819" layer="200"/>
<rectangle x1="214.2871" y1="32.9819" x2="216.2683" y2="33.0581" layer="200"/>
<rectangle x1="214.3633" y1="33.0581" x2="216.1921" y2="33.1343" layer="200"/>
<rectangle x1="214.3633" y1="33.1343" x2="216.1159" y2="33.2105" layer="200"/>
<rectangle x1="214.5157" y1="33.2105" x2="216.0397" y2="33.2867" layer="200"/>
<rectangle x1="214.5919" y1="33.2867" x2="215.9635" y2="33.3629" layer="200"/>
<rectangle x1="214.6681" y1="33.3629" x2="215.8111" y2="33.4391" layer="200"/>
<rectangle x1="214.8967" y1="33.4391" x2="215.6587" y2="33.5153" layer="200"/>
</symbol>
<symbol name="IQ_PROBE">
<pin name="GND@1" x="10.16" y="15.24" length="middle" rot="R180"/>
<pin name="GND@2" x="10.16" y="12.7" length="middle" rot="R180"/>
<pin name="PT100+" x="10.16" y="10.16" length="middle" rot="R180"/>
<pin name="NTC+" x="10.16" y="7.62" length="middle" rot="R180"/>
<pin name="SDA" x="10.16" y="5.08" length="middle" rot="R180"/>
<pin name="GND@6" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="VDD" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="SCL" x="10.16" y="-2.54" length="middle" rot="R180"/>
<pin name="NTC-" x="10.16" y="-5.08" length="middle" rot="R180"/>
<pin name="PT100-" x="10.16" y="-7.62" length="middle" rot="R180"/>
<pin name="GND@11" x="10.16" y="-10.16" length="middle" rot="R180"/>
<pin name="GND@12" x="10.16" y="-12.7" length="middle" rot="R180"/>
<text x="-7.62" y="19.05" size="1.778" layer="95" rot="MR0" align="center">&gt;NAME</text>
<wire x1="5.08" y1="17.78" x2="-7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="-15.24" x2="5.08" y2="17.78" width="0.254" layer="94"/>
<pin name="M1" x="-12.7" y="0" visible="pad" length="middle"/>
<pin name="M2" x="-12.7" y="-5.08" visible="pad" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="IMET3_CARDEDGE" prefix="J">
<description>iMet-3 Card-Edge Connector 4 CT 0.100"</description>
<gates>
<gate name="G$1" symbol="IMET3_EDGE" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="IMET3_CARDEDGE">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="GPIO" pad="6"/>
<connect gate="G$1" pin="RX" pad="2"/>
<connect gate="G$1" pin="SWCLK" pad="8"/>
<connect gate="G$1" pin="SWDIO" pad="5"/>
<connect gate="G$1" pin="TX" pad="3"/>
<connect gate="G$1" pin="V+" pad="1"/>
<connect gate="G$1" pin="VDD" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="403_WHIP" prefix="ANT">
<gates>
<gate name="G$1" symbol="ANTENNA" x="0" y="0"/>
</gates>
<devices>
<device name="_SMT" package="WHIP">
<connects>
<connect gate="G$1" pin="ANTENNA" pad="ANT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FRAME_8.5X11" prefix="FRAME">
<gates>
<gate name="G$1" symbol="FRAME_8.5X11" x="139.7" y="109.22"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IQ_PROBE" prefix="J">
<gates>
<gate name="G$1" symbol="IQ_PROBE" x="0" y="0"/>
</gates>
<devices>
<device name="_CARDEDGE" package="IQ_PROBE_12P">
<connects>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@11" pad="11"/>
<connect gate="G$1" pin="GND@12" pad="12"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@6" pad="6"/>
<connect gate="G$1" pin="NTC+" pad="4"/>
<connect gate="G$1" pin="NTC-" pad="9"/>
<connect gate="G$1" pin="PT100+" pad="3"/>
<connect gate="G$1" pin="PT100-" pad="10"/>
<connect gate="G$1" pin="SCL" pad="8"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VDD" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_12FPZ-SM-TF" package="JST_12FPZ-SM-TF">
<connects>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@11" pad="11"/>
<connect gate="G$1" pin="GND@12" pad="12"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@6" pad="6"/>
<connect gate="G$1" pin="M1" pad="M1"/>
<connect gate="G$1" pin="M2" pad="M2"/>
<connect gate="G$1" pin="NTC+" pad="4"/>
<connect gate="G$1" pin="NTC-" pad="9"/>
<connect gate="G$1" pin="PT100+" pad="3"/>
<connect gate="G$1" pin="PT100-" pad="10"/>
<connect gate="G$1" pin="SCL" pad="8"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VDD" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="battery_IMS">
<packages>
<package name="CR-2/3A_FLAT">
<wire x1="-18.3261" y1="2.159" x2="-18.3261" y2="-2.159" width="0.508" layer="46"/>
<wire x1="18.3261" y1="2.159" x2="18.3261" y2="-2.159" width="0.508" layer="46"/>
<wire x1="-16.1" y1="8.5" x2="16.1" y2="8.5" width="0.25" layer="51"/>
<wire x1="16.1" y1="8.5" x2="16.1" y2="-8.5" width="0.25" layer="51"/>
<wire x1="-16.1" y1="-8.5" x2="16.1" y2="-8.5" width="0.25" layer="51"/>
<wire x1="-16.1" y1="8.5" x2="-16.1" y2="-8.5" width="0.25" layer="51"/>
<smd name="M2" x="-18.3261" y="0" dx="6" dy="2.54" layer="1" rot="R90" cream="no"/>
<smd name="M1" x="18.3261" y="0" dx="6" dy="2.54" layer="1" rot="R90" cream="no"/>
<smd name="MINUS" x="-18.3261" y="0" dx="6" dy="2.54" layer="16" rot="R90" cream="no"/>
<smd name="PLUS" x="18.3261" y="0" dx="6" dy="2.54" layer="16" rot="R90" cream="no"/>
<wire x1="11.5" y1="2" x2="11.5" y2="-2" width="0.5" layer="51"/>
<wire x1="9.5" y1="0" x2="13.5" y2="0" width="0.5" layer="51"/>
<wire x1="-12.63" y1="2" x2="-12.63" y2="-2" width="0.5" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="BATTERY_2TAB">
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="0" width="0.4064" layer="94"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="3.175" y1="2.54" x2="3.175" y2="0" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0" x2="3.175" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.4064" layer="94"/>
<text x="-3.81" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<pin name="M1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<wire x1="5.715" y1="-1.27" x2="5.715" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="5.08" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-5.715" y2="-1.905" width="0.254" layer="94"/>
<pin name="P1" x="7.62" y="0" visible="off" length="middle" direction="pas" rot="R180"/>
<wire x1="7.62" y1="0" x2="3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BATTERY_FLAT_TAB" prefix="BAT">
<gates>
<gate name="G$1" symbol="BATTERY_2TAB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CR-2/3A_FLAT">
<connects>
<connect gate="G$1" pin="M1" pad="MINUS"/>
<connect gate="G$1" pin="P1" pad="PLUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="+3V3" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V" prefix="3V">
<gates>
<gate name="G$1" symbol="+3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="switch" urn="urn:adsk.eagle:library:380">
<description>&lt;b&gt;Switches&lt;/b&gt;&lt;p&gt;
Marquardt, Siemens, C&amp;K, ITT, and others&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SMD/SMT">
<smd name="1" x="2.23" y="4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="2.23" y="-4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="-2.23" y="4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-2.23" y="-4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.127" layer="21"/>
<text x="-4" y="0" size="1.27" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="4" y="0" size="1.27" layer="27" font="vector" ratio="15" rot="R90" align="center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="NORMOPEN">
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="3.048" y2="1.778" width="0.2032" layer="94"/>
<wire x1="3.048" y1="0.508" x2="3.048" y2="0" width="0.2032" layer="94"/>
<wire x1="0.762" y1="1.016" x2="0.762" y2="1.524" width="0.2032" layer="94"/>
<wire x1="0.762" y1="2.032" x2="0.762" y2="2.794" width="0.2032" layer="94"/>
<wire x1="0.762" y1="2.794" x2="0.762" y2="3.048" width="0.2032" layer="94"/>
<wire x1="0.762" y1="3.556" x2="0.762" y2="4.064" width="0.2032" layer="94"/>
<wire x1="1.27" y1="4.064" x2="0.762" y2="4.064" width="0.2032" layer="94"/>
<wire x1="0.762" y1="4.064" x2="0.254" y2="4.064" width="0.2032" layer="94"/>
<wire x1="0.254" y1="2.286" x2="0.762" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.794" x2="1.27" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.2032" layer="94"/>
<wire x1="3.048" y1="0" x2="5.08" y2="0" width="0.2032" layer="94"/>
<circle x="-2.54" y="0" radius="0.508" width="0" layer="94"/>
<circle x="5.08" y="0" radius="0.508" width="0" layer="94"/>
<text x="-3.048" y="4.826" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-5.588" size="1.778" layer="96">&gt;VALUE</text>
<pin name="0@A" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="0@B" x="-5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="1@B" x="7.62" y="-2.54" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="1@A" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TE3-1437565-0" prefix="SW">
<description>Pushbutton Switches SPST OFF(ON) RND SMT MINI PB TACT SWITCH</description>
<gates>
<gate name="G$1" symbol="NORMOPEN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD/SMT">
<connects>
<connect gate="G$1" pin="0@A" pad="1"/>
<connect gate="G$1" pin="0@B" pad="2"/>
<connect gate="G$1" pin="1@A" pad="3"/>
<connect gate="G$1" pin="1@B" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="texas" urn="urn:adsk.eagle:library:387">
<description>&lt;b&gt;Texas Instruments Devices&lt;/b&gt;&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="QFN(20)">
<wire x1="-1.48" y1="1.05" x2="-1.48" y2="0.95" width="0.15" layer="51" curve="-180"/>
<wire x1="-1.48" y1="0.55" x2="-1.48" y2="0.45" width="0.15" layer="51" curve="-180"/>
<wire x1="-1.48" y1="0.05" x2="-1.48" y2="-0.05" width="0.15" layer="51" curve="-180"/>
<wire x1="-1.48" y1="-0.45" x2="-1.48" y2="-0.55" width="0.15" layer="51" curve="-180"/>
<wire x1="-1.48" y1="-0.95" x2="-1.48" y2="-1.05" width="0.15" layer="51" curve="-180"/>
<wire x1="-1.03" y1="-1.5" x2="-0.93" y2="-1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="-0.53" y1="-1.5" x2="-0.43" y2="-1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="-0.03" y1="-1.5" x2="0.07" y2="-1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="0.47" y1="-1.5" x2="0.57" y2="-1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="0.97" y1="-1.5" x2="1.07" y2="-1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="1.52" y1="-1.05" x2="1.52" y2="-0.95" width="0.15" layer="51" curve="-180"/>
<wire x1="1.52" y1="-0.55" x2="1.52" y2="-0.45" width="0.15" layer="51" curve="-180"/>
<wire x1="1.52" y1="-0.05" x2="1.52" y2="0.05" width="0.15" layer="51" curve="-180"/>
<wire x1="1.52" y1="0.45" x2="1.52" y2="0.55" width="0.15" layer="51" curve="-180"/>
<wire x1="1.52" y1="0.95" x2="1.52" y2="1.05" width="0.15" layer="51" curve="-180"/>
<wire x1="1.07" y1="1.5" x2="0.97" y2="1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="0.57" y1="1.5" x2="0.47" y2="1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="0.07" y1="1.5" x2="-0.03" y2="1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="-0.43" y1="1.5" x2="-0.53" y2="1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="-0.93" y1="1.5" x2="-1.03" y2="1.5" width="0.15" layer="51" curve="-180"/>
<wire x1="-1.93" y1="1.95" x2="1.97" y2="1.95" width="0.1016" layer="51"/>
<wire x1="1.97" y1="1.95" x2="1.97" y2="-1.95" width="0.1016" layer="51"/>
<wire x1="1.97" y1="-1.95" x2="-1.93" y2="-1.95" width="0.1016" layer="51"/>
<wire x1="-1.93" y1="-1.95" x2="-1.93" y2="1.95" width="0.1016" layer="51"/>
<circle x="-2.08" y="2.1" radius="0.5" width="0" layer="21"/>
<smd name="1" x="-1.78" y="1" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="EXP" x="0" y="0" dx="2.159" dy="2.159" layer="1" stop="no" cream="no"/>
<smd name="2" x="-1.78" y="0.5" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="3" x="-1.78" y="0" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="4" x="-1.78" y="-0.5" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="5" x="-1.78" y="-1" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="6" x="-1" y="-1.8" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="7" x="-0.5" y="-1.8" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="8" x="0" y="-1.8" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="9" x="0.5" y="-1.8" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="10" x="1" y="-1.8" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="11" x="1.82" y="-1" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="12" x="1.82" y="-0.5" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="13" x="1.82" y="0" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="14" x="1.82" y="0.5" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="15" x="1.82" y="1" dx="0.3" dy="0.85" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="16" x="1.02" y="1.8" dx="0.3" dy="0.85" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="17" x="0.52" y="1.8" dx="0.3" dy="0.85" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="18" x="0" y="1.8" dx="0.3" dy="0.85" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="19" x="-0.5" y="1.8" dx="0.3" dy="0.85" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="20" x="-1" y="1.8" dx="0.3" dy="0.85" layer="1" roundness="75" stop="no" cream="no"/>
<text x="3" y="2.5" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<rectangle x1="-1.98" y1="0.875" x2="-1.48" y2="1.125" layer="51"/>
<rectangle x1="-1.98" y1="0.375" x2="-1.48" y2="0.625" layer="51"/>
<rectangle x1="-1.98" y1="-0.125" x2="-1.48" y2="0.125" layer="51"/>
<rectangle x1="-1.98" y1="-0.625" x2="-1.48" y2="-0.375" layer="51"/>
<rectangle x1="-1.98" y1="-1.125" x2="-1.48" y2="-0.875" layer="51"/>
<rectangle x1="-1.23" y1="-1.875" x2="-0.73" y2="-1.625" layer="51" rot="R90"/>
<rectangle x1="-0.73" y1="-1.875" x2="-0.23" y2="-1.625" layer="51" rot="R90"/>
<rectangle x1="-0.23" y1="-1.875" x2="0.27" y2="-1.625" layer="51" rot="R90"/>
<rectangle x1="0.27" y1="-1.875" x2="0.77" y2="-1.625" layer="51" rot="R90"/>
<rectangle x1="0.77" y1="-1.875" x2="1.27" y2="-1.625" layer="51" rot="R90"/>
<rectangle x1="1.52" y1="-1.125" x2="2.02" y2="-0.875" layer="51" rot="R180"/>
<rectangle x1="1.52" y1="-0.625" x2="2.02" y2="-0.375" layer="51" rot="R180"/>
<rectangle x1="1.52" y1="-0.125" x2="2.02" y2="0.125" layer="51" rot="R180"/>
<rectangle x1="1.52" y1="0.375" x2="2.02" y2="0.625" layer="51" rot="R180"/>
<rectangle x1="1.52" y1="0.875" x2="2.02" y2="1.125" layer="51" rot="R180"/>
<rectangle x1="0.77" y1="1.625" x2="1.27" y2="1.875" layer="51" rot="R270"/>
<rectangle x1="0.27" y1="1.625" x2="0.77" y2="1.875" layer="51" rot="R270"/>
<rectangle x1="-0.23" y1="1.625" x2="0.27" y2="1.875" layer="51" rot="R270"/>
<rectangle x1="-0.73" y1="1.625" x2="-0.23" y2="1.875" layer="51" rot="R270"/>
<rectangle x1="-1.23" y1="1.625" x2="-0.73" y2="1.875" layer="51" rot="R270"/>
<rectangle x1="-0.9" y1="-0.25" x2="-0.65" y2="0.25" layer="31"/>
<rectangle x1="-0.125" y1="0.525" x2="0.125" y2="1.025" layer="31" rot="R90"/>
<rectangle x1="0.65" y1="-0.25" x2="0.9" y2="0.25" layer="31" rot="R180"/>
<rectangle x1="-0.125" y1="-1.025" x2="0.125" y2="-0.525" layer="31" rot="R270"/>
<circle x="-1.5" y="1" radius="0.125" width="0" layer="31"/>
<circle x="-1.5" y="1" radius="0.2" width="0" layer="29"/>
<rectangle x1="-2.15" y1="0.875" x2="-1.5" y2="1.125" layer="31"/>
<rectangle x1="-2.2" y1="0.8" x2="-1.5" y2="1.2" layer="29"/>
<circle x="1.54" y="1" radius="0.125" width="0" layer="31"/>
<circle x="1.54" y="1" radius="0.2" width="0" layer="29"/>
<rectangle x1="1.54" y1="0.875" x2="2.19" y2="1.125" layer="31" rot="R180"/>
<rectangle x1="1.54" y1="0.8" x2="2.24" y2="1.2" layer="29" rot="R180"/>
<circle x="-1.5" y="0.5" radius="0.125" width="0" layer="31"/>
<circle x="-1.5" y="0.5" radius="0.2" width="0" layer="29"/>
<rectangle x1="-2.15" y1="0.375" x2="-1.5" y2="0.625" layer="31"/>
<rectangle x1="-2.2" y1="0.3" x2="-1.5" y2="0.7" layer="29"/>
<circle x="-1.5" y="0" radius="0.125" width="0" layer="31"/>
<circle x="-1.5" y="0" radius="0.2" width="0" layer="29"/>
<rectangle x1="-2.15" y1="-0.125" x2="-1.5" y2="0.125" layer="31"/>
<rectangle x1="-2.2" y1="-0.2" x2="-1.5" y2="0.2" layer="29"/>
<circle x="-1.5" y="-0.5" radius="0.125" width="0" layer="31"/>
<circle x="-1.5" y="-0.5" radius="0.2" width="0" layer="29"/>
<rectangle x1="-2.15" y1="-0.625" x2="-1.5" y2="-0.375" layer="31"/>
<rectangle x1="-2.2" y1="-0.7" x2="-1.5" y2="-0.3" layer="29"/>
<circle x="-1.5" y="-1" radius="0.125" width="0" layer="31"/>
<circle x="-1.5" y="-1" radius="0.2" width="0" layer="29"/>
<rectangle x1="-2.15" y1="-1.125" x2="-1.5" y2="-0.875" layer="31"/>
<rectangle x1="-2.2" y1="-1.2" x2="-1.5" y2="-0.8" layer="29"/>
<circle x="1.54" y="0.5" radius="0.125" width="0" layer="31"/>
<circle x="1.54" y="0.5" radius="0.2" width="0" layer="29"/>
<rectangle x1="1.54" y1="0.375" x2="2.19" y2="0.625" layer="31" rot="R180"/>
<rectangle x1="1.54" y1="0.3" x2="2.24" y2="0.7" layer="29" rot="R180"/>
<circle x="1.54" y="0" radius="0.125" width="0" layer="31"/>
<circle x="1.54" y="0" radius="0.2" width="0" layer="29"/>
<rectangle x1="1.54" y1="-0.125" x2="2.19" y2="0.125" layer="31" rot="R180"/>
<rectangle x1="1.54" y1="-0.2" x2="2.24" y2="0.2" layer="29" rot="R180"/>
<circle x="1.54" y="-0.5" radius="0.125" width="0" layer="31"/>
<circle x="1.54" y="-0.5" radius="0.2" width="0" layer="29"/>
<rectangle x1="1.54" y1="-0.625" x2="2.19" y2="-0.375" layer="31" rot="R180"/>
<rectangle x1="1.54" y1="-0.7" x2="2.24" y2="-0.3" layer="29" rot="R180"/>
<circle x="1.54" y="-1" radius="0.125" width="0" layer="31"/>
<circle x="1.54" y="-1" radius="0.2" width="0" layer="29"/>
<rectangle x1="1.54" y1="-1.125" x2="2.19" y2="-0.875" layer="31" rot="R180"/>
<rectangle x1="1.54" y1="-1.2" x2="2.24" y2="-0.8" layer="29" rot="R180"/>
<circle x="-1" y="1.53" radius="0.125" width="0" layer="31"/>
<circle x="-1" y="1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="-1.325" y1="1.73" x2="-0.675" y2="1.98" layer="31" rot="R270"/>
<rectangle x1="-1.35" y1="1.68" x2="-0.65" y2="2.08" layer="29" rot="R270"/>
<circle x="-0.5" y="1.53" radius="0.125" width="0" layer="31"/>
<circle x="-0.5" y="1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="-0.825" y1="1.73" x2="-0.175" y2="1.98" layer="31" rot="R270"/>
<rectangle x1="-0.85" y1="1.68" x2="-0.15" y2="2.08" layer="29" rot="R270"/>
<circle x="0" y="1.53" radius="0.125" width="0" layer="31"/>
<circle x="0" y="1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="-0.325" y1="1.73" x2="0.325" y2="1.98" layer="31" rot="R270"/>
<rectangle x1="-0.35" y1="1.68" x2="0.35" y2="2.08" layer="29" rot="R270"/>
<circle x="0.5" y="1.53" radius="0.125" width="0" layer="31"/>
<circle x="0.5" y="1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="0.175" y1="1.73" x2="0.825" y2="1.98" layer="31" rot="R270"/>
<rectangle x1="0.15" y1="1.68" x2="0.85" y2="2.08" layer="29" rot="R270"/>
<circle x="1" y="1.53" radius="0.125" width="0" layer="31"/>
<circle x="1" y="1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="0.675" y1="1.73" x2="1.325" y2="1.98" layer="31" rot="R270"/>
<rectangle x1="0.65" y1="1.68" x2="1.35" y2="2.08" layer="29" rot="R270"/>
<circle x="1" y="-1.53" radius="0.125" width="0" layer="31"/>
<circle x="1" y="-1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="0.675" y1="-1.98" x2="1.325" y2="-1.73" layer="31" rot="R90"/>
<rectangle x1="0.65" y1="-2.08" x2="1.35" y2="-1.68" layer="29" rot="R90"/>
<circle x="0.5" y="-1.53" radius="0.125" width="0" layer="31"/>
<circle x="0.5" y="-1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="0.175" y1="-1.98" x2="0.825" y2="-1.73" layer="31" rot="R90"/>
<rectangle x1="0.15" y1="-2.08" x2="0.85" y2="-1.68" layer="29" rot="R90"/>
<circle x="0" y="-1.53" radius="0.125" width="0" layer="31"/>
<circle x="0" y="-1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="-0.325" y1="-1.98" x2="0.325" y2="-1.73" layer="31" rot="R90"/>
<rectangle x1="-0.35" y1="-2.08" x2="0.35" y2="-1.68" layer="29" rot="R90"/>
<circle x="-0.5" y="-1.53" radius="0.125" width="0" layer="31"/>
<circle x="-0.5" y="-1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="-0.825" y1="-1.98" x2="-0.175" y2="-1.73" layer="31" rot="R90"/>
<rectangle x1="-0.85" y1="-2.08" x2="-0.15" y2="-1.68" layer="29" rot="R90"/>
<circle x="-1" y="-1.53" radius="0.125" width="0" layer="31"/>
<circle x="-1" y="-1.53" radius="0.2" width="0" layer="29"/>
<rectangle x1="-1.325" y1="-1.98" x2="-0.675" y2="-1.73" layer="31" rot="R90"/>
<rectangle x1="-1.35" y1="-2.08" x2="-0.65" y2="-1.68" layer="29" rot="R90"/>
<rectangle x1="-0.4" y1="-1.1" x2="0.4" y2="-0.4" layer="29"/>
<rectangle x1="-0.4" y1="0.4" x2="0.4" y2="1.1" layer="29"/>
<rectangle x1="-1.15" y1="-0.35" x2="-0.35" y2="0.35" layer="29" rot="R90"/>
<rectangle x1="0.35" y1="-0.35" x2="1.15" y2="0.35" layer="29" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="CC115L">
<wire x1="-15.24" y1="-22.86" x2="15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="-22.86" x2="15.24" y2="22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="22.86" x2="-15.24" y2="22.86" width="0.254" layer="94"/>
<wire x1="-15.24" y1="22.86" x2="-15.24" y2="-22.86" width="0.254" layer="94"/>
<text x="-13.97" y="24.13" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<pin name="GDO2" x="-17.78" y="0" length="short" direction="in"/>
<pin name="DVDD" x="-17.78" y="-5.08" length="short" direction="pwr"/>
<pin name="DCOUPL" x="-17.78" y="-10.16" length="short" direction="pwr"/>
<pin name="SCLK" x="-17.78" y="10.16" length="short" direction="in"/>
<pin name="SO(GDO1)" x="-17.78" y="5.08" length="short" direction="out"/>
<pin name="CS_N" x="-5.08" y="-25.4" length="short" rot="R90"/>
<pin name="XOSC_Q2" x="10.16" y="-25.4" length="short" direction="pas" rot="R90"/>
<pin name="XOSC_Q1" x="0" y="-25.4" length="short" direction="pas" rot="R90"/>
<pin name="GDO0" x="-10.16" y="-25.4" length="short" direction="in" rot="R90"/>
<pin name="AVDD@11" x="17.78" y="-10.16" length="short" rot="R180"/>
<pin name="RF_P" x="17.78" y="-5.08" length="short" rot="R180"/>
<pin name="RF_N" x="17.78" y="0" length="short" rot="R180"/>
<pin name="AVDD@14" x="17.78" y="5.08" length="short" rot="R180"/>
<pin name="AVDD@15" x="17.78" y="10.16" length="short" rot="R180"/>
<pin name="GND@16" x="10.16" y="25.4" length="short" rot="R270"/>
<pin name="RBIAS" x="5.08" y="25.4" length="short" rot="R270"/>
<pin name="SI" x="-10.16" y="25.4" length="short" rot="R270"/>
<pin name="GND@19" x="-5.08" y="25.4" length="short" rot="R270"/>
<pin name="DGUARD" x="0" y="25.4" length="short" rot="R270"/>
<pin name="AVDD@9" x="5.08" y="-25.4" length="short" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CC115L" prefix="U">
<description>CC115L Value Line Transmitter</description>
<gates>
<gate name="G$1" symbol="CC115L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN(20)">
<connects>
<connect gate="G$1" pin="AVDD@11" pad="11"/>
<connect gate="G$1" pin="AVDD@14" pad="14"/>
<connect gate="G$1" pin="AVDD@15" pad="15"/>
<connect gate="G$1" pin="AVDD@9" pad="9"/>
<connect gate="G$1" pin="CS_N" pad="7"/>
<connect gate="G$1" pin="DCOUPL" pad="5"/>
<connect gate="G$1" pin="DGUARD" pad="18"/>
<connect gate="G$1" pin="DVDD" pad="4"/>
<connect gate="G$1" pin="GDO0" pad="6"/>
<connect gate="G$1" pin="GDO2" pad="3"/>
<connect gate="G$1" pin="GND@16" pad="16"/>
<connect gate="G$1" pin="GND@19" pad="19"/>
<connect gate="G$1" pin="RBIAS" pad="17"/>
<connect gate="G$1" pin="RF_N" pad="13"/>
<connect gate="G$1" pin="RF_P" pad="12"/>
<connect gate="G$1" pin="SCLK" pad="1"/>
<connect gate="G$1" pin="SI" pad="20"/>
<connect gate="G$1" pin="SO(GDO1)" pad="2"/>
<connect gate="G$1" pin="XOSC_Q1" pad="8"/>
<connect gate="G$1" pin="XOSC_Q2" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rf-micro-devices" urn="urn:adsk.eagle:library:349">
<description>&lt;b&gt;RF Micro Devices RF Transceivers&lt;/b&gt;&lt;p&gt;
http://www.rfmd.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="QFN-16">
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.025" width="0.1016" layer="21"/>
<wire x1="1.025" y1="1.5" x2="1.5" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.05" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="1.05" x2="-1.5" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.025" width="0.1016" layer="21"/>
<wire x1="-1.025" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.025" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="1.5" y1="-1.025" x2="1.5" y2="-1.5" width="0.1016" layer="21"/>
<circle x="-1.2" y="0.75" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="0.75" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="0.25" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="0.25" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="-0.25" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="-0.75" radius="0.125" width="0" layer="31"/>
<circle x="-0.75" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.25" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.25" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.75" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="-0.75" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="-0.25" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="0.25" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="0.75" radius="0.125" width="0" layer="31"/>
<circle x="0.75" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.25" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.25" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.75" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="0" y="0" radius="0.15" width="0.01" layer="49"/>
<circle x="0.5" y="0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="0.5" y="-0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-0.5" y="-0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-0.5" y="0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-1.2" y="-0.25" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="-0.75" radius="0.2" width="0" layer="29"/>
<circle x="-0.75" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.25" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.25" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.75" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="-0.75" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="-0.25" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="0.25" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="0.75" radius="0.2" width="0" layer="29"/>
<circle x="0.75" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.25" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.25" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.75" y="1.2" radius="0.2" width="0" layer="29"/>
<smd name="TH" x="0" y="0" dx="1.5" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="1" x="-1.475" y="0.75" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="2" x="-1.475" y="0.25" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="3" x="-1.475" y="-0.25" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="4" x="-1.475" y="-0.75" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="5" x="-0.75" y="-1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="6" x="-0.25" y="-1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="7" x="0.25" y="-1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="8" x="0.75" y="-1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="9" x="1.475" y="-0.75" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="10" x="1.475" y="-0.25" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="11" x="1.475" y="0.25" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="12" x="1.475" y="0.75" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="13" x="0.75" y="1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="14" x="0.25" y="1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="15" x="-0.25" y="1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="16" x="-0.75" y="1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<text x="-0.1" y="2.6" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<rectangle x1="-1.9" y1="0.55" x2="-1.2" y2="0.95" layer="29"/>
<rectangle x1="-1.85" y1="0.625" x2="-1.2" y2="0.875" layer="31"/>
<rectangle x1="-1.9" y1="0.05" x2="-1.2" y2="0.45" layer="29"/>
<rectangle x1="-1.85" y1="0.125" x2="-1.2" y2="0.375" layer="31"/>
<rectangle x1="-1.85" y1="-0.375" x2="-1.2" y2="-0.125" layer="31"/>
<rectangle x1="-1.85" y1="-0.875" x2="-1.2" y2="-0.625" layer="31"/>
<rectangle x1="-1.075" y1="-1.65" x2="-0.425" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="-0.575" y1="-1.65" x2="0.075" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="-0.075" y1="-1.65" x2="0.575" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="0.425" y1="-1.65" x2="1.075" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="1.2" y1="-0.875" x2="1.85" y2="-0.625" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="-0.375" x2="1.85" y2="-0.125" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="0.125" x2="1.85" y2="0.375" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="0.625" x2="1.85" y2="0.875" layer="31" rot="R180"/>
<rectangle x1="0.425" y1="1.4" x2="1.075" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.075" y1="1.4" x2="0.575" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.575" y1="1.4" x2="0.075" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-1.075" y1="1.4" x2="-0.425" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-1.9" y1="-0.45" x2="-1.2" y2="-0.05" layer="29"/>
<rectangle x1="-1.9" y1="-0.95" x2="-1.2" y2="-0.55" layer="29"/>
<rectangle x1="-1.1" y1="-1.75" x2="-0.4" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="-0.6" y1="-1.75" x2="0.1" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="-0.1" y1="-1.75" x2="0.6" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="0.4" y1="-1.75" x2="1.1" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="1.2" y1="-0.95" x2="1.9" y2="-0.55" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="-0.45" x2="1.9" y2="-0.05" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="0.05" x2="1.9" y2="0.45" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="0.55" x2="1.9" y2="0.95" layer="29" rot="R180"/>
<rectangle x1="0.4" y1="1.35" x2="1.1" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-0.1" y1="1.35" x2="0.6" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-0.6" y1="1.35" x2="0.1" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-1.1" y1="1.35" x2="-0.4" y2="1.75" layer="29" rot="R270"/>
<circle x="-2" y="1.9" radius="0.125" width="0.5" layer="21"/>
<rectangle x1="-0.75" y1="-0.25" x2="-0.25" y2="0.25" layer="29"/>
<rectangle x1="-0.25" y1="-0.75" x2="0.25" y2="-0.25" layer="29"/>
<rectangle x1="0.25" y1="-0.25" x2="0.75" y2="0.25" layer="29"/>
<rectangle x1="-0.25" y1="0.25" x2="0.25" y2="0.75" layer="29"/>
<rectangle x1="-0.65" y1="-0.15" x2="-0.35" y2="0.15" layer="31"/>
<rectangle x1="-0.15" y1="0.35" x2="0.15" y2="0.65" layer="31"/>
<rectangle x1="0.35" y1="-0.15" x2="0.65" y2="0.15" layer="31"/>
<rectangle x1="-0.15" y1="-0.65" x2="0.15" y2="-0.35" layer="31"/>
</package>
</packages>
<symbols>
<symbol name="RFPA0133">
<wire x1="-15.24" y1="-20.32" x2="15.24" y2="-20.32" width="0.254" layer="94"/>
<wire x1="15.24" y1="-20.32" x2="15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="15.24" y1="20.32" x2="-15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="-15.24" y1="20.32" x2="-15.24" y2="-20.32" width="0.254" layer="94"/>
<text x="-12.7" y="21.59" size="1.778" layer="95" rot="R180" align="center">&gt;NAME</text>
<text x="-1.27" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<pin name="RF_IN" x="-17.78" y="-2.54" length="short" direction="pas"/>
<pin name="NC@4" x="-17.78" y="-7.62" length="short" direction="pas"/>
<pin name="NC@1" x="-17.78" y="7.62" length="short" direction="pas"/>
<pin name="GND" x="-17.78" y="2.54" length="short" direction="pwr"/>
<pin name="NC@5" x="-7.62" y="-22.86" length="short" direction="pas" rot="R90"/>
<pin name="NC@8" x="7.62" y="-22.86" length="short" direction="pas" rot="R90"/>
<pin name="VPD" x="-2.54" y="-22.86" length="short" direction="in" rot="R90"/>
<pin name="RF_OUT@9" x="17.78" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="RF_OUT@10" x="17.78" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="NC@11" x="17.78" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="G8" x="17.78" y="7.62" length="short" direction="in" rot="R180"/>
<pin name="G16" x="7.62" y="22.86" length="short" direction="in" rot="R270"/>
<pin name="VBIAS" x="-7.62" y="22.86" length="short" direction="pas" rot="R270"/>
<pin name="NC@15" x="-2.54" y="22.86" length="short" direction="pas" rot="R270"/>
<pin name="VCC1" x="2.54" y="22.86" length="short" direction="pwr" rot="R270"/>
<pin name="NC@7" x="2.54" y="-22.86" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RFPA0133" prefix="U">
<description>3 to 5V Programmable Gain High Efficiency Power Amplifier</description>
<gates>
<gate name="G$1" symbol="RFPA0133" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN-16">
<connects>
<connect gate="G$1" pin="G16" pad="13"/>
<connect gate="G$1" pin="G8" pad="12"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="NC@1" pad="1"/>
<connect gate="G$1" pin="NC@11" pad="11"/>
<connect gate="G$1" pin="NC@15" pad="15"/>
<connect gate="G$1" pin="NC@4" pad="4"/>
<connect gate="G$1" pin="NC@5" pad="5"/>
<connect gate="G$1" pin="NC@7" pad="7"/>
<connect gate="G$1" pin="NC@8" pad="8"/>
<connect gate="G$1" pin="RF_IN" pad="3"/>
<connect gate="G$1" pin="RF_OUT@10" pad="10"/>
<connect gate="G$1" pin="RF_OUT@9" pad="9"/>
<connect gate="G$1" pin="VBIAS" pad="16"/>
<connect gate="G$1" pin="VCC1" pad="14"/>
<connect gate="G$1" pin="VPD" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="inductor">
<packages>
<package name="0402">
<smd name="1" x="-0.597" y="0" dx="0.635" dy="0.61" layer="1"/>
<smd name="2" x="0.597" y="0" dx="0.635" dy="0.61" layer="1"/>
<text x="0" y="1.4" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.4" size="1.27" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="0603">
<wire x1="-0.129" y1="0.7114" x2="0.129" y2="0.7114" width="0.127" layer="21"/>
<wire x1="-0.129" y1="-0.7238" x2="0.129" y2="-0.7238" width="0.127" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="1.4" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.4" size="1.27" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="0805">
<wire x1="-1.719" y1="0.8814" x2="1.719" y2="0.8814" width="0.127" layer="51"/>
<wire x1="1.719" y1="-0.8814" x2="-1.719" y2="-0.8814" width="0.127" layer="51"/>
<wire x1="-1.719" y1="-0.8814" x2="-1.719" y2="0.8814" width="0.127" layer="51"/>
<wire x1="1.719" y1="0.8814" x2="1.719" y2="-0.8814" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="0" y="1.6444" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.6444" size="1.27" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<wire x1="-0.0778" y1="0.6858" x2="0.0778" y2="0.6858" width="0.127" layer="21"/>
<wire x1="-0.0778" y1="-0.6858" x2="0.0778" y2="-0.6858" width="0.127" layer="21"/>
</package>
<package name="TY_5040">
<smd name="1" x="-1.8" y="0" dx="4" dy="1.5" layer="1" rot="R90"/>
<smd name="2" x="1.8" y="0" dx="4" dy="1.5" layer="1" rot="R90"/>
<text x="-3.6" y="0" size="1.27" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" font="vector" ratio="15" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-0.75" y1="-2" x2="0.75" y2="-2" width="0.127" layer="21"/>
<wire x1="-0.75" y1="2" x2="0.75" y2="2" width="0.127" layer="21"/>
<wire x1="-2.75" y1="2.25" x2="2.75" y2="2.25" width="0.127" layer="51"/>
<wire x1="2.75" y1="2.25" x2="2.75" y2="-2.25" width="0.127" layer="51"/>
<wire x1="2.75" y1="-2.25" x2="-2.75" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-2.75" y1="-2.25" x2="-2.75" y2="2.25" width="0.127" layer="51"/>
</package>
<package name="ASPI-4020S">
<smd name="1" x="-1.5" y="0" dx="3.7" dy="1.1" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="0" dx="3.7" dy="1.1" layer="1" rot="R90"/>
<text x="-3.6" y="0" size="1.27" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" font="vector" ratio="15" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-0.75" y1="-2" x2="0.75" y2="-2" width="0.127" layer="21"/>
<wire x1="-0.75" y1="2" x2="0.75" y2="2" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.05" x2="2.25" y2="2.05" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.05" x2="2.25" y2="-2.05" width="0.127" layer="51"/>
<wire x1="2.25" y1="-2.05" x2="-2.25" y2="-2.05" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.05" x2="-2.25" y2="2.05" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FERRITE">
<wire x1="-5.08" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="2.54" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="5.08" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="90"/>
<text x="0" y="5.08" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="-5.08" y1="3.175" x2="5.08" y2="3.175" width="0.254" layer="94" style="shortdash"/>
</symbol>
<symbol name="L-US">
<wire x1="-5.08" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="2.54" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="5.08" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="90"/>
<text x="0" y="3.81" size="1.778" layer="95" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96" rot="R180" align="center">&gt;VALUE</text>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FERRITE" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="FERRITE" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L-US" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TY5040" package="TY_5040">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ASPI-4020S" package="ASPI-4020S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="st-microelectronics" urn="urn:adsk.eagle:library:368">
<description>&lt;b&gt;ST Microelectronics Devices&lt;/b&gt;&lt;p&gt;
Microcontrollers,  I2C components, linear devices&lt;p&gt;
http://www.st.com&lt;p&gt;
&lt;i&gt;Include st-microelectronics-2.lbr, st-microelectronics-3.lbr.&lt;p&gt;&lt;/i&gt;

&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="LQFP48-7X7">
<description>&lt;b&gt;48-pin low profile quad flat package (7 x 7)&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.st.com/stonline/products/literature/ds/14771/stm8s105c4.pdf"&gt; Data sheet &lt;/a&gt;</description>
<wire x1="-3.146" y1="3.1" x2="-3.1" y2="3.146" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="3.146" x2="3.1" y2="3.146" width="0.2032" layer="21"/>
<wire x1="3.1" y1="3.146" x2="3.146" y2="3.1" width="0.2032" layer="21"/>
<wire x1="3.146" y1="3.1" x2="3.146" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="3.146" y1="-3.1" x2="3.1" y2="-3.146" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-3.146" x2="-3.1" y2="-3.146" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.146" x2="-3.146" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-3.146" y1="-3.1" x2="-3.146" y2="3.1" width="0.2032" layer="21"/>
<circle x="-4" y="4" radius="0.5" width="0" layer="21"/>
<smd name="1" x="-4.25" y="2.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="2" x="-4.25" y="2.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="3" x="-4.25" y="1.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="4" x="-4.25" y="1.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="5" x="-4.25" y="0.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="6" x="-4.25" y="0.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="7" x="-4.25" y="-0.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="8" x="-4.25" y="-0.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="9" x="-4.25" y="-1.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="10" x="-4.25" y="-1.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="11" x="-4.25" y="-2.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="12" x="-4.25" y="-2.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="13" x="-2.75" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="-2.25" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="-1.75" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="-1.25" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="-0.75" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="18" x="-0.25" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="19" x="0.25" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="20" x="0.75" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="21" x="1.25" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="1.75" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="2.25" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="24" x="2.75" y="-4.25" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="25" x="4.25" y="-2.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="26" x="4.25" y="-2.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="27" x="4.25" y="-1.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="28" x="4.25" y="-1.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="29" x="4.25" y="-0.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="30" x="4.25" y="-0.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="31" x="4.25" y="0.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="32" x="4.25" y="0.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="33" x="4.25" y="1.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="34" x="4.25" y="1.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="35" x="4.25" y="2.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="36" x="4.25" y="2.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="37" x="2.75" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="38" x="2.25" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="39" x="1.75" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="40" x="1.25" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="41" x="0.75" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="42" x="0.25" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="43" x="-0.25" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="44" x="-0.75" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="45" x="-1.25" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="46" x="-1.75" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="47" x="-2.25" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="48" x="-2.75" y="4.25" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<text x="0" y="6" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-6" size="1.27" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="-4.225" y1="2.65" x2="-3.475" y2="2.85" layer="51"/>
<rectangle x1="-4.225" y1="2.15" x2="-3.475" y2="2.35" layer="51"/>
<rectangle x1="-4.225" y1="1.65" x2="-3.475" y2="1.85" layer="51"/>
<rectangle x1="-4.225" y1="1.15" x2="-3.475" y2="1.35" layer="51"/>
<rectangle x1="-4.225" y1="0.65" x2="-3.475" y2="0.85" layer="51"/>
<rectangle x1="-4.225" y1="0.15" x2="-3.475" y2="0.35" layer="51"/>
<rectangle x1="-4.225" y1="-0.35" x2="-3.475" y2="-0.15" layer="51"/>
<rectangle x1="-4.225" y1="-0.85" x2="-3.475" y2="-0.65" layer="51"/>
<rectangle x1="-4.225" y1="-1.35" x2="-3.475" y2="-1.15" layer="51"/>
<rectangle x1="-4.225" y1="-1.85" x2="-3.475" y2="-1.65" layer="51"/>
<rectangle x1="-4.225" y1="-2.35" x2="-3.475" y2="-2.15" layer="51"/>
<rectangle x1="-4.225" y1="-2.85" x2="-3.475" y2="-2.65" layer="51"/>
<rectangle x1="-3.125" y1="-3.95" x2="-2.375" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-2.625" y1="-3.95" x2="-1.875" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-2.125" y1="-3.95" x2="-1.375" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-1.625" y1="-3.95" x2="-0.875" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-1.125" y1="-3.95" x2="-0.375" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-0.625" y1="-3.95" x2="0.125" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="-0.125" y1="-3.95" x2="0.625" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="0.375" y1="-3.95" x2="1.125" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="0.875" y1="-3.95" x2="1.625" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="1.375" y1="-3.95" x2="2.125" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="1.875" y1="-3.95" x2="2.625" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="2.375" y1="-3.95" x2="3.125" y2="-3.75" layer="51" rot="R90"/>
<rectangle x1="3.475" y1="-2.85" x2="4.225" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="-2.35" x2="4.225" y2="-2.15" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="-1.85" x2="4.225" y2="-1.65" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="-1.35" x2="4.225" y2="-1.15" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="-0.85" x2="4.225" y2="-0.65" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="-0.35" x2="4.225" y2="-0.15" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="0.15" x2="4.225" y2="0.35" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="0.65" x2="4.225" y2="0.85" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="1.15" x2="4.225" y2="1.35" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="1.65" x2="4.225" y2="1.85" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="2.15" x2="4.225" y2="2.35" layer="51" rot="R180"/>
<rectangle x1="3.475" y1="2.65" x2="4.225" y2="2.85" layer="51" rot="R180"/>
<rectangle x1="2.375" y1="3.75" x2="3.125" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="1.875" y1="3.75" x2="2.625" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="1.375" y1="3.75" x2="2.125" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="0.875" y1="3.75" x2="1.625" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="0.375" y1="3.75" x2="1.125" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-0.125" y1="3.75" x2="0.625" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-0.625" y1="3.75" x2="0.125" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-1.125" y1="3.75" x2="-0.375" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-1.625" y1="3.75" x2="-0.875" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-2.125" y1="3.75" x2="-1.375" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-2.625" y1="3.75" x2="-1.875" y2="3.95" layer="51" rot="R270"/>
<rectangle x1="-3.125" y1="3.75" x2="-2.375" y2="3.95" layer="51" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="STM32F373XX">
<wire x1="-38.1" y1="38.1" x2="-38.1" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-38.1" y1="-38.1" x2="38.1" y2="-38.1" width="0.254" layer="94"/>
<wire x1="38.1" y1="-38.1" x2="38.1" y2="33.02" width="0.254" layer="94"/>
<text x="-35.56" y="39.37" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<pin name="VBAT" x="27.94" y="40.64" length="short" direction="pwr" rot="R270"/>
<pin name="PC13" x="22.86" y="40.64" length="short" rot="R270"/>
<pin name="PC14-OSC32_IN" x="17.78" y="40.64" length="short" rot="R270"/>
<pin name="PC15-OSC32_OUT" x="12.7" y="40.64" length="short" rot="R270"/>
<pin name="PF0-OSC_IN" x="7.62" y="40.64" length="short" rot="R270"/>
<pin name="PF1-OSC_OUT" x="2.54" y="40.64" length="short" rot="R270"/>
<pin name="NRST" x="-2.54" y="40.64" length="short" direction="oc" rot="R270"/>
<pin name="VSSA/VREF-" x="-7.62" y="40.64" length="short" direction="pwr" rot="R270"/>
<pin name="VDDA/VREF+" x="-12.7" y="40.64" length="short" direction="pwr" rot="R270"/>
<pin name="PA0" x="-17.78" y="40.64" length="short" rot="R270"/>
<pin name="PA1" x="-22.86" y="40.64" length="short" rot="R270"/>
<pin name="PA2" x="-27.94" y="40.64" length="short" rot="R270"/>
<pin name="PA3" x="-40.64" y="27.94" length="short"/>
<pin name="PA4" x="-40.64" y="22.86" length="short"/>
<pin name="PA5" x="-40.64" y="17.78" length="short"/>
<pin name="PA6" x="-40.64" y="12.7" length="short"/>
<pin name="VDD_2" x="-40.64" y="7.62" length="short" direction="pwr"/>
<pin name="PB0" x="-40.64" y="2.54" length="short"/>
<pin name="PB1" x="-40.64" y="-2.54" length="short"/>
<pin name="PB2" x="-40.64" y="-7.62" length="short"/>
<pin name="PE8" x="-40.64" y="-12.7" length="short"/>
<pin name="PE9" x="-40.64" y="-17.78" length="short"/>
<pin name="VSSSD" x="-40.64" y="-22.86" length="short" direction="pwr"/>
<pin name="VDDSD" x="-40.64" y="-27.94" length="short" direction="pwr"/>
<pin name="VREFSD+" x="-27.94" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PB14" x="-22.86" y="-40.64" length="short" rot="R90"/>
<pin name="PB15" x="-17.78" y="-40.64" length="short" rot="R90"/>
<pin name="PD8" x="-12.7" y="-40.64" length="short" rot="R90"/>
<pin name="PA8" x="-7.62" y="-40.64" length="short" rot="R90"/>
<pin name="PA9" x="-2.54" y="-40.64" length="short" rot="R90"/>
<pin name="PA10" x="2.54" y="-40.64" length="short" rot="R90"/>
<pin name="PA11" x="7.62" y="-40.64" length="short" rot="R90"/>
<pin name="PA12" x="12.7" y="-40.64" length="short" rot="R90"/>
<pin name="PA13" x="17.78" y="-40.64" length="short" rot="R90"/>
<pin name="PF6" x="22.86" y="-40.64" length="short" rot="R90"/>
<pin name="PF7" x="27.94" y="-40.64" length="short" rot="R90"/>
<pin name="PA14" x="40.64" y="-27.94" length="short" rot="R180"/>
<pin name="PA15" x="40.64" y="-22.86" length="short" rot="R180"/>
<pin name="PB3" x="40.64" y="-17.78" length="short" rot="R180"/>
<pin name="PB4" x="40.64" y="-12.7" length="short" rot="R180"/>
<pin name="PB5" x="40.64" y="-7.62" length="short" rot="R180"/>
<pin name="PB6" x="40.64" y="-2.54" length="short" rot="R180"/>
<pin name="PB7" x="40.64" y="2.54" length="short" rot="R180"/>
<pin name="BOOT0" x="40.64" y="7.62" length="short" rot="R180"/>
<pin name="PB8" x="40.64" y="12.7" length="short" rot="R180"/>
<pin name="PB9" x="40.64" y="17.78" length="short" rot="R180"/>
<pin name="VSS_1" x="40.64" y="22.86" length="short" direction="pwr" rot="R180"/>
<pin name="VDD_1" x="40.64" y="27.94" length="short" direction="pwr" rot="R180"/>
<wire x1="-38.1" y1="38.1" x2="33.02" y2="38.1" width="0.254" layer="94"/>
<wire x1="33.02" y1="38.1" x2="38.1" y2="33.02" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="STM32F373XX" prefix="U">
<gates>
<gate name="G$1" symbol="STM32F373XX" x="-30.48" y="-35.56"/>
</gates>
<devices>
<device name="" package="LQFP48-7X7">
<connects>
<connect gate="G$1" pin="BOOT0" pad="44"/>
<connect gate="G$1" pin="NRST" pad="7"/>
<connect gate="G$1" pin="PA0" pad="10"/>
<connect gate="G$1" pin="PA1" pad="11"/>
<connect gate="G$1" pin="PA10" pad="31"/>
<connect gate="G$1" pin="PA11" pad="32"/>
<connect gate="G$1" pin="PA12" pad="33"/>
<connect gate="G$1" pin="PA13" pad="34"/>
<connect gate="G$1" pin="PA14" pad="37"/>
<connect gate="G$1" pin="PA15" pad="38"/>
<connect gate="G$1" pin="PA2" pad="12"/>
<connect gate="G$1" pin="PA3" pad="13"/>
<connect gate="G$1" pin="PA4" pad="14"/>
<connect gate="G$1" pin="PA5" pad="15"/>
<connect gate="G$1" pin="PA6" pad="16"/>
<connect gate="G$1" pin="PA8" pad="29"/>
<connect gate="G$1" pin="PA9" pad="30"/>
<connect gate="G$1" pin="PB0" pad="18"/>
<connect gate="G$1" pin="PB1" pad="19"/>
<connect gate="G$1" pin="PB14" pad="26"/>
<connect gate="G$1" pin="PB15" pad="27"/>
<connect gate="G$1" pin="PB2" pad="20"/>
<connect gate="G$1" pin="PB3" pad="39"/>
<connect gate="G$1" pin="PB4" pad="40"/>
<connect gate="G$1" pin="PB5" pad="41"/>
<connect gate="G$1" pin="PB6" pad="42"/>
<connect gate="G$1" pin="PB7" pad="43"/>
<connect gate="G$1" pin="PB8" pad="45"/>
<connect gate="G$1" pin="PB9" pad="46"/>
<connect gate="G$1" pin="PC13" pad="2"/>
<connect gate="G$1" pin="PC14-OSC32_IN" pad="3"/>
<connect gate="G$1" pin="PC15-OSC32_OUT" pad="4"/>
<connect gate="G$1" pin="PD8" pad="28"/>
<connect gate="G$1" pin="PE8" pad="21"/>
<connect gate="G$1" pin="PE9" pad="22"/>
<connect gate="G$1" pin="PF0-OSC_IN" pad="5"/>
<connect gate="G$1" pin="PF1-OSC_OUT" pad="6"/>
<connect gate="G$1" pin="PF6" pad="35"/>
<connect gate="G$1" pin="PF7" pad="36"/>
<connect gate="G$1" pin="VBAT" pad="1"/>
<connect gate="G$1" pin="VDDA/VREF+" pad="9"/>
<connect gate="G$1" pin="VDDSD" pad="24"/>
<connect gate="G$1" pin="VDD_1" pad="48"/>
<connect gate="G$1" pin="VDD_2" pad="17"/>
<connect gate="G$1" pin="VREFSD+" pad="25"/>
<connect gate="G$1" pin="VSSA/VREF-" pad="8"/>
<connect gate="G$1" pin="VSSSD" pad="23"/>
<connect gate="G$1" pin="VSS_1" pad="47"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="u-blox" urn="urn:adsk.eagle:library:406">
<description>&lt;b&gt;Produkte für GPS und drahtlose Kommunikation&lt;/b&gt;&lt;p&gt;
GPS Chips GPS Lösungen Wireless Module Wireless Lösungen Antennen&lt;br&gt;
http://www.u-blox.com&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="CAM-M8">
<smd name="1" x="4.4" y="-6.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="2" x="4.4" y="-5.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="3" x="4.4" y="-4.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="4" x="4.4" y="-3.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="5" x="4.4" y="-2.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="6" x="4.4" y="-1.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="7" x="4.4" y="-0.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="8" x="4.4" y="0.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="9" x="4.4" y="1.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="10" x="4.4" y="2.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="11" x="4.4" y="3.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="12" x="4.4" y="4.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="13" x="4.4" y="5.95" dx="1.5" dy="1" layer="1"/>
<smd name="14" x="-4.4" y="5.95" dx="1.5" dy="1" layer="1"/>
<smd name="15" x="-4.4" y="4.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="16" x="-4.4" y="3.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="17" x="-4.4" y="2.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="18" x="-4.4" y="1.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="19" x="-4.4" y="0.8" dx="1.5" dy="0.7" layer="1"/>
<smd name="20" x="-4.4" y="-0.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="21" x="-4.4" y="-1.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="22" x="-4.4" y="-2.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="23" x="-4.4" y="-3.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="24" x="-4.4" y="-4.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="25" x="-4.4" y="-5.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="26" x="-4.4" y="-6.2" dx="1.5" dy="0.7" layer="1"/>
<smd name="27" x="-2" y="-6.6" dx="1.5" dy="0.7" layer="1" rot="R90"/>
<smd name="28" x="-1" y="-6.6" dx="1.5" dy="0.7" layer="1" rot="R90"/>
<smd name="29" x="0" y="-6.6" dx="1.5" dy="0.7" layer="1" rot="R90"/>
<smd name="30" x="1" y="-6.6" dx="1.5" dy="0.7" layer="1" rot="R90"/>
<smd name="31" x="2" y="-6.6" dx="1.5" dy="0.7" layer="1" rot="R90"/>
<text x="5.75" y="-8" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="-4.8" y1="7" x2="4.8" y2="7" width="0.127" layer="21"/>
<wire x1="4.8" y1="-7" x2="2.8" y2="-7" width="0.127" layer="21"/>
<wire x1="-2.8" y1="-7" x2="-4.8" y2="-7" width="0.127" layer="21"/>
<wire x1="4.8" y1="-7" x2="4.8" y2="-6.8" width="0.127" layer="21"/>
<wire x1="-4.8" y1="-7" x2="-4.8" y2="-6.8" width="0.127" layer="21"/>
<wire x1="-4.8" y1="7" x2="-4.8" y2="6.8" width="0.127" layer="21"/>
<wire x1="4.8" y1="7" x2="4.8" y2="6.8" width="0.127" layer="21"/>
<rectangle x1="-3.6" y1="2.2" x2="3.6" y2="7" layer="41"/>
<rectangle x1="-3.6" y1="2.2" x2="3.6" y2="7" layer="42"/>
<wire x1="-4.75" y1="7" x2="4.75" y2="7" width="0.127" layer="51"/>
<wire x1="4.75" y1="7" x2="4.75" y2="-7" width="0.127" layer="51"/>
<wire x1="4.75" y1="-7" x2="-4.75" y2="-7" width="0.127" layer="51"/>
<wire x1="-4.75" y1="-7" x2="-4.75" y2="7" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="CAM-M8Q">
<wire x1="-20.32" y1="20.32" x2="20.32" y2="20.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="20.32" x2="20.32" y2="-20.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="-20.32" x2="-20.32" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-20.32" x2="-20.32" y2="20.32" width="0.254" layer="94"/>
<text x="-20.32" y="21.59" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-20.32" y="-21.59" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="GND@14" x="-25.4" y="17.78" length="middle" direction="pwr"/>
<pin name="GND@15" x="-25.4" y="15.24" length="middle" direction="pwr"/>
<pin name="ANT" x="-25.4" y="12.7" length="middle" direction="pas"/>
<pin name="GND@18" x="-25.4" y="7.62" length="middle" direction="pwr"/>
<pin name="GND@19" x="-25.4" y="5.08" length="middle" direction="pwr"/>
<pin name="DSEL" x="-25.4" y="2.54" length="middle"/>
<pin name="GND@21" x="-25.4" y="0" length="middle" direction="pwr"/>
<pin name="GND@22" x="-25.4" y="-2.54" length="middle" direction="pwr"/>
<pin name="RESET_N" x="-25.4" y="-5.08" length="middle"/>
<pin name="RSV1" x="-25.4" y="-7.62" length="middle"/>
<pin name="TXD" x="-25.4" y="-10.16" length="middle" direction="out"/>
<pin name="RXD" x="-25.4" y="-12.7" length="middle" direction="in"/>
<pin name="TIMEPULSE" x="0" y="-25.4" length="middle" direction="out" rot="R90"/>
<pin name="RSV2" x="-2.54" y="-25.4" length="middle" rot="R90"/>
<pin name="GND@27" x="-5.08" y="-25.4" length="middle" direction="pwr" rot="R90"/>
<pin name="ANT_ON" x="2.54" y="-25.4" length="middle" rot="R90"/>
<pin name="GND@31" x="5.08" y="-25.4" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC_IO" x="25.4" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="RSV3" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="SDA" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="GND@4" x="25.4" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@5" x="25.4" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="SCL" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="EXTINT" x="25.4" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="V_BCKP" x="25.4" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="VCC" x="25.4" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@10" x="25.4" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@11" x="25.4" y="12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@12" x="25.4" y="15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@13" x="25.4" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="RF_IN" x="-25.4" y="10.16" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAM-M8Q" prefix="U">
<description>u-blox M8 Concurrent GNSS Antenna Module</description>
<gates>
<gate name="1" symbol="CAM-M8Q" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAM-M8">
<connects>
<connect gate="1" pin="ANT" pad="16"/>
<connect gate="1" pin="ANT_ON" pad="30"/>
<connect gate="1" pin="DSEL" pad="20"/>
<connect gate="1" pin="EXTINT" pad="7"/>
<connect gate="1" pin="GND@10" pad="10"/>
<connect gate="1" pin="GND@11" pad="11"/>
<connect gate="1" pin="GND@12" pad="12"/>
<connect gate="1" pin="GND@13" pad="13"/>
<connect gate="1" pin="GND@14" pad="14"/>
<connect gate="1" pin="GND@15" pad="15"/>
<connect gate="1" pin="GND@18" pad="18"/>
<connect gate="1" pin="GND@19" pad="19"/>
<connect gate="1" pin="GND@21" pad="21"/>
<connect gate="1" pin="GND@22" pad="22"/>
<connect gate="1" pin="GND@27" pad="27"/>
<connect gate="1" pin="GND@31" pad="31"/>
<connect gate="1" pin="GND@4" pad="4"/>
<connect gate="1" pin="GND@5" pad="5"/>
<connect gate="1" pin="RESET_N" pad="23"/>
<connect gate="1" pin="RF_IN" pad="17"/>
<connect gate="1" pin="RSV1" pad="2"/>
<connect gate="1" pin="RSV2" pad="24"/>
<connect gate="1" pin="RSV3" pad="28"/>
<connect gate="1" pin="RXD" pad="26"/>
<connect gate="1" pin="SCL" pad="6"/>
<connect gate="1" pin="SDA" pad="3"/>
<connect gate="1" pin="TIMEPULSE" pad="29"/>
<connect gate="1" pin="TXD" pad="25"/>
<connect gate="1" pin="VCC" pad="9"/>
<connect gate="1" pin="VCC_IO" pad="1"/>
<connect gate="1" pin="V_BCKP" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal" urn="urn:adsk.eagle:library:204">
<packages>
<package name="3.2X2.5MM">
<wire x1="-0.965" y1="1.17" x2="1.065" y2="1.17" width="0.2032" layer="51"/>
<wire x1="-0.4" y1="0.13" x2="0.5" y2="0.13" width="0.2032" layer="21"/>
<wire x1="1.49" y1="0.745" x2="1.49" y2="-0.705" width="0.2032" layer="51"/>
<wire x1="0.5" y1="0.13" x2="0.5" y2="-0.07" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.07" x2="-0.4" y2="-0.07" width="0.2032" layer="21"/>
<wire x1="1.065" y1="-1.13" x2="-0.965" y2="-1.13" width="0.2032" layer="51"/>
<wire x1="-0.4" y1="-0.07" x2="-0.4" y2="0.13" width="0.2032" layer="21"/>
<wire x1="-1.39" y1="-0.705" x2="-1.39" y2="0.745" width="0.2032" layer="51"/>
<wire x1="-1.39" y1="0.745" x2="-0.965" y2="1.17" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="1.065" y1="1.17" x2="1.49" y2="0.745" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="1.49" y1="-0.705" x2="1.065" y2="-1.13" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="-0.965" y1="-1.13" x2="-1.39" y2="-0.705" width="0.2032" layer="51" curve="89.516721"/>
<smd name="1" x="-1.15" y="-0.925" dx="1.3" dy="1.05" layer="1"/>
<smd name="2" x="1.15" y="-0.925" dx="1.3" dy="1.05" layer="1"/>
<smd name="3" x="1.15" y="0.925" dx="1.3" dy="1.05" layer="1"/>
<smd name="4" x="-1.15" y="0.925" dx="1.3" dy="1.05" layer="1"/>
<text x="0" y="2.31" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.31" size="1.27" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<wire x1="0.22" y1="-1.13" x2="-0.215" y2="-1.13" width="0.127" layer="21"/>
<wire x1="0.22" y1="1.17" x2="-0.215" y2="1.17" width="0.127" layer="21"/>
<wire x1="1.49" y1="0.145" x2="1.49" y2="-0.155" width="0.127" layer="21"/>
<wire x1="-1.39" y1="-0.155" x2="-1.39" y2="0.145" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="XTAL_2SH">
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-3.048" y1="1.905" x2="-3.048" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-3.048" y1="2.54" x2="3.048" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="1.905" width="0.1524" layer="94" style="shortdash"/>
<wire x1="3.048" y1="-1.905" x2="3.048" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-3.048" y1="-2.54" x2="3.048" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.905" width="0.1524" layer="94" style="shortdash"/>
<text x="0" y="6.35" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="3.81" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="HOT@3" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="HOT@1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="GND@4" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="GND@2" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<wire x1="-1.016" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="XTAL_2SH" prefix="Y" uservalue="yes">
<gates>
<gate name="G$1" symbol="XTAL_2SH" x="0" y="0"/>
</gates>
<devices>
<device name="_3.2X2.5MM" package="3.2X2.5MM">
<connects>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@4" pad="4"/>
<connect gate="G$1" pin="HOT@1" pad="1"/>
<connect gate="G$1" pin="HOT@3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diode" urn="urn:adsk.eagle:library:210">
<packages>
<package name="LED_0805">
<smd name="C" x="1.05" y="0" dx="1.2" dy="1.2" layer="1" rot="R270"/>
<smd name="A" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1" rot="R270"/>
<text x="0" y="1.8796" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.0066" size="1.27" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<wire x1="-0.127" y1="0.635" x2="0.127" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="-0.635" x2="0.127" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.6" y1="0.9" x2="2.4" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.9" x2="2.4" y2="-0.9" width="0.127" layer="21"/>
<rectangle x1="1.4" y1="-0.3" x2="3.4" y2="0.3" layer="21" rot="R270"/>
<wire x1="2.75" y1="1" x2="2.75" y2="-1" width="0.127" layer="51"/>
<wire x1="2.75" y1="-1" x2="-2" y2="-1" width="0.127" layer="51"/>
<wire x1="-2" y1="-1" x2="-2" y2="1" width="0.127" layer="51"/>
<wire x1="-2" y1="1" x2="2.75" y2="1" width="0.127" layer="51"/>
<rectangle x1="1.25" y1="-0.5" x2="3.25" y2="0.5" layer="51" rot="R270"/>
</package>
<package name="SOT-23">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" font="vector" ratio="15" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="-5.08" y="0" size="1.778" layer="95" rot="R90" align="center">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
<symbol name="BAV70">
<wire x1="0.381" y1="-2.54" x2="-1.27" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.302" x2="0.381" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.778" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.381" y1="-3.302" x2="0.381" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.381" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-2.54" x2="0.381" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-4.445" x2="-2.54" y2="-4.445" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-4.445" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="4.445" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="4.445" x2="2.54" y2="4.445" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-4.445" x2="2.54" y2="0" width="0.4064" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="4.445" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.778" x2="0.381" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.381" y1="2.54" x2="-1.27" y2="3.302" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.302" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.778" width="0.254" layer="94"/>
<circle x="1.27" y="0" radius="0.254" width="0.4064" layer="94"/>
<text x="0" y="6.096" size="1.778" layer="95" rot="R180" align="center">&gt;NAME</text>
<text x="0" y="-5.715" size="1.778" layer="96" rot="R180" align="center">&gt;VALUE</text>
<pin name="A1" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="C" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="A2" x="-5.08" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.778" x2="0.381" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.381" y1="2.54" x2="0.381" y2="3.302" width="0.254" layer="94"/>
<wire x1="0.381" y1="2.54" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_SMT" prefix="LED" uservalue="yes">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="_0805" package="LED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BAV70" prefix="D">
<gates>
<gate name="G$1" symbol="BAV70" x="0" y="0"/>
</gates>
<devices>
<device name="_SOT23" package="SOT-23">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-fet" urn="urn:adsk.eagle:library:396">
<description>&lt;b&gt;Field Effect Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
Symbols changed according to IEC617&lt;p&gt; 
All types, packages and assignment to symbols and pins checked&lt;p&gt;
Package outlines partly checked&lt;p&gt;
&lt;p&gt;
JFET = junction FET&lt;p&gt;
IGBT-x = insulated gate bipolar transistor&lt;p&gt;
x=N: NPN; x=P: PNP&lt;p&gt;
IGFET-mc-nnn; (IGFET=insulated gate field effect transistor)&lt;P&gt;
m=D: depletion mode (Verdr&amp;auml;ngungstyp)&lt;p&gt;
m=E: enhancement mode (Anreicherungstyp)&lt;p&gt;
c: N=N-channel; P=P-Channel&lt;p&gt;
GDSB: gate, drain, source, bulk&lt;p&gt;
&lt;p&gt;
by R. Vogg  15.March.2002</description>
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1854" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.6576" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1854" width="0.1524" layer="21"/>
<wire x1="0.6326" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" font="vector" ratio="15" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="P-TRENCH-MOSFET">
<wire x1="-1.778" y1="-0.762" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-3.175" x2="-1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="-1.778" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.905" x2="-1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="-1.778" y2="3.175" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.778" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="0.762" y2="-0.508" width="0.1524" layer="94"/>
<circle x="0" y="2.54" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-2.54" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<pin name="D" x="0" y="7.62" visible="pad" length="middle" direction="pas" rot="R270"/>
<pin name="G" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="S" x="0" y="-7.62" visible="pad" length="middle" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="-0.508"/>
<vertex x="0.762" y="0.254"/>
<vertex x="1.778" y="0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="0" y="0"/>
<vertex x="-1.016" y="0.762"/>
<vertex x="-1.016" y="-0.762"/>
</polygon>
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="4.57905" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.778" layer="94">D</text>
<text x="-2.54" y="-7.62" size="1.778" layer="94">S</text>
<text x="-7.62" y="0" size="1.778" layer="94">G</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PMV65XP" prefix="Q">
<description>MOSFET P-CH TRENCH 20V</description>
<gates>
<gate name="G$1" symbol="P-TRENCH-MOSFET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-bjt">
<description>Bipolar Junction Transistors</description>
<packages>
<package name="SOT-23">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" font="vector" ratio="15" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" align="center-right">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" align="center-right">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MMBT2222A-7-F" prefix="Q">
<description>SOT-23 NPN GEN PURPOSE</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="linear" urn="urn:adsk.eagle:library:262">
<description>&lt;b&gt;Linear Devices&lt;/b&gt;&lt;p&gt;
Operational amplifiers,  comparators, voltage regulators, ADCs, DACs, etc.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DFN16-5X3">
<wire x1="-2.5" y1="-2" x2="2.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="2.5" y1="2" x2="2.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="2.5" y1="2" x2="-2.5" y2="2" width="0.2032" layer="51"/>
<text x="-3.5" y="0" size="1.27" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" font="vector" ratio="15" rot="R180" align="center">&gt;VALUE</text>
<smd name="1" x="-1.75" y="-1.425" dx="0.65" dy="0.25" layer="1" rot="R90"/>
<smd name="2" x="-1.25" y="-1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-0.75" y="-1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="4" x="-0.25" y="-1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="5" x="0.25" y="-1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="6" x="0.75" y="-1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="7" x="1.25" y="-1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="8" x="1.75" y="-1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="9" x="1.75" y="1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="10" x="1.25" y="1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="11" x="0.75" y="1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="12" x="0.25" y="1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="13" x="-0.25" y="1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="14" x="-0.75" y="1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="15" x="-1.25" y="1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="16" x="-1.75" y="1.425" dx="0.65" dy="0.25" layer="1" roundness="50" rot="R90"/>
<smd name="17" x="0" y="0" dx="4.4" dy="1.65" layer="1" roundness="25" stop="no" thermals="no" cream="no"/>
<circle x="-3" y="-1.5" radius="0.125" width="0.25" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="2" width="0.2032" layer="51"/>
<rectangle x1="-1.4" y1="-0.6" x2="-0.7" y2="0.6" layer="31"/>
<rectangle x1="0.7" y1="-0.6" x2="1.4" y2="0.6" layer="31"/>
<rectangle x1="-1.7" y1="-0.8" x2="-0.4" y2="0.8" layer="29"/>
<rectangle x1="0.4" y1="-0.8" x2="1.7" y2="0.8" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="LTC3534">
<wire x1="-12.7" y1="20.32" x2="12.7" y2="20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="20.32" x2="12.7" y2="-20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="-12.7" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-20.32" x2="-12.7" y2="20.32" width="0.254" layer="94"/>
<text x="-12.7" y="21.59" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<pin name="GND@16" x="-15.24" y="-17.78" length="short" direction="pwr"/>
<pin name="GND@1" x="-15.24" y="-7.62" length="short" direction="pwr"/>
<pin name="FB" x="15.24" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="RUN/SS" x="-15.24" y="0" length="short" direction="in"/>
<pin name="GND@3" x="-15.24" y="-10.16" length="short" direction="pwr"/>
<pin name="VC" x="15.24" y="-2.54" length="short" direction="pwr" rot="R180"/>
<pin name="VIN" x="-15.24" y="10.16" length="short" direction="pwr"/>
<pin name="PVIN" x="-15.24" y="12.7" length="short" direction="pwr"/>
<pin name="VOUT" x="15.24" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="PWM" x="-15.24" y="5.08" length="short" direction="out"/>
<pin name="GND@9" x="-15.24" y="-15.24" length="short" direction="pwr"/>
<pin name="PGND1" x="15.24" y="-12.7" length="short" direction="pwr" rot="R180"/>
<pin name="SW1" x="-15.24" y="17.78" length="short" direction="pas"/>
<pin name="SW2" x="15.24" y="17.78" length="short" direction="pas" rot="R180"/>
<pin name="PGND2" x="15.24" y="-15.24" length="short" direction="pwr" rot="R180"/>
<pin name="GND@8" x="-15.24" y="-12.7" length="short" direction="pwr"/>
<pin name="FRAME" x="15.24" y="-17.78" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LTC3534" prefix="U">
<description>7V, 500mA Synchronous Buck-Boost DC/DC Converter</description>
<gates>
<gate name="G$1" symbol="LTC3534" x="0" y="0"/>
</gates>
<devices>
<device name="_DFN16" package="DFN16-5X3">
<connects>
<connect gate="G$1" pin="FB" pad="15"/>
<connect gate="G$1" pin="FRAME" pad="17"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@16" pad="16"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@8" pad="8"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="PGND1" pad="4"/>
<connect gate="G$1" pin="PGND2" pad="7"/>
<connect gate="G$1" pin="PVIN" pad="12"/>
<connect gate="G$1" pin="PWM" pad="10"/>
<connect gate="G$1" pin="RUN/SS" pad="2"/>
<connect gate="G$1" pin="SW1" pad="5"/>
<connect gate="G$1" pin="SW2" pad="6"/>
<connect gate="G$1" pin="VC" pad="14"/>
<connect gate="G$1" pin="VIN" pad="13"/>
<connect gate="G$1" pin="VOUT" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="meas-specialities">
<packages>
<package name="QFN5X3X1">
<smd name="1" x="-1.55" y="1.875" dx="2" dy="0.6" layer="1"/>
<smd name="2" x="-1.55" y="0.625" dx="2" dy="0.6" layer="1"/>
<smd name="3" x="-1.55" y="-0.625" dx="2" dy="0.6" layer="1"/>
<smd name="4" x="-1.55" y="-1.875" dx="2" dy="0.6" layer="1"/>
<smd name="8" x="1.55" y="1.875" dx="2" dy="0.6" layer="1"/>
<smd name="7" x="1.55" y="0.625" dx="2" dy="0.6" layer="1"/>
<smd name="6" x="1.55" y="-0.625" dx="2" dy="0.6" layer="1"/>
<smd name="5" x="1.55" y="-1.875" dx="2" dy="0.6" layer="1"/>
<text x="0" y="3.6" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-3.5" size="1.27" layer="27" font="vector" ratio="15" rot="SR0" align="center">&gt;VALUE</text>
<circle x="-2.03" y="2.715" radius="0.3" width="0" layer="21"/>
<wire x1="1.5" y1="1.375" x2="1.5" y2="1.125" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.125" x2="1.5" y2="-0.125" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.125" x2="1.5" y2="-1.375" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2.375" x2="1.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2.375" x2="-1.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.125" x2="-1.5" y2="-1.375" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.125" x2="-1.5" y2="-0.125" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.375" x2="-1.5" y2="1.125" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="2.375" width="0.127" layer="21"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="2.375" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2.5" x2="1.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="1.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2.5" x2="-1.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2.5" x2="-1.5" y2="2.5" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PRESSURE_I2C/SPI">
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="-12.7" y="5.08" length="short" direction="pwr"/>
<pin name="PS" x="-12.7" y="2.54" length="short"/>
<pin name="GND" x="-12.7" y="0" length="short"/>
<pin name="CSB" x="-12.7" y="-2.54" length="short"/>
<pin name="SDO" x="12.7" y="0" length="short" rot="R180"/>
<pin name="SDI/SDA" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="SCLK" x="12.7" y="5.08" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MS5607" prefix="U">
<gates>
<gate name="G$1" symbol="PRESSURE_I2C/SPI" x="12.7" y="-15.24"/>
</gates>
<devices>
<device name="" package="QFN5X3X1">
<connects>
<connect gate="G$1" pin="CSB" pad="4"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="PS" pad="2"/>
<connect gate="G$1" pin="SCLK" pad="8"/>
<connect gate="G$1" pin="SDI/SDA" pad="7"/>
<connect gate="G$1" pin="SDO" pad="6"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="v-reg" urn="urn:adsk.eagle:library:409">
<description>&lt;b&gt;Voltage Regulators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT-23-5">
<wire x1="1.422" y1="0.785" x2="1.422" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.785" x2="-1.422" y2="-0.785" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.785" x2="-1.422" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.785" x2="1.422" y2="0.785" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.785" x2="0.522" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.785" x2="-0.522" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.785" x2="0.428" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.785" x2="-1.422" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.785" x2="1.328" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.785" x2="1.422" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.785" x2="-1.328" y2="0.785" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.15" dx="0.6" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.15" dx="0.6" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.15" dx="0.6" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.15" dx="0.6" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.15" dx="0.6" dy="1.2" layer="1"/>
<text x="0" y="3" size="1.27" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-3" size="1.27" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LDK120">
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<text x="0" y="10.16" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="7.62" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="IN" x="-10.16" y="2.54" visible="pad" length="short" direction="pwr"/>
<pin name="GND" x="-5.08" y="-10.16" visible="pad" length="short" direction="pwr" rot="R90"/>
<pin name="OUT" x="10.16" y="2.54" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="BYPASS" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="EN" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<text x="-6.985" y="2.54" size="1.524" layer="97" align="center-left">IN</text>
<text x="-6.985" y="-2.54" size="1.524" layer="97" align="center-left">EN</text>
<text x="-5.08" y="-6.35" size="1.524" layer="97" align="center">GND</text>
<text x="5.08" y="-6.35" size="1.524" layer="97" align="center">BYP</text>
<text x="6.985" y="2.54" size="1.524" layer="97" align="center-right">OUT</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LDK120" prefix="U">
<description>LDO Voltage Regulators Linear Voltage Regulator</description>
<gates>
<gate name="G$1" symbol="LDK120" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SOT-23-5">
<connects>
<connect gate="G$1" pin="BYPASS" pad="4"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="test">
<packages>
<package name="SQ_2.5MM">
<smd name="1" x="0" y="0" dx="2.5" dy="2.5" layer="1"/>
<text x="3" y="0" size="1.27" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
</package>
<package name="SQ_2X1MM">
<smd name="P$1" x="0" y="0" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="3.27" size="1.27" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
</package>
<package name="SQ_2X0.5MM">
<smd name="P$1" x="0" y="0" dx="2" dy="0.5" layer="1" rot="R90"/>
<text x="0" y="3.27" size="1.27" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="TEST">
<wire x1="-0.762" y1="1.778" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.778" x2="0" y2="1.016" width="0.254" layer="94"/>
<wire x1="0" y1="1.016" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<text x="0" y="5.08" size="1.778" layer="95" align="center">&gt;NAME</text>
<pin name="TP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMT" prefix="TP">
<description>2.5MM Square Testpad</description>
<gates>
<gate name="G$1" symbol="TEST" x="0" y="0"/>
</gates>
<devices>
<device name="-SQ2.5MM" package="SQ_2.5MM">
<connects>
<connect gate="G$1" pin="TP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2X1MM" package="SQ_2X1MM">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SQ2X0.5MM" package="SQ_2X0.5MM">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="intermet_library">
<packages>
<package name="RESCAXE80P285X260X70-8" urn="urn:adsk.eagle:footprint:10198822/1" locally_modified="yes">
<description>8-Chiparray 2-Side Convex, 0.80 mm pitch, 2.85 X 2.60 X 0.70 mm body
&lt;p&gt;8-pin Chiparray 2-Side Convex package with 0.80 mm pitch with body size 2.85 X 2.60 X 0.70 mm&lt;/p&gt;</description>
<smd name="1" x="-0.84" y="1.2" dx="0.9" dy="0.45" layer="1"/>
<smd name="2" x="-0.84" y="0.4" dx="0.9" dy="0.45" layer="1"/>
<smd name="3" x="-0.84" y="-0.4" dx="0.9" dy="0.45" layer="1"/>
<smd name="4" x="-0.84" y="-1.2" dx="0.9" dy="0.45" layer="1"/>
<smd name="5" x="0.84" y="-1.2" dx="0.9" dy="0.45" layer="1"/>
<smd name="6" x="0.84" y="-0.4" dx="0.9" dy="0.45" layer="1"/>
<smd name="7" x="0.84" y="0.4" dx="0.9" dy="0.45" layer="1"/>
<smd name="8" x="0.84" y="1.2" dx="0.9" dy="0.45" layer="1"/>
<text x="0" y="2.7999" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3599" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="C0603" urn="urn:adsk.eagle:footprint:23123/1" locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.356" y1="0.632" x2="0.356" y2="0.632" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.619" x2="0.356" y2="-0.619" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0402" urn="urn:adsk.eagle:footprint:23121/1" locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<smd name="1" x="-0.597" y="0" dx="0.635" dy="0.61" layer="1"/>
<smd name="2" x="0.597" y="0" dx="0.635" dy="0.61" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504" urn="urn:adsk.eagle:footprint:23122/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0805" urn="urn:adsk.eagle:footprint:23124/1" locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.411" y1="0.94" x2="0.351" y2="0.94" width="0.1016" layer="51"/>
<wire x1="-0.416" y1="-0.98" x2="0.321" y2="-0.98" width="0.1016" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206" urn="urn:adsk.eagle:footprint:23125/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210" urn="urn:adsk.eagle:footprint:23126/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310" urn="urn:adsk.eagle:footprint:23127/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608" urn="urn:adsk.eagle:footprint:23128/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812" urn="urn:adsk.eagle:footprint:23129/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825" urn="urn:adsk.eagle:footprint:23130/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012" urn="urn:adsk.eagle:footprint:23131/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216" urn="urn:adsk.eagle:footprint:23132/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225" urn="urn:adsk.eagle:footprint:23133/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532" urn="urn:adsk.eagle:footprint:23134/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564" urn="urn:adsk.eagle:footprint:23135/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044" urn="urn:adsk.eagle:footprint:23136/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050" urn="urn:adsk.eagle:footprint:23137/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050" urn="urn:adsk.eagle:footprint:23138/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050" urn="urn:adsk.eagle:footprint:23139/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050" urn="urn:adsk.eagle:footprint:23140/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050" urn="urn:adsk.eagle:footprint:23141/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070" urn="urn:adsk.eagle:footprint:23142/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075" urn="urn:adsk.eagle:footprint:23143/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075" urn="urn:adsk.eagle:footprint:23144/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075" urn="urn:adsk.eagle:footprint:23145/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075" urn="urn:adsk.eagle:footprint:23146/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044" urn="urn:adsk.eagle:footprint:23147/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075" urn="urn:adsk.eagle:footprint:23148/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075" urn="urn:adsk.eagle:footprint:23149/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075" urn="urn:adsk.eagle:footprint:23150/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075" urn="urn:adsk.eagle:footprint:23151/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075" urn="urn:adsk.eagle:footprint:23152/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075" urn="urn:adsk.eagle:footprint:23153/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075" urn="urn:adsk.eagle:footprint:23154/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103" urn="urn:adsk.eagle:footprint:23155/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103" urn="urn:adsk.eagle:footprint:23156/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106" urn="urn:adsk.eagle:footprint:23157/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133" urn="urn:adsk.eagle:footprint:23158/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133" urn="urn:adsk.eagle:footprint:23159/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133" urn="urn:adsk.eagle:footprint:23160/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184" urn="urn:adsk.eagle:footprint:23161/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183" urn="urn:adsk.eagle:footprint:23162/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183" urn="urn:adsk.eagle:footprint:23163/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183" urn="urn:adsk.eagle:footprint:23164/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183" urn="urn:adsk.eagle:footprint:23165/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182" urn="urn:adsk.eagle:footprint:23166/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268" urn="urn:adsk.eagle:footprint:23167/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268" urn="urn:adsk.eagle:footprint:23168/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268" urn="urn:adsk.eagle:footprint:23169/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268" urn="urn:adsk.eagle:footprint:23170/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268" urn="urn:adsk.eagle:footprint:23171/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316" urn="urn:adsk.eagle:footprint:23172/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316" urn="urn:adsk.eagle:footprint:23173/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316" urn="urn:adsk.eagle:footprint:23174/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316" urn="urn:adsk.eagle:footprint:23175/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374" urn="urn:adsk.eagle:footprint:23176/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374" urn="urn:adsk.eagle:footprint:23177/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374" urn="urn:adsk.eagle:footprint:23178/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418" urn="urn:adsk.eagle:footprint:23179/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418" urn="urn:adsk.eagle:footprint:23180/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075" urn="urn:adsk.eagle:footprint:23181/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418" urn="urn:adsk.eagle:footprint:23182/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106" urn="urn:adsk.eagle:footprint:23183/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316" urn="urn:adsk.eagle:footprint:23184/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316" urn="urn:adsk.eagle:footprint:23185/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K" urn="urn:adsk.eagle:footprint:23186/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K" urn="urn:adsk.eagle:footprint:23187/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K" urn="urn:adsk.eagle:footprint:23188/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K" urn="urn:adsk.eagle:footprint:23189/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K" urn="urn:adsk.eagle:footprint:23190/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K" urn="urn:adsk.eagle:footprint:23191/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K" urn="urn:adsk.eagle:footprint:23192/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K" urn="urn:adsk.eagle:footprint:23193/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K" urn="urn:adsk.eagle:footprint:23194/1">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="HPC0201" urn="urn:adsk.eagle:footprint:25783/1">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
<package name="C0201" urn="urn:adsk.eagle:footprint:23196/1">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808" urn="urn:adsk.eagle:footprint:23197/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640" urn="urn:adsk.eagle:footprint:23198/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="C01005" urn="urn:adsk.eagle:footprint:25781/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:23044/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.482" y1="-0.716" x2="0.382" y2="-0.716" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.706" x2="-0.432" y2="0.706" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0402" urn="urn:adsk.eagle:footprint:25625/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="1" x="-0.597" y="0" dx="0.635" dy="0.61" layer="1"/>
<smd name="2" x="0.597" y="0" dx="0.635" dy="0.61" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:23045/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.42" y1="0.955" x2="0.4" y2="0.955" width="0.1524" layer="51"/>
<wire x1="-0.46" y1="-0.975" x2="0.36" y2="-0.975" width="0.1524" layer="51"/>
<smd name="1" x="-1.175" y="0" dx="1.3" dy="1.15" layer="1"/>
<smd name="2" x="1.175" y="0" dx="1.3" dy="1.15" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W" urn="urn:adsk.eagle:footprint:23046/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:23047/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W" urn="urn:adsk.eagle:footprint:23048/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210" urn="urn:adsk.eagle:footprint:23049/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W" urn="urn:adsk.eagle:footprint:23050/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010" urn="urn:adsk.eagle:footprint:23051/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.762" y1="1.705" x2="1.562" y2="1.705" width="0.1524" layer="51"/>
<wire x1="-1.817" y1="-1.705" x2="1.507" y2="-1.705" width="0.1524" layer="51"/>
<smd name="1" x="-2.45" y="0" dx="1" dy="2.5" layer="1"/>
<smd name="2" x="2.45" y="0" dx="1" dy="2.5" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R2010W" urn="urn:adsk.eagle:footprint:23052/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012" urn="urn:adsk.eagle:footprint:23053/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W" urn="urn:adsk.eagle:footprint:23054/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512" urn="urn:adsk.eagle:footprint:23055/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W" urn="urn:adsk.eagle:footprint:23056/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216" urn="urn:adsk.eagle:footprint:23057/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W" urn="urn:adsk.eagle:footprint:23058/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225" urn="urn:adsk.eagle:footprint:23059/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W" urn="urn:adsk.eagle:footprint:23060/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025" urn="urn:adsk.eagle:footprint:23061/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W" urn="urn:adsk.eagle:footprint:23062/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332" urn="urn:adsk.eagle:footprint:23063/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W" urn="urn:adsk.eagle:footprint:25646/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805" urn="urn:adsk.eagle:footprint:23065/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206" urn="urn:adsk.eagle:footprint:23066/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406" urn="urn:adsk.eagle:footprint:23067/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012" urn="urn:adsk.eagle:footprint:23068/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309" urn="urn:adsk.eagle:footprint:23069/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216" urn="urn:adsk.eagle:footprint:23070/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516" urn="urn:adsk.eagle:footprint:23071/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923" urn="urn:adsk.eagle:footprint:23072/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5" urn="urn:adsk.eagle:footprint:22991/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7" urn="urn:adsk.eagle:footprint:22998/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10" urn="urn:adsk.eagle:footprint:22992/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12" urn="urn:adsk.eagle:footprint:22993/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15" urn="urn:adsk.eagle:footprint:22997/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V" urn="urn:adsk.eagle:footprint:22994/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V" urn="urn:adsk.eagle:footprint:22995/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7" urn="urn:adsk.eagle:footprint:22996/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10" urn="urn:adsk.eagle:footprint:23073/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12" urn="urn:adsk.eagle:footprint:23074/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12" urn="urn:adsk.eagle:footprint:23076/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15" urn="urn:adsk.eagle:footprint:23077/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V" urn="urn:adsk.eagle:footprint:23078/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15" urn="urn:adsk.eagle:footprint:23079/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V" urn="urn:adsk.eagle:footprint:23080/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17" urn="urn:adsk.eagle:footprint:23081/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22" urn="urn:adsk.eagle:footprint:23082/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V" urn="urn:adsk.eagle:footprint:23083/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22" urn="urn:adsk.eagle:footprint:23084/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V" urn="urn:adsk.eagle:footprint:23085/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15" urn="urn:adsk.eagle:footprint:23086/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22" urn="urn:adsk.eagle:footprint:23087/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V" urn="urn:adsk.eagle:footprint:23088/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12" urn="urn:adsk.eagle:footprint:23089/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17" urn="urn:adsk.eagle:footprint:23090/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0" urn="urn:adsk.eagle:footprint:23091/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX" urn="urn:adsk.eagle:footprint:23100/1">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V" urn="urn:adsk.eagle:footprint:23098/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R" urn="urn:adsk.eagle:footprint:23092/1">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W" urn="urn:adsk.eagle:footprint:23093/1">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R" urn="urn:adsk.eagle:footprint:25676/1">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W" urn="urn:adsk.eagle:footprint:25677/1">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R" urn="urn:adsk.eagle:footprint:25678/1">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W" urn="urn:adsk.eagle:footprint:25679/1">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15" urn="urn:adsk.eagle:footprint:23099/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V" urn="urn:adsk.eagle:footprint:22999/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V" urn="urn:adsk.eagle:footprint:23075/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="R0201" urn="urn:adsk.eagle:footprint:25683/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VMTA55" urn="urn:adsk.eagle:footprint:25689/1">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60" urn="urn:adsk.eagle:footprint:25690/1">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52" urn="urn:adsk.eagle:footprint:25684/1">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53" urn="urn:adsk.eagle:footprint:25685/1">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54" urn="urn:adsk.eagle:footprint:25686/1">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55" urn="urn:adsk.eagle:footprint:25687/1">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56" urn="urn:adsk.eagle:footprint:25688/1">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527" urn="urn:adsk.eagle:footprint:13246/1">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001" urn="urn:adsk.eagle:footprint:25692/1">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002" urn="urn:adsk.eagle:footprint:25693/1">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2" urn="urn:adsk.eagle:footprint:25694/1">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515" urn="urn:adsk.eagle:footprint:25695/1">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527" urn="urn:adsk.eagle:footprint:25696/1">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927" urn="urn:adsk.eagle:footprint:25697/1">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218" urn="urn:adsk.eagle:footprint:25698/1">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R" urn="urn:adsk.eagle:footprint:25699/1">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="R01005" urn="urn:adsk.eagle:footprint:25701/1">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="22-11-2042" urn="urn:adsk.eagle:footprint:8078261/1" locally_modified="yes">
<description>&lt;b&gt;KK® 254 Solid Header, Vertical, with Friction Lock, 4 Circuits, Tin (Sn) Plating&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/022232041_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<wire x1="-5.08" y1="3.175" x2="5.08" y2="3.175" width="0.254" layer="21"/>
<wire x1="5.08" y1="3.175" x2="5.08" y2="1.27" width="0.254" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-3.175" width="0.254" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="-5.08" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.08" y2="1.27" width="0.254" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="3.175" width="0.254" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1" shape="long" rot="R90"/>
<text x="-5.08" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESCAXE80P285X260X70-8" urn="urn:adsk.eagle:package:10198810/1" locally_modified="yes" type="model">
<description>8-Chiparray 2-Side Convex, 0.80 mm pitch, 2.85 X 2.60 X 0.70 mm body
&lt;p&gt;8-pin Chiparray 2-Side Convex package with 0.80 mm pitch with body size 2.85 X 2.60 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESCAXE80P285X260X70-8"/>
</packageinstances>
</package3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:23616/2" locally_modified="yes" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0603"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:23626/2" locally_modified="yes" type="model">
<description>Chip, 1.00 X 0.50 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="C0402"/>
</packageinstances>
</package3d>
<package3d name="C0504" urn="urn:adsk.eagle:package:23624/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0504"/>
</packageinstances>
</package3d>
<package3d name="C0805" urn="urn:adsk.eagle:package:23617/2" locally_modified="yes" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0805"/>
</packageinstances>
</package3d>
<package3d name="C1206" urn="urn:adsk.eagle:package:23618/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1206"/>
</packageinstances>
</package3d>
<package3d name="C1210" urn="urn:adsk.eagle:package:23619/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1210"/>
</packageinstances>
</package3d>
<package3d name="C1310" urn="urn:adsk.eagle:package:23620/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1310"/>
</packageinstances>
</package3d>
<package3d name="C1608" urn="urn:adsk.eagle:package:23621/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1608"/>
</packageinstances>
</package3d>
<package3d name="C1812" urn="urn:adsk.eagle:package:23622/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1812"/>
</packageinstances>
</package3d>
<package3d name="C1825" urn="urn:adsk.eagle:package:23623/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1825"/>
</packageinstances>
</package3d>
<package3d name="C2012" urn="urn:adsk.eagle:package:23625/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C2012"/>
</packageinstances>
</package3d>
<package3d name="C3216" urn="urn:adsk.eagle:package:23628/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C3216"/>
</packageinstances>
</package3d>
<package3d name="C3225" urn="urn:adsk.eagle:package:23655/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C3225"/>
</packageinstances>
</package3d>
<package3d name="C4532" urn="urn:adsk.eagle:package:23627/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C4532"/>
</packageinstances>
</package3d>
<package3d name="C4564" urn="urn:adsk.eagle:package:23648/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C4564"/>
</packageinstances>
</package3d>
<package3d name="C025-024X044" urn="urn:adsk.eagle:package:23630/1" type="box">
<description>CAPACITOR
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<packageinstances>
<packageinstance name="C025-024X044"/>
</packageinstances>
</package3d>
<package3d name="C025-025X050" urn="urn:adsk.eagle:package:23629/2" type="model">
<description>CAPACITOR
grid 2.5 mm, outline 2.5 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-025X050"/>
</packageinstances>
</package3d>
<package3d name="C025-030X050" urn="urn:adsk.eagle:package:23631/1" type="box">
<description>CAPACITOR
grid 2.5 mm, outline 3 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-030X050"/>
</packageinstances>
</package3d>
<package3d name="C025-040X050" urn="urn:adsk.eagle:package:23634/1" type="box">
<description>CAPACITOR
grid 2.5 mm, outline 4 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-040X050"/>
</packageinstances>
</package3d>
<package3d name="C025-050X050" urn="urn:adsk.eagle:package:23633/1" type="box">
<description>CAPACITOR
grid 2.5 mm, outline 5 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-050X050"/>
</packageinstances>
</package3d>
<package3d name="C025-060X050" urn="urn:adsk.eagle:package:23632/1" type="box">
<description>CAPACITOR
grid 2.5 mm, outline 6 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-060X050"/>
</packageinstances>
</package3d>
<package3d name="C025_050-024X070" urn="urn:adsk.eagle:package:23639/1" type="box">
<description>CAPACITOR
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<packageinstances>
<packageinstance name="C025_050-024X070"/>
</packageinstances>
</package3d>
<package3d name="C025_050-025X075" urn="urn:adsk.eagle:package:23641/1" type="box">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-025X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-035X075" urn="urn:adsk.eagle:package:23651/1" type="box">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-035X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-045X075" urn="urn:adsk.eagle:package:23635/1" type="box">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-045X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-055X075" urn="urn:adsk.eagle:package:23636/1" type="box">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-055X075"/>
</packageinstances>
</package3d>
<package3d name="C050-024X044" urn="urn:adsk.eagle:package:23643/1" type="box">
<description>CAPACITOR
grid 5 mm, outline 2.4 x 4.4 mm</description>
<packageinstances>
<packageinstance name="C050-024X044"/>
</packageinstances>
</package3d>
<package3d name="C050-025X075" urn="urn:adsk.eagle:package:23637/1" type="box">
<description>CAPACITOR
grid 5 mm, outline 2.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-025X075"/>
</packageinstances>
</package3d>
<package3d name="C050-045X075" urn="urn:adsk.eagle:package:23638/1" type="box">
<description>CAPACITOR
grid 5 mm, outline 4.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-045X075"/>
</packageinstances>
</package3d>
<package3d name="C050-030X075" urn="urn:adsk.eagle:package:23640/1" type="box">
<description>CAPACITOR
grid 5 mm, outline 3 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-030X075"/>
</packageinstances>
</package3d>
<package3d name="C050-050X075" urn="urn:adsk.eagle:package:23665/1" type="box">
<description>CAPACITOR
grid 5 mm, outline 5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-050X075"/>
</packageinstances>
</package3d>
<package3d name="C050-055X075" urn="urn:adsk.eagle:package:23642/1" type="box">
<description>CAPACITOR
grid 5 mm, outline 5.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-055X075"/>
</packageinstances>
</package3d>
<package3d name="C050-075X075" urn="urn:adsk.eagle:package:23645/1" type="box">
<description>CAPACITOR
grid 5 mm, outline 7.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-075X075"/>
</packageinstances>
</package3d>
<package3d name="C050H075X075" urn="urn:adsk.eagle:package:23644/1" type="box">
<description>CAPACITOR
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050H075X075"/>
</packageinstances>
</package3d>
<package3d name="C075-032X103" urn="urn:adsk.eagle:package:23646/1" type="box">
<description>CAPACITOR
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<packageinstances>
<packageinstance name="C075-032X103"/>
</packageinstances>
</package3d>
<package3d name="C075-042X103" urn="urn:adsk.eagle:package:23656/1" type="box">
<description>CAPACITOR
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<packageinstances>
<packageinstance name="C075-042X103"/>
</packageinstances>
</package3d>
<package3d name="C075-052X106" urn="urn:adsk.eagle:package:23650/1" type="box">
<description>CAPACITOR
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<packageinstances>
<packageinstance name="C075-052X106"/>
</packageinstances>
</package3d>
<package3d name="C102-043X133" urn="urn:adsk.eagle:package:23647/1" type="box">
<description>CAPACITOR
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-043X133"/>
</packageinstances>
</package3d>
<package3d name="C102-054X133" urn="urn:adsk.eagle:package:23649/1" type="box">
<description>CAPACITOR
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-054X133"/>
</packageinstances>
</package3d>
<package3d name="C102-064X133" urn="urn:adsk.eagle:package:23653/1" type="box">
<description>CAPACITOR
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-064X133"/>
</packageinstances>
</package3d>
<package3d name="C102_152-062X184" urn="urn:adsk.eagle:package:23652/1" type="box">
<description>CAPACITOR
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<packageinstances>
<packageinstance name="C102_152-062X184"/>
</packageinstances>
</package3d>
<package3d name="C150-054X183" urn="urn:adsk.eagle:package:23669/1" type="box">
<description>CAPACITOR
grid 15 mm, outline 5.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-054X183"/>
</packageinstances>
</package3d>
<package3d name="C150-064X183" urn="urn:adsk.eagle:package:23654/1" type="box">
<description>CAPACITOR
grid 15 mm, outline 6.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-064X183"/>
</packageinstances>
</package3d>
<package3d name="C150-072X183" urn="urn:adsk.eagle:package:23657/1" type="box">
<description>CAPACITOR
grid 15 mm, outline 7.2 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-072X183"/>
</packageinstances>
</package3d>
<package3d name="C150-084X183" urn="urn:adsk.eagle:package:23658/1" type="box">
<description>CAPACITOR
grid 15 mm, outline 8.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-084X183"/>
</packageinstances>
</package3d>
<package3d name="C150-091X182" urn="urn:adsk.eagle:package:23659/1" type="box">
<description>CAPACITOR
grid 15 mm, outline 9.1 x 18.2 mm</description>
<packageinstances>
<packageinstance name="C150-091X182"/>
</packageinstances>
</package3d>
<package3d name="C225-062X268" urn="urn:adsk.eagle:package:23661/1" type="box">
<description>CAPACITOR
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-062X268"/>
</packageinstances>
</package3d>
<package3d name="C225-074X268" urn="urn:adsk.eagle:package:23660/1" type="box">
<description>CAPACITOR
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-074X268"/>
</packageinstances>
</package3d>
<package3d name="C225-087X268" urn="urn:adsk.eagle:package:23662/1" type="box">
<description>CAPACITOR
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-087X268"/>
</packageinstances>
</package3d>
<package3d name="C225-108X268" urn="urn:adsk.eagle:package:23663/1" type="box">
<description>CAPACITOR
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-108X268"/>
</packageinstances>
</package3d>
<package3d name="C225-113X268" urn="urn:adsk.eagle:package:23667/1" type="box">
<description>CAPACITOR
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-113X268"/>
</packageinstances>
</package3d>
<package3d name="C275-093X316" urn="urn:adsk.eagle:package:23701/1" type="box">
<description>CAPACITOR
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-093X316"/>
</packageinstances>
</package3d>
<package3d name="C275-113X316" urn="urn:adsk.eagle:package:23673/1" type="box">
<description>CAPACITOR
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-113X316"/>
</packageinstances>
</package3d>
<package3d name="C275-134X316" urn="urn:adsk.eagle:package:23664/1" type="box">
<description>CAPACITOR
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-134X316"/>
</packageinstances>
</package3d>
<package3d name="C275-205X316" urn="urn:adsk.eagle:package:23666/1" type="box">
<description>CAPACITOR
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-205X316"/>
</packageinstances>
</package3d>
<package3d name="C325-137X374" urn="urn:adsk.eagle:package:23672/1" type="box">
<description>CAPACITOR
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-137X374"/>
</packageinstances>
</package3d>
<package3d name="C325-162X374" urn="urn:adsk.eagle:package:23670/1" type="box">
<description>CAPACITOR
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-162X374"/>
</packageinstances>
</package3d>
<package3d name="C325-182X374" urn="urn:adsk.eagle:package:23668/1" type="box">
<description>CAPACITOR
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-182X374"/>
</packageinstances>
</package3d>
<package3d name="C375-192X418" urn="urn:adsk.eagle:package:23674/1" type="box">
<description>CAPACITOR
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-192X418"/>
</packageinstances>
</package3d>
<package3d name="C375-203X418" urn="urn:adsk.eagle:package:23671/1" type="box">
<description>CAPACITOR
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-203X418"/>
</packageinstances>
</package3d>
<package3d name="C050-035X075" urn="urn:adsk.eagle:package:23677/1" type="box">
<description>CAPACITOR
grid 5 mm, outline 3.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-035X075"/>
</packageinstances>
</package3d>
<package3d name="C375-155X418" urn="urn:adsk.eagle:package:23675/1" type="box">
<description>CAPACITOR
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-155X418"/>
</packageinstances>
</package3d>
<package3d name="C075-063X106" urn="urn:adsk.eagle:package:23678/1" type="box">
<description>CAPACITOR
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<packageinstances>
<packageinstance name="C075-063X106"/>
</packageinstances>
</package3d>
<package3d name="C275-154X316" urn="urn:adsk.eagle:package:23685/1" type="box">
<description>CAPACITOR
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-154X316"/>
</packageinstances>
</package3d>
<package3d name="C275-173X316" urn="urn:adsk.eagle:package:23676/1" type="box">
<description>CAPACITOR
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-173X316"/>
</packageinstances>
</package3d>
<package3d name="C0402K" urn="urn:adsk.eagle:package:23679/2" type="model">
<description>Ceramic Chip Capacitor KEMET 0204 reflow solder
Metric Code Size 1005</description>
<packageinstances>
<packageinstance name="C0402K"/>
</packageinstances>
</package3d>
<package3d name="C0603K" urn="urn:adsk.eagle:package:23680/2" type="model">
<description>Ceramic Chip Capacitor KEMET 0603 reflow solder
Metric Code Size 1608</description>
<packageinstances>
<packageinstance name="C0603K"/>
</packageinstances>
</package3d>
<package3d name="C0805K" urn="urn:adsk.eagle:package:23681/2" type="model">
<description>Ceramic Chip Capacitor KEMET 0805 reflow solder
Metric Code Size 2012</description>
<packageinstances>
<packageinstance name="C0805K"/>
</packageinstances>
</package3d>
<package3d name="C1206K" urn="urn:adsk.eagle:package:23682/2" type="model">
<description>Ceramic Chip Capacitor KEMET 1206 reflow solder
Metric Code Size 3216</description>
<packageinstances>
<packageinstance name="C1206K"/>
</packageinstances>
</package3d>
<package3d name="C1210K" urn="urn:adsk.eagle:package:23683/2" type="model">
<description>Ceramic Chip Capacitor KEMET 1210 reflow solder
Metric Code Size 3225</description>
<packageinstances>
<packageinstance name="C1210K"/>
</packageinstances>
</package3d>
<package3d name="C1812K" urn="urn:adsk.eagle:package:23686/2" type="model">
<description>Ceramic Chip Capacitor KEMET 1812 reflow solder
Metric Code Size 4532</description>
<packageinstances>
<packageinstance name="C1812K"/>
</packageinstances>
</package3d>
<package3d name="C1825K" urn="urn:adsk.eagle:package:23684/2" type="model">
<description>Ceramic Chip Capacitor KEMET 1825 reflow solder
Metric Code Size 4564</description>
<packageinstances>
<packageinstance name="C1825K"/>
</packageinstances>
</package3d>
<package3d name="C2220K" urn="urn:adsk.eagle:package:23687/2" type="model">
<description>Ceramic Chip Capacitor KEMET 2220 reflow solderMetric Code Size 5650</description>
<packageinstances>
<packageinstance name="C2220K"/>
</packageinstances>
</package3d>
<package3d name="C2225K" urn="urn:adsk.eagle:package:23692/2" type="model">
<description>Ceramic Chip Capacitor KEMET 2225 reflow solderMetric Code Size 5664</description>
<packageinstances>
<packageinstance name="C2225K"/>
</packageinstances>
</package3d>
<package3d name="HPC0201" urn="urn:adsk.eagle:package:26213/1" type="box">
<description> 
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<packageinstances>
<packageinstance name="HPC0201"/>
</packageinstances>
</package3d>
<package3d name="C0201" urn="urn:adsk.eagle:package:23690/2" type="model">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<packageinstances>
<packageinstance name="C0201"/>
</packageinstances>
</package3d>
<package3d name="C1808" urn="urn:adsk.eagle:package:23689/2" type="model">
<description>CAPACITOR
Source: AVX .. aphvc.pdf</description>
<packageinstances>
<packageinstance name="C1808"/>
</packageinstances>
</package3d>
<package3d name="C3640" urn="urn:adsk.eagle:package:23693/2" type="model">
<description>CAPACITOR
Source: AVX .. aphvc.pdf</description>
<packageinstances>
<packageinstance name="C3640"/>
</packageinstances>
</package3d>
<package3d name="C01005" urn="urn:adsk.eagle:package:26211/1" type="box">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C01005"/>
</packageinstances>
</package3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:23555/3" locally_modified="yes" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
<package3d name="R0402" urn="urn:adsk.eagle:package:26058/2" locally_modified="yes" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0402"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:23553/2" locally_modified="yes" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
<package3d name="R0805W" urn="urn:adsk.eagle:package:23537/2" type="model">
<description>RESISTOR wave soldering</description>
<packageinstances>
<packageinstance name="R0805W"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:23540/2" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R1206W" urn="urn:adsk.eagle:package:23539/2" type="model">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1206W"/>
</packageinstances>
</package3d>
<package3d name="R1210" urn="urn:adsk.eagle:package:23554/2" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1210"/>
</packageinstances>
</package3d>
<package3d name="R1210W" urn="urn:adsk.eagle:package:23541/2" type="model">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1210W"/>
</packageinstances>
</package3d>
<package3d name="R2010" urn="urn:adsk.eagle:package:23551/2" locally_modified="yes" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2010"/>
</packageinstances>
</package3d>
<package3d name="R2010W" urn="urn:adsk.eagle:package:23542/2" type="model">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2010W"/>
</packageinstances>
</package3d>
<package3d name="R2012" urn="urn:adsk.eagle:package:23543/2" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2012"/>
</packageinstances>
</package3d>
<package3d name="R2012W" urn="urn:adsk.eagle:package:23544/2" type="model">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2012W"/>
</packageinstances>
</package3d>
<package3d name="R2512" urn="urn:adsk.eagle:package:23545/2" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2512"/>
</packageinstances>
</package3d>
<package3d name="R2512W" urn="urn:adsk.eagle:package:23565/2" type="model">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2512W"/>
</packageinstances>
</package3d>
<package3d name="R3216" urn="urn:adsk.eagle:package:23557/2" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3216"/>
</packageinstances>
</package3d>
<package3d name="R3216W" urn="urn:adsk.eagle:package:23548/2" type="model">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3216W"/>
</packageinstances>
</package3d>
<package3d name="R3225" urn="urn:adsk.eagle:package:23549/2" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3225"/>
</packageinstances>
</package3d>
<package3d name="R3225W" urn="urn:adsk.eagle:package:23550/2" type="model">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3225W"/>
</packageinstances>
</package3d>
<package3d name="R5025" urn="urn:adsk.eagle:package:23552/2" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R5025"/>
</packageinstances>
</package3d>
<package3d name="R5025W" urn="urn:adsk.eagle:package:23558/2" type="model">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R5025W"/>
</packageinstances>
</package3d>
<package3d name="R6332" urn="urn:adsk.eagle:package:23559/2" type="model">
<description>RESISTOR
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332"/>
</packageinstances>
</package3d>
<package3d name="R6332W" urn="urn:adsk.eagle:package:26078/2" type="model">
<description>RESISTOR wave soldering
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332W"/>
</packageinstances>
</package3d>
<package3d name="M0805" urn="urn:adsk.eagle:package:23556/2" type="model">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M0805"/>
</packageinstances>
</package3d>
<package3d name="M1206" urn="urn:adsk.eagle:package:23566/2" type="model">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M1206"/>
</packageinstances>
</package3d>
<package3d name="M1406" urn="urn:adsk.eagle:package:23569/2" type="model">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M1406"/>
</packageinstances>
</package3d>
<package3d name="M2012" urn="urn:adsk.eagle:package:23561/2" type="model">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M2012"/>
</packageinstances>
</package3d>
<package3d name="M2309" urn="urn:adsk.eagle:package:23562/2" type="model">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M2309"/>
</packageinstances>
</package3d>
<package3d name="M3216" urn="urn:adsk.eagle:package:23563/1" type="box">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M3216"/>
</packageinstances>
</package3d>
<package3d name="M3516" urn="urn:adsk.eagle:package:23573/1" type="box">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M3516"/>
</packageinstances>
</package3d>
<package3d name="M5923" urn="urn:adsk.eagle:package:23564/1" type="box">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M5923"/>
</packageinstances>
</package3d>
<package3d name="0204/5" urn="urn:adsk.eagle:package:23488/1" type="box">
<description>RESISTOR
type 0204, grid 5 mm</description>
<packageinstances>
<packageinstance name="0204/5"/>
</packageinstances>
</package3d>
<package3d name="0204/7" urn="urn:adsk.eagle:package:23498/2" type="model">
<description>RESISTOR
type 0204, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0204/7"/>
</packageinstances>
</package3d>
<package3d name="0207/10" urn="urn:adsk.eagle:package:23491/2" type="model">
<description>RESISTOR
type 0207, grid 10 mm</description>
<packageinstances>
<packageinstance name="0207/10"/>
</packageinstances>
</package3d>
<package3d name="0207/12" urn="urn:adsk.eagle:package:23489/1" type="box">
<description>RESISTOR
type 0207, grid 12 mm</description>
<packageinstances>
<packageinstance name="0207/12"/>
</packageinstances>
</package3d>
<package3d name="0207/15" urn="urn:adsk.eagle:package:23492/1" type="box">
<description>RESISTOR
type 0207, grid 15mm</description>
<packageinstances>
<packageinstance name="0207/15"/>
</packageinstances>
</package3d>
<package3d name="0207/2V" urn="urn:adsk.eagle:package:23490/1" type="box">
<description>RESISTOR
type 0207, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0207/2V"/>
</packageinstances>
</package3d>
<package3d name="0207/5V" urn="urn:adsk.eagle:package:23502/1" type="box">
<description>RESISTOR
type 0207, grid 5 mm</description>
<packageinstances>
<packageinstance name="0207/5V"/>
</packageinstances>
</package3d>
<package3d name="0207/7" urn="urn:adsk.eagle:package:23493/2" type="model">
<description>RESISTOR
type 0207, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0207/7"/>
</packageinstances>
</package3d>
<package3d name="0309/10" urn="urn:adsk.eagle:package:23567/1" type="box">
<description>RESISTOR
type 0309, grid 10mm</description>
<packageinstances>
<packageinstance name="0309/10"/>
</packageinstances>
</package3d>
<package3d name="0309/12" urn="urn:adsk.eagle:package:23571/1" type="box">
<description>RESISTOR
type 0309, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0309/12"/>
</packageinstances>
</package3d>
<package3d name="0411/12" urn="urn:adsk.eagle:package:23578/1" type="box">
<description>RESISTOR
type 0411, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0411/12"/>
</packageinstances>
</package3d>
<package3d name="0411/15" urn="urn:adsk.eagle:package:23568/1" type="box">
<description>RESISTOR
type 0411, grid 15 mm</description>
<packageinstances>
<packageinstance name="0411/15"/>
</packageinstances>
</package3d>
<package3d name="0411V" urn="urn:adsk.eagle:package:23570/1" type="box">
<description>RESISTOR
type 0411, grid 3.81 mm</description>
<packageinstances>
<packageinstance name="0411V"/>
</packageinstances>
</package3d>
<package3d name="0414/15" urn="urn:adsk.eagle:package:23579/2" type="model">
<description>RESISTOR
type 0414, grid 15 mm</description>
<packageinstances>
<packageinstance name="0414/15"/>
</packageinstances>
</package3d>
<package3d name="0414V" urn="urn:adsk.eagle:package:23574/1" type="box">
<description>RESISTOR
type 0414, grid 5 mm</description>
<packageinstances>
<packageinstance name="0414V"/>
</packageinstances>
</package3d>
<package3d name="0617/17" urn="urn:adsk.eagle:package:23575/2" type="model">
<description>RESISTOR
type 0617, grid 17.5 mm</description>
<packageinstances>
<packageinstance name="0617/17"/>
</packageinstances>
</package3d>
<package3d name="0617/22" urn="urn:adsk.eagle:package:23577/1" type="box">
<description>RESISTOR
type 0617, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0617/22"/>
</packageinstances>
</package3d>
<package3d name="0617V" urn="urn:adsk.eagle:package:23576/1" type="box">
<description>RESISTOR
type 0617, grid 5 mm</description>
<packageinstances>
<packageinstance name="0617V"/>
</packageinstances>
</package3d>
<package3d name="0922/22" urn="urn:adsk.eagle:package:23580/2" type="model">
<description>RESISTOR
type 0922, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0922/22"/>
</packageinstances>
</package3d>
<package3d name="P0613V" urn="urn:adsk.eagle:package:23582/1" type="box">
<description>RESISTOR
type 0613, grid 5 mm</description>
<packageinstances>
<packageinstance name="P0613V"/>
</packageinstances>
</package3d>
<package3d name="P0613/15" urn="urn:adsk.eagle:package:23581/2" type="model">
<description>RESISTOR
type 0613, grid 15 mm</description>
<packageinstances>
<packageinstance name="P0613/15"/>
</packageinstances>
</package3d>
<package3d name="P0817/22" urn="urn:adsk.eagle:package:23583/1" type="box">
<description>RESISTOR
type 0817, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="P0817/22"/>
</packageinstances>
</package3d>
<package3d name="P0817V" urn="urn:adsk.eagle:package:23608/1" type="box">
<description>RESISTOR
type 0817, grid 6.35 mm</description>
<packageinstances>
<packageinstance name="P0817V"/>
</packageinstances>
</package3d>
<package3d name="V234/12" urn="urn:adsk.eagle:package:23592/1" type="box">
<description>RESISTOR
type V234, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="V234/12"/>
</packageinstances>
</package3d>
<package3d name="V235/17" urn="urn:adsk.eagle:package:23586/1" type="box">
<description>RESISTOR
type V235, grid 17.78 mm</description>
<packageinstances>
<packageinstance name="V235/17"/>
</packageinstances>
</package3d>
<package3d name="V526-0" urn="urn:adsk.eagle:package:23590/1" type="box">
<description>RESISTOR
type V526-0, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="V526-0"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102AX" urn="urn:adsk.eagle:package:23594/1" type="box">
<description>Mini MELF 0102 Axial</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102AX"/>
</packageinstances>
</package3d>
<package3d name="0922V" urn="urn:adsk.eagle:package:23589/1" type="box">
<description>RESISTOR
type 0922, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0922V"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102R" urn="urn:adsk.eagle:package:23591/2" type="model">
<description>CECC Size RC2211 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102W" urn="urn:adsk.eagle:package:23588/2" type="model">
<description>CECC Size RC2211 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204R" urn="urn:adsk.eagle:package:26109/2" type="model">
<description>CECC Size RC3715 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204W" urn="urn:adsk.eagle:package:26111/2" type="model">
<description>CECC Size RC3715 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207R" urn="urn:adsk.eagle:package:26113/2" type="model">
<description>CECC Size RC6123 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207W" urn="urn:adsk.eagle:package:26112/2" type="model">
<description>CECC Size RC6123 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207W"/>
</packageinstances>
</package3d>
<package3d name="RDH/15" urn="urn:adsk.eagle:package:23595/1" type="box">
<description>RESISTOR
type RDH, grid 15 mm</description>
<packageinstances>
<packageinstance name="RDH/15"/>
</packageinstances>
</package3d>
<package3d name="0204V" urn="urn:adsk.eagle:package:23495/1" type="box">
<description>RESISTOR
type 0204, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0204V"/>
</packageinstances>
</package3d>
<package3d name="0309V" urn="urn:adsk.eagle:package:23572/1" type="box">
<description>RESISTOR
type 0309, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0309V"/>
</packageinstances>
</package3d>
<package3d name="R0201" urn="urn:adsk.eagle:package:26117/2" type="model">
<description>RESISTOR chip
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R0201"/>
</packageinstances>
</package3d>
<package3d name="VMTA55" urn="urn:adsk.eagle:package:26121/2" type="model">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTA55"/>
</packageinstances>
</package3d>
<package3d name="VMTB60" urn="urn:adsk.eagle:package:26122/2" type="model">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC60
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTB60"/>
</packageinstances>
</package3d>
<package3d name="VTA52" urn="urn:adsk.eagle:package:26116/2" type="model">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR52
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA52"/>
</packageinstances>
</package3d>
<package3d name="VTA53" urn="urn:adsk.eagle:package:26118/2" type="model">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR53
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA53"/>
</packageinstances>
</package3d>
<package3d name="VTA54" urn="urn:adsk.eagle:package:26119/2" type="model">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR54
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA54"/>
</packageinstances>
</package3d>
<package3d name="VTA55" urn="urn:adsk.eagle:package:26120/2" type="model">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA55"/>
</packageinstances>
</package3d>
<package3d name="VTA56" urn="urn:adsk.eagle:package:26129/3" type="model">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR56
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA56"/>
</packageinstances>
</package3d>
<package3d name="R4527" urn="urn:adsk.eagle:package:13310/2" type="model">
<description>Package 4527
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<packageinstances>
<packageinstance name="R4527"/>
</packageinstances>
</package3d>
<package3d name="WSC0001" urn="urn:adsk.eagle:package:26123/2" type="model">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0001"/>
</packageinstances>
</package3d>
<package3d name="WSC0002" urn="urn:adsk.eagle:package:26125/2" type="model">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0002"/>
</packageinstances>
</package3d>
<package3d name="WSC01/2" urn="urn:adsk.eagle:package:26127/2" type="model">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC01/2"/>
</packageinstances>
</package3d>
<package3d name="WSC2515" urn="urn:adsk.eagle:package:26134/2" type="model">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC2515"/>
</packageinstances>
</package3d>
<package3d name="WSC4527" urn="urn:adsk.eagle:package:26126/2" type="model">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC4527"/>
</packageinstances>
</package3d>
<package3d name="WSC6927" urn="urn:adsk.eagle:package:26128/2" type="model">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC6927"/>
</packageinstances>
</package3d>
<package3d name="R1218" urn="urn:adsk.eagle:package:26131/2" type="model">
<description>CRCW1218 Thick Film, Rectangular Chip Resistors
Source: http://www.vishay.com .. dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R1218"/>
</packageinstances>
</package3d>
<package3d name="1812X7R" urn="urn:adsk.eagle:package:26130/2" type="model">
<description>Chip Monolithic Ceramic Capacitors Medium Voltage High Capacitance for General Use
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<packageinstances>
<packageinstance name="1812X7R"/>
</packageinstances>
</package3d>
<package3d name="R01005" urn="urn:adsk.eagle:package:26133/2" type="model">
<description>Chip, 0.40 X 0.20 X 0.16 mm body
&lt;p&gt;Chip package with body size 0.40 X 0.20 X 0.16 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="R01005"/>
</packageinstances>
</package3d>
<package3d name="22-23-2041" urn="urn:adsk.eagle:package:8078635/1" locally_modified="yes" type="box">
<description>&lt;b&gt;KK® 254 Solid Header, Vertical, with Friction Lock, 4 Circuits, Tin (Sn) Plating&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/022232041_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<packageinstances>
<packageinstance name="22-11-2042"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RN4">
<wire x1="-1.27" y1="1.778" x2="3.81" y2="1.778" width="0.254" layer="94"/>
<wire x1="3.81" y1="3.302" x2="-1.27" y2="3.302" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.778" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.302" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.778" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="3.302" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="3.81" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-1.27" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.302" x2="3.81" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.842" x2="3.81" y2="-5.842" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.778" x2="-1.27" y2="-1.778" width="0.254" layer="94"/>
<wire x1="3.81" y1="-4.318" x2="-1.27" y2="-4.318" width="0.254" layer="94"/>
<wire x1="3.81" y1="-3.302" x2="3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.842" x2="3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="-1.778" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="-5.842" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.778" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-4.318" x2="-1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="4.318" x2="-2.54" y2="4.318" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-6.858" x2="-2.54" y2="-6.858" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="4.318" x2="-2.54" y2="-6.858" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="4.318" x2="5.08" y2="-6.858" width="0.4064" layer="94"/>
<text x="-1.905" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-9.398" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="3" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="3"/>
<pin name="4" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="4"/>
<pin name="5" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="4" rot="R180"/>
<pin name="6" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="3" rot="R180"/>
<pin name="7" x="7.62" y="0" visible="pad" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="1" x="-5.08" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="C-EU-1">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="742C083221JP" prefix="R">
<description>SMD 4 resistor array
220Ω 1/16W rated per resistor</description>
<gates>
<gate name="G$1" symbol="RN4" x="0" y="0"/>
</gates>
<devices>
<device name="_220R" package="RESCAXE80P285X260X70-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10198810/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="742C083181JP" prefix="R">
<description>SMD 4 resistor array
180Ω 1/16W rated per resistor</description>
<gates>
<gate name="G$1" symbol="RN4" x="0" y="0"/>
</gates>
<devices>
<device name="_180R" package="RESCAXE80P285X260X70-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10198810/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU-1" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23626/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23624/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23617/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23619/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23620/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23621/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23622/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23623/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23625/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23628/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23655/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23627/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23648/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23630/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23629/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23631/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23634/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23633/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23632/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23639/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23641/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23651/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23635/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23636/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23643/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23637/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23638/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23640/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23665/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23642/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23645/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23644/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23646/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23656/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23650/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23647/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23649/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23653/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23652/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23669/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23654/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23657/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23658/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23659/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23661/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23660/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23662/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23663/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23667/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23701/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23673/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23664/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23666/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23672/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23670/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23668/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23674/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23671/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23677/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23675/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23678/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23685/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23676/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23679/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23680/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23681/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23682/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23683/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23686/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23684/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23687/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23692/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26213/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23690/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23689/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23693/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="C01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26211/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-US_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26058/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23553/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23537/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23540/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23539/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23554/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23541/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23551/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23542/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23543/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23544/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23545/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23565/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23557/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23548/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23549/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23550/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23552/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23558/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23559/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26078/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23556/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23566/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23569/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23561/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23562/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23563/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23573/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23564/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23488/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23498/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23491/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23489/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23492/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23490/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23502/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23493/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23567/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23571/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23578/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23568/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23570/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23579/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23574/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23575/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23577/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23576/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23580/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23582/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23581/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23583/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23608/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23592/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23586/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23590/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23594/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23589/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23591/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23588/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26109/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26111/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26113/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26112/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23595/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23495/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23572/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26117/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26121/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26122/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26116/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26118/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26119/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26120/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26129/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13310/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26123/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26125/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26127/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26134/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26126/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26128/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26131/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26130/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26133/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22-11-2042" prefix="X">
<description>HEADER, 4CT, 0.100" PITCH, LOCKING RAMP - MFG- MOLEX MFG P/N: 22-11-2042</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-11-2042">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8078635/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2041" constant="no"/>
<attribute name="OC_FARNELL" value="1462920" constant="no"/>
<attribute name="OC_NEWARK" value="38C0355" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="J1" library="InterMet" deviceset="IMET3_CARDEDGE" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="SW1" library="switch" library_urn="urn:adsk.eagle:library:380" deviceset="TE3-1437565-0" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="U8" library="texas" library_urn="urn:adsk.eagle:library:387" deviceset="CC115L" device=""/>
<part name="U7" library="rf-micro-devices" library_urn="urn:adsk.eagle:library:349" deviceset="RFPA0133" device=""/>
<part name="R1" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="20.0K 1%"/>
<part name="R3" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="10.0K 1%"/>
<part name="C36" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="GND72" library="supply1" deviceset="GND" device=""/>
<part name="GND60" library="supply1" deviceset="GND" device=""/>
<part name="GND61" library="supply1" deviceset="GND" device=""/>
<part name="C46" library="intermet_library" deviceset="C-EU" device="C0805" package3d_urn="urn:adsk.eagle:package:23617/2" value="10uF"/>
<part name="C47" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="L14" library="inductor" deviceset="FERRITE" device="_0603" value="1K @ 100MHz"/>
<part name="GND84" library="supply1" deviceset="GND" device=""/>
<part name="GND85" library="supply1" deviceset="GND" device=""/>
<part name="C48" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="220pF"/>
<part name="C49" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="220pF"/>
<part name="C50" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="220pF"/>
<part name="C51" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="220pF"/>
<part name="GND86" library="supply1" deviceset="GND" device=""/>
<part name="GND87" library="supply1" deviceset="GND" device=""/>
<part name="GND88" library="supply1" deviceset="GND" device=""/>
<part name="GND89" library="supply1" deviceset="GND" device=""/>
<part name="R37" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="56K 1%"/>
<part name="GND57" library="supply1" deviceset="GND" device=""/>
<part name="U6" library="st-microelectronics" library_urn="urn:adsk.eagle:library:368" deviceset="STM32F373XX" device="" value="STM32F373CBT6"/>
<part name="Y2" library="crystal" library_urn="urn:adsk.eagle:library:204" deviceset="XTAL_2SH" device="_3.2X2.5MM" value="26.0MHz 10ppm"/>
<part name="GND78" library="supply1" deviceset="GND" device=""/>
<part name="GND79" library="supply1" deviceset="GND" device=""/>
<part name="C41" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="10pF"/>
<part name="C42" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="10pF"/>
<part name="GND77" library="supply1" deviceset="GND" device=""/>
<part name="GND80" library="supply1" deviceset="GND" device=""/>
<part name="L10" library="inductor" deviceset="L-US" device="_0402" value="27nH"/>
<part name="C37" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="220pF"/>
<part name="GND74" library="supply1" deviceset="GND" device=""/>
<part name="C29" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="3.9pF"/>
<part name="C31" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="3.9pF"/>
<part name="L5" library="inductor" deviceset="L-US" device="_0402" value="27nH"/>
<part name="GND65" library="supply1" deviceset="GND" device=""/>
<part name="L6" library="inductor" deviceset="L-US" device="_0402" value="22nH"/>
<part name="L7" library="inductor" deviceset="L-US" device="_0402" value="27nH"/>
<part name="C32" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="8.2pF"/>
<part name="C33" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="5.6pF"/>
<part name="GND68" library="supply1" deviceset="GND" device=""/>
<part name="GND69" library="supply1" deviceset="GND" device=""/>
<part name="GND70" library="supply1" deviceset="GND" device=""/>
<part name="C30" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="L8" library="inductor" deviceset="L-US" device="_0402" value="18nH"/>
<part name="GND67" library="supply1" deviceset="GND" device=""/>
<part name="C26" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="2.2uF"/>
<part name="C27" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="GND63" library="supply1" deviceset="GND" device=""/>
<part name="GND64" library="supply1" deviceset="GND" device=""/>
<part name="R36" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="3.9R 5%"/>
<part name="L4" library="inductor" deviceset="L-US" device="_0402" value="15nH"/>
<part name="C23" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="2.2uF"/>
<part name="C24" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="33pF"/>
<part name="GND58" library="supply1" deviceset="GND" device=""/>
<part name="GND59" library="supply1" deviceset="GND" device=""/>
<part name="L9" library="inductor" deviceset="L-US" device="_0603" value="12nH"/>
<part name="L11" library="inductor" deviceset="L-US" device="_0603" value="27nH"/>
<part name="C38" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="C40" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="2.2uF"/>
<part name="GND73" library="supply1" deviceset="GND" device=""/>
<part name="GND75" library="supply1" deviceset="GND" device=""/>
<part name="C34" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="C35" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="9.1pF"/>
<part name="GND71" library="supply1" deviceset="GND" device=""/>
<part name="C39" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="C28" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="C25" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="GND66" library="supply1" deviceset="GND" device=""/>
<part name="GND62" library="supply1" deviceset="GND" device=""/>
<part name="GND76" library="supply1" deviceset="GND" device=""/>
<part name="C43" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="8.2pF"/>
<part name="C44" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="15pF"/>
<part name="C45" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="8.2pF"/>
<part name="L12" library="inductor" deviceset="L-US" device="_0603" value="22nH"/>
<part name="L13" library="inductor" deviceset="L-US" device="_0603" value="22nH"/>
<part name="GND81" library="supply1" deviceset="GND" device=""/>
<part name="GND82" library="supply1" deviceset="GND" device=""/>
<part name="GND83" library="supply1" deviceset="GND" device=""/>
<part name="ANT1" library="InterMet" deviceset="403_WHIP" device="_SMT"/>
<part name="3V1" library="supply1" deviceset="+3V" device=""/>
<part name="Y1" library="crystal" library_urn="urn:adsk.eagle:library:204" deviceset="XTAL_2SH" device="_3.2X2.5MM" value="16.0MHz 10ppm"/>
<part name="C21" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="6.8pF"/>
<part name="C22" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="6.8pF"/>
<part name="GND47" library="supply1" deviceset="GND" device=""/>
<part name="GND50" library="supply1" deviceset="GND" device=""/>
<part name="GND49" library="supply1" deviceset="GND" device=""/>
<part name="GND48" library="supply1" deviceset="GND" device=""/>
<part name="R23" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="1.5K 5%"/>
<part name="L3" library="inductor" deviceset="FERRITE" device="_0603" value="1K @ 100MHz"/>
<part name="C15" library="intermet_library" deviceset="C-EU" device="C0805" package3d_urn="urn:adsk.eagle:package:23617/2" value="10uF"/>
<part name="C16" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="C17" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="GND42" library="supply1" deviceset="GND" device=""/>
<part name="C18" library="intermet_library" deviceset="C-EU" device="C0805" package3d_urn="urn:adsk.eagle:package:23617/2" value="10uF"/>
<part name="C19" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="C20" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="GND43" library="supply1" deviceset="GND" device=""/>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="GND45" library="supply1" deviceset="GND" device=""/>
<part name="GND51" library="supply1" deviceset="GND" device=""/>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="GND46" library="supply1" deviceset="GND" device=""/>
<part name="3V17" library="supply1" deviceset="+3V" device=""/>
<part name="3V16" library="supply1" deviceset="+3V" device=""/>
<part name="3V8" library="supply1" deviceset="+3V" device=""/>
<part name="3V10" library="supply1" deviceset="+3V" device=""/>
<part name="3V9" library="supply1" deviceset="+3V" device=""/>
<part name="3V11" library="supply1" deviceset="+3V" device=""/>
<part name="FRAME1" library="InterMet" deviceset="FRAME_8.5X11" device=""/>
<part name="FRAME3" library="InterMet" deviceset="FRAME_8.5X11" device=""/>
<part name="FRAME4" library="InterMet" deviceset="FRAME_8.5X11" device=""/>
<part name="LED1" library="diode" library_urn="urn:adsk.eagle:library:210" deviceset="LED_SMT" device="_0805" value="BLU"/>
<part name="LED2" library="diode" library_urn="urn:adsk.eagle:library:210" deviceset="LED_SMT" device="_0805" value="BLU"/>
<part name="LED3" library="diode" library_urn="urn:adsk.eagle:library:210" deviceset="LED_SMT" device="_0805" value="BLU"/>
<part name="LED4" library="diode" library_urn="urn:adsk.eagle:library:210" deviceset="LED_SMT" device="_0805" value="BLU"/>
<part name="GND53" library="supply1" deviceset="GND" device=""/>
<part name="GND54" library="supply1" deviceset="GND" device=""/>
<part name="GND55" library="supply1" deviceset="GND" device=""/>
<part name="GND56" library="supply1" deviceset="GND" device=""/>
<part name="R38" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="220R 5%"/>
<part name="R22" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="220R 5%"/>
<part name="R24" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="330R 5%"/>
<part name="R25" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="330R 5%"/>
<part name="FRAME2" library="InterMet" deviceset="FRAME_8.5X11" device=""/>
<part name="Q1" library="transistor-fet" library_urn="urn:adsk.eagle:library:396" deviceset="PMV65XP" device=""/>
<part name="Q2" library="transistor-bjt" deviceset="MMBT2222A-7-F" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="R2" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="10K 5%"/>
<part name="U5" library="u-blox" library_urn="urn:adsk.eagle:library:406" deviceset="CAM-M8Q" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="C14" library="intermet_library" deviceset="C-EU" device="C0805" package3d_urn="urn:adsk.eagle:package:23617/2" value="10uF"/>
<part name="C13" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="3V7" library="supply1" deviceset="+3V" device=""/>
<part name="R19" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="220R 5%"/>
<part name="R20" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="220R 5%"/>
<part name="Q4" library="transistor-bjt" deviceset="MMBT2222A-7-F" device=""/>
<part name="GND52" library="supply1" deviceset="GND" device=""/>
<part name="U2" library="linear" library_urn="urn:adsk.eagle:library:262" deviceset="LTC3534" device="_DFN16"/>
<part name="C1" library="intermet_library" deviceset="C-EU" device="C0805" package3d_urn="urn:adsk.eagle:package:23617/2" value="10uF"/>
<part name="C2" library="intermet_library" deviceset="C-EU" device="C0805" package3d_urn="urn:adsk.eagle:package:23617/2" value="10uF"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="3V2" library="supply1" deviceset="+3V" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="C4" library="intermet_library" deviceset="C-EU" device="C0805" package3d_urn="urn:adsk.eagle:package:23617/2" value="10uF"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="R9" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="15K 5%"/>
<part name="R6" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="374K 1%"/>
<part name="R10" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="162K 1%"/>
<part name="C8" library="intermet_library" deviceset="C-EU" device="C0603" package3d_urn="urn:adsk.eagle:package:23616/2" value="330pF"/>
<part name="R8" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="10K 5%"/>
<part name="C7" library="intermet_library" deviceset="C-EU" device="C0603" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.047uF"/>
<part name="R7" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="499K 1%"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="U4" library="meas-specialities" deviceset="MS5607" device=""/>
<part name="3V6" library="supply1" deviceset="+3V" device=""/>
<part name="C12" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="R26" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="10K 5%"/>
<part name="R27" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="10K 5%"/>
<part name="3V12" library="supply1" deviceset="+3V" device=""/>
<part name="3V13" library="supply1" deviceset="+3V" device=""/>
<part name="J2" library="InterMet" deviceset="IQ_PROBE" device="_12FPZ-SM-TF"/>
<part name="R28" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="2.2K 5%"/>
<part name="R29" library="intermet_library" deviceset="R-US_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="2.2K 5%"/>
<part name="3V14" library="supply1" deviceset="+3V" device=""/>
<part name="3V15" library="supply1" deviceset="+3V" device=""/>
<part name="3V5" library="supply1" deviceset="+3V" device=""/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="R17" library="intermet_library" deviceset="R-US_" device="R0805" package3d_urn="urn:adsk.eagle:package:23553/2" value="1M 0.1%"/>
<part name="C11" library="intermet_library" deviceset="C-EU" device="C0603" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.47uF"/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="R18" library="intermet_library" deviceset="R-US_" device="R0805" package3d_urn="urn:adsk.eagle:package:23553/2" value="64.9K 0.1%"/>
<part name="Q3" library="transistor-bjt" deviceset="MMBT2222A-7-F" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device="" value="+3V3"/>
<part name="P+3" library="supply1" deviceset="+5V" device="" value="+3V3"/>
<part name="P+4" library="supply1" deviceset="+5V" device="" value="+3V3"/>
<part name="U1" library="v-reg" library_urn="urn:adsk.eagle:library:409" deviceset="LDK120" device=""/>
<part name="P+1" library="supply1" deviceset="+5V" device="" value="+3V3"/>
<part name="C3" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="D1" library="diode" library_urn="urn:adsk.eagle:library:210" deviceset="BAV70" device="_SOT23"/>
<part name="C52" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="220pF"/>
<part name="GND90" library="supply1" deviceset="GND" device=""/>
<part name="L2" library="inductor" deviceset="FERRITE" device="_0603" value="1K @ 100MHz"/>
<part name="C10" library="intermet_library" deviceset="C-EU" device="C0805" package3d_urn="urn:adsk.eagle:package:23617/2" value="10uF"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="TP1" library="test" deviceset="SMT" device="_2X1MM"/>
<part name="GND91" library="supply1" deviceset="GND" device=""/>
<part name="C5" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="33pF"/>
<part name="C6" library="intermet_library" deviceset="C-EU" device="C0603" package3d_urn="urn:adsk.eagle:package:23616/2" value="22uF"/>
<part name="C53" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="C54" library="intermet_library" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100pF"/>
<part name="GND92" library="supply1" deviceset="GND" device=""/>
<part name="GND93" library="supply1" deviceset="GND" device=""/>
<part name="R4" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="220R 5%"/>
<part name="R30" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="220R 5%"/>
<part name="R16" library="intermet_library" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:26058/2" value="220R 5%"/>
<part name="GND94" library="supply1" deviceset="GND" device=""/>
<part name="L1" library="inductor" deviceset="L-US" device="_ASPI-4020S" value="3.3uH"/>
<part name="R32" library="intermet_library" deviceset="R-US_" device="R2010" package3d_urn="urn:adsk.eagle:package:23551/2" value="51R 5%"/>
<part name="R34" library="intermet_library" deviceset="R-US_" device="R2010" package3d_urn="urn:adsk.eagle:package:23551/2" value="51R 5%"/>
<part name="R35" library="intermet_library" deviceset="R-US_" device="R2010" package3d_urn="urn:adsk.eagle:package:23551/2" value="51R 5%"/>
<part name="BAT1" library="battery_IMS" deviceset="BATTERY_FLAT_TAB" device=""/>
<part name="BAT3" library="battery_IMS" deviceset="BATTERY_FLAT_TAB" device=""/>
<part name="J4" library="intermet_library" deviceset="22-11-2042" device="" package3d_urn="urn:adsk.eagle:package:8078635/1"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L15" library="inductor" deviceset="L-US" device="_0603" value="27nH"/>
<part name="C9" library="intermet_library" deviceset="C-EU" device="C0603" package3d_urn="urn:adsk.eagle:package:23616/2" value="10pF"/>
<part name="L17" library="inductor" deviceset="FERRITE" device="_0603" value="1K @ 100MHz"/>
<part name="L18" library="inductor" deviceset="FERRITE" device="_0603" value="1K @ 100MHz"/>
<part name="L19" library="inductor" deviceset="L-US" device="_0805" value="390nH"/>
<part name="GND95" library="supply1" deviceset="GND" device=""/>
<part name="C55" library="intermet_library" deviceset="C-EU" device="C0603" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.001uF"/>
<part name="C56" library="intermet_library" deviceset="C-EU" device="C0603" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.001uF"/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L16" library="inductor" deviceset="FERRITE" device="_0603" value="1K @ 100MHz"/>
<part name="L20" library="inductor" deviceset="FERRITE" device="_0603" value="1K @ 100MHz"/>
<part name="R21" library="intermet_library" deviceset="742C083221JP" device="_220R" package3d_urn="urn:adsk.eagle:package:10198810/1"/>
<part name="R31" library="intermet_library" deviceset="742C083181JP" device="_180R" package3d_urn="urn:adsk.eagle:package:10198810/1"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="220.98" y="7.62" size="3.175" layer="94">120809</text>
<text x="269.24" y="7.62" size="3.175" layer="94">10</text>
<text x="220.98" y="199.39" size="2.54" layer="97" align="center">MANUFACTURING INTERFACE</text>
<text x="81.28" y="76.2" size="2.54" layer="97" align="center">BUCK-BOOST VOLTAGE REGULATOR</text>
<text x="43.18" y="63.5" size="1.27" layer="97" align="center">INPUT MINIMUM = 2.4 V
INPUT MAXIMUM = 7.0 V
MAXIMUM CURRENT = 1 A</text>
<text x="73.66" y="205.74" size="2.54" layer="97" align="center">BATTERY AND POWER ON CIRCUITRY</text>
<text x="200.66" y="147.32" size="2.54" layer="97">RSB Interface</text>
<text x="175.514" y="24.638" size="2.54" layer="94">InterMet Systems
4767 Broadmoor Ave SE #7
Grand Rapids, MI 49512</text>
</plain>
<instances>
<instance part="GND3" gate="1" x="27.94" y="142.24" smashed="yes">
<attribute name="VALUE" x="25.4" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="223.52" y="185.42" smashed="yes">
<attribute name="NAME" x="223.52" y="195.58" size="1.778" layer="95" rot="MR0" align="center"/>
<attribute name="VALUE" x="223.52" y="177.8" size="1.778" layer="96" rot="MR0" align="center"/>
</instance>
<instance part="GND1" gate="1" x="205.74" y="177.8" smashed="yes">
<attribute name="VALUE" x="203.2" y="175.26" size="1.778" layer="96"/>
</instance>
<instance part="SW1" gate="G$1" x="106.68" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="112.522" y="145.796" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="119.38" y="141.478" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="GND2" gate="1" x="116.84" y="160.02" smashed="yes">
<attribute name="VALUE" x="114.3" y="157.48" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="116.84" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="114.3" y="185.42" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="119.38" y="185.42" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R3" gate="G$1" x="116.84" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="114.3" y="170.18" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="119.38" y="170.18" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="3V1" gate="G$1" x="248.92" y="185.42" smashed="yes" rot="R270">
<attribute name="VALUE" x="251.46" y="185.42" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="185.42" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="259.08" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="190.5" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="Q1" gate="G$1" x="76.2" y="193.04" smashed="yes" rot="MR90">
<attribute name="VALUE" x="76.2" y="199.39" size="1.778" layer="96" rot="MR180" align="center"/>
<attribute name="NAME" x="76.2" y="201.93" size="1.778" layer="95" rot="MR180" align="center"/>
</instance>
<instance part="Q2" gate="G$1" x="71.12" y="142.24" smashed="yes">
<attribute name="NAME" x="71.12" y="149.86" size="1.778" layer="95" align="center-right"/>
<attribute name="VALUE" x="71.12" y="147.32" size="1.778" layer="96" align="center-right"/>
</instance>
<instance part="GND4" gate="1" x="73.66" y="132.08" smashed="yes">
<attribute name="VALUE" x="71.12" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="106.68" y="132.08" smashed="yes">
<attribute name="VALUE" x="104.14" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="58.42" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="55.88" y="182.88" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="60.96" y="182.88" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="U2" gate="G$1" x="78.74" y="40.64" smashed="yes">
<attribute name="NAME" x="66.04" y="62.23" size="1.778" layer="95"/>
<attribute name="VALUE" x="78.74" y="40.64" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="GND15" gate="1" x="60.96" y="15.24" smashed="yes">
<attribute name="VALUE" x="58.42" y="12.7" size="1.778" layer="96"/>
</instance>
<instance part="GND16" gate="1" x="96.52" y="15.24" smashed="yes">
<attribute name="VALUE" x="93.98" y="12.7" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="124.46" y="22.86" smashed="yes">
<attribute name="VALUE" x="121.92" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="33.02" y="48.26" smashed="yes">
<attribute name="NAME" x="27.94" y="48.26" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="27.94" y="45.72" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND13" gate="1" x="144.78" y="38.1" smashed="yes">
<attribute name="VALUE" x="142.24" y="35.56" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="101.6" y="38.1" smashed="yes">
<attribute name="NAME" x="101.6" y="40.64" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="101.6" y="35.56" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R6" gate="G$1" x="121.92" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="119.38" y="48.26" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="124.46" y="48.26" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R10" gate="G$1" x="124.46" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="121.92" y="30.48" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="127" y="30.48" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="C8" gate="G$1" x="111.76" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="113.03" y="41.91" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="113.03" y="34.29" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="R8" gate="G$1" x="129.54" y="40.64" smashed="yes">
<attribute name="NAME" x="129.54" y="43.18" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="129.54" y="38.1" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C7" gate="G$1" x="48.26" y="38.1" smashed="yes">
<attribute name="NAME" x="43.18" y="38.1" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="43.18" y="35.56" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R7" gate="G$1" x="48.26" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="45.72" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="50.8" y="45.72" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="GND12" gate="1" x="33.02" y="40.64" smashed="yes">
<attribute name="VALUE" x="30.48" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="144.78" y="58.42" smashed="yes">
<attribute name="VALUE" x="147.32" y="60.96" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D1" gate="G$1" x="96.52" y="165.1" smashed="yes">
<attribute name="NAME" x="96.52" y="171.196" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="96.52" y="159.385" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="C5" gate="G$1" x="137.16" y="48.26" smashed="yes">
<attribute name="NAME" x="132.08" y="48.26" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="132.08" y="45.72" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C6" gate="G$1" x="144.78" y="48.26" smashed="yes">
<attribute name="NAME" x="149.86" y="48.26" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="149.86" y="45.72" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R4" gate="G$1" x="60.96" y="142.24" smashed="yes">
<attribute name="NAME" x="60.96" y="144.78" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="60.96" y="139.7" size="1.778" layer="96" align="center"/>
</instance>
<instance part="L1" gate="G$1" x="76.2" y="68.58" smashed="yes">
<attribute name="NAME" x="76.2" y="72.39" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="76.2" y="66.04" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="BAT1" gate="G$1" x="27.94" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="24.765" y="173.99" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="BAT3" gate="G$1" x="27.94" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="24.765" y="153.67" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="J4" gate="-1" x="170.18" y="124.46" smashed="yes" rot="MR0">
<attribute name="NAME" x="167.64" y="123.698" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="170.942" y="125.857" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="J4" gate="-2" x="170.18" y="121.92" smashed="yes" rot="MR0">
<attribute name="NAME" x="167.64" y="121.158" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="J4" gate="-3" x="170.18" y="119.38" smashed="yes" rot="MR0">
<attribute name="NAME" x="167.64" y="118.618" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="J4" gate="-4" x="170.18" y="116.84" smashed="yes" rot="MR0">
<attribute name="NAME" x="167.64" y="116.078" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="GND6" gate="1" x="203.2" y="96.52" smashed="yes">
<attribute name="VALUE" x="200.66" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="104.14" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="104.14" y="25.4" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="106.68" y="25.4" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="L17" gate="G$1" x="187.96" y="137.16" smashed="yes">
<attribute name="NAME" x="187.96" y="142.24" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="187.96" y="134.62" size="1.778" layer="96" align="center"/>
</instance>
<instance part="L18" gate="G$1" x="187.96" y="124.46" smashed="yes">
<attribute name="NAME" x="187.96" y="129.54" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="187.96" y="121.92" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C55" gate="G$1" x="218.44" y="106.68" smashed="yes">
<attribute name="NAME" x="219.964" y="107.061" size="1.778" layer="95"/>
<attribute name="VALUE" x="219.964" y="101.981" size="1.778" layer="96"/>
</instance>
<instance part="C56" gate="G$1" x="236.22" y="106.68" smashed="yes">
<attribute name="NAME" x="237.744" y="107.061" size="1.778" layer="95"/>
<attribute name="VALUE" x="237.744" y="101.981" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="218.44" y="96.52" smashed="yes">
<attribute name="VALUE" x="215.9" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="236.22" y="96.52" smashed="yes">
<attribute name="VALUE" x="233.68" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="L16" gate="G$1" x="187.96" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="187.96" y="109.22" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="187.96" y="116.84" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="L20" gate="G$1" x="187.96" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="187.96" y="96.52" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="187.96" y="104.14" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="208.28" y1="182.88" x2="205.74" y2="182.88" width="0.1524" layer="91"/>
<wire x1="205.74" y1="182.88" x2="205.74" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="116.84" y1="162.56" x2="116.84" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="E"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="73.66" y1="137.16" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="1@A"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="106.68" y1="137.16" x2="106.68" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="144.78" y1="40.64" x2="144.78" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND@1"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="63.5" y1="33.02" x2="60.96" y2="33.02" width="0.1524" layer="91"/>
<wire x1="60.96" y1="33.02" x2="60.96" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND@3"/>
<wire x1="60.96" y1="30.48" x2="60.96" y2="27.94" width="0.1524" layer="91"/>
<wire x1="60.96" y1="27.94" x2="60.96" y2="25.4" width="0.1524" layer="91"/>
<wire x1="60.96" y1="25.4" x2="60.96" y2="22.86" width="0.1524" layer="91"/>
<wire x1="60.96" y1="22.86" x2="60.96" y2="17.78" width="0.1524" layer="91"/>
<wire x1="63.5" y1="30.48" x2="60.96" y2="30.48" width="0.1524" layer="91"/>
<junction x="60.96" y="30.48"/>
<pinref part="U2" gate="G$1" pin="GND@8"/>
<wire x1="63.5" y1="27.94" x2="60.96" y2="27.94" width="0.1524" layer="91"/>
<junction x="60.96" y="27.94"/>
<pinref part="U2" gate="G$1" pin="GND@9"/>
<wire x1="63.5" y1="25.4" x2="60.96" y2="25.4" width="0.1524" layer="91"/>
<junction x="60.96" y="25.4"/>
<pinref part="U2" gate="G$1" pin="GND@16"/>
<wire x1="63.5" y1="22.86" x2="60.96" y2="22.86" width="0.1524" layer="91"/>
<junction x="60.96" y="22.86"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="48.26" y1="33.02" x2="60.96" y2="33.02" width="0.1524" layer="91"/>
<junction x="60.96" y="33.02"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PGND1"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="93.98" y1="27.94" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<wire x1="96.52" y1="27.94" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="PGND2"/>
<wire x1="96.52" y1="25.4" x2="96.52" y2="22.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="22.86" x2="96.52" y2="17.78" width="0.1524" layer="91"/>
<wire x1="93.98" y1="25.4" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<junction x="96.52" y="25.4"/>
<pinref part="U2" gate="G$1" pin="FRAME"/>
<wire x1="93.98" y1="22.86" x2="96.52" y2="22.86" width="0.1524" layer="91"/>
<junction x="96.52" y="22.86"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="BAT3" gate="G$1" pin="M1"/>
<wire x1="27.94" y1="144.78" x2="27.94" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L17" gate="G$1" pin="2"/>
<wire x1="195.58" y1="137.16" x2="203.2" y2="137.16" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="203.2" y1="137.16" x2="203.2" y2="101.6" width="0.1524" layer="91"/>
<pinref part="L20" gate="G$1" pin="1"/>
<wire x1="203.2" y1="101.6" x2="203.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="195.58" y1="101.6" x2="203.2" y2="101.6" width="0.1524" layer="91"/>
<junction x="203.2" y="101.6"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="236.22" y1="99.06" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="218.44" y1="99.06" x2="218.44" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="SWCLK"/>
<wire x1="238.76" y1="182.88" x2="241.3" y2="182.88" width="0.1524" layer="91"/>
<label x="241.3" y="182.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="SWDIO"/>
<wire x1="238.76" y1="190.5" x2="241.3" y2="190.5" width="0.1524" layer="91"/>
<label x="241.3" y="190.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="1/3_VIN" class="0">
<segment>
<label x="121.92" y="177.8" size="1.27" layer="95" xref="yes"/>
<wire x1="116.84" y1="180.34" x2="116.84" y2="177.8" width="0.1524" layer="91"/>
<wire x1="116.84" y1="177.8" x2="116.84" y2="175.26" width="0.1524" layer="91"/>
<wire x1="116.84" y1="177.8" x2="121.92" y2="177.8" width="0.1524" layer="91"/>
<junction x="116.84" y="177.8"/>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BATTV" class="0">
<segment>
<wire x1="58.42" y1="193.04" x2="27.94" y2="193.04" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="58.42" y1="193.04" x2="68.58" y2="193.04" width="0.1524" layer="91"/>
<wire x1="58.42" y1="187.96" x2="58.42" y2="193.04" width="0.1524" layer="91"/>
<junction x="58.42" y="193.04"/>
<wire x1="27.94" y1="193.04" x2="20.32" y2="193.04" width="0.1524" layer="91"/>
<junction x="27.94" y="193.04"/>
<label x="20.32" y="193.04" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="BAT1" gate="G$1" pin="P1"/>
<wire x1="27.94" y1="185.42" x2="27.94" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GPIO"/>
<wire x1="238.76" y1="187.96" x2="241.3" y2="187.96" width="0.1524" layer="91"/>
<label x="241.3" y="187.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="UART1_RX" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="RX"/>
<wire x1="208.28" y1="187.96" x2="200.66" y2="187.96" width="0.1524" layer="91"/>
<label x="200.66" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<label x="243.84" y="124.46" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="218.44" y1="124.46" x2="243.84" y2="124.46" width="0.1524" layer="91"/>
<wire x1="218.44" y1="109.22" x2="218.44" y2="124.46" width="0.1524" layer="91"/>
<junction x="218.44" y="124.46"/>
<pinref part="L18" gate="G$1" pin="2"/>
<wire x1="195.58" y1="124.46" x2="218.44" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART1_TX" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="TX"/>
<wire x1="208.28" y1="185.42" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
<label x="200.66" y="185.42" size="1.27" layer="95" rot="R180" xref="yes"/>
<label x="243.84" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="236.22" y1="114.3" x2="243.84" y2="114.3" width="0.1524" layer="91"/>
<wire x1="236.22" y1="109.22" x2="236.22" y2="114.3" width="0.1524" layer="91"/>
<junction x="236.22" y="114.3"/>
<pinref part="L16" gate="G$1" pin="1"/>
<wire x1="195.58" y1="114.3" x2="236.22" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="VDD"/>
<pinref part="3V1" gate="G$1" pin="+3V3"/>
<wire x1="238.76" y1="185.42" x2="248.92" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="B"/>
<wire x1="68.58" y1="142.24" x2="66.04" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PWR_ON" class="0">
<segment>
<wire x1="55.88" y1="142.24" x2="53.34" y2="142.24" width="0.1524" layer="91"/>
<label x="53.34" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="73.66" y1="167.64" x2="73.66" y2="172.72" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="C"/>
<wire x1="73.66" y1="172.72" x2="73.66" y2="185.42" width="0.1524" layer="91"/>
<wire x1="73.66" y1="147.32" x2="73.66" y2="167.64" width="0.1524" layer="91"/>
<wire x1="73.66" y1="167.64" x2="91.44" y2="167.64" width="0.1524" layer="91"/>
<junction x="73.66" y="167.64"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="58.42" y1="177.8" x2="58.42" y2="172.72" width="0.1524" layer="91"/>
<wire x1="58.42" y1="172.72" x2="73.66" y2="172.72" width="0.1524" layer="91"/>
<junction x="73.66" y="172.72"/>
<pinref part="D1" gate="G$1" pin="A2"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="SW1" gate="G$1" pin="0@A"/>
<wire x1="106.68" y1="149.86" x2="106.68" y2="165.1" width="0.1524" layer="91"/>
<wire x1="106.68" y1="165.1" x2="101.6" y2="165.1" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
</segment>
</net>
<net name="PB_IN" class="0">
<segment>
<wire x1="91.44" y1="162.56" x2="86.36" y2="162.56" width="0.1524" layer="91"/>
<label x="86.36" y="162.56" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="D1" gate="G$1" pin="A1"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="PVIN"/>
<wire x1="48.26" y1="50.8" x2="48.26" y2="53.34" width="0.1524" layer="91"/>
<wire x1="48.26" y1="53.34" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VIN"/>
<wire x1="58.42" y1="53.34" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<wire x1="63.5" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<junction x="58.42" y="53.34"/>
<pinref part="U2" gate="G$1" pin="PWM"/>
<wire x1="63.5" y1="45.72" x2="58.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="58.42" y1="45.72" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<junction x="58.42" y="50.8"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="48.26" y1="53.34" x2="33.02" y2="53.34" width="0.1524" layer="91"/>
<wire x1="33.02" y1="53.34" x2="33.02" y2="50.8" width="0.1524" layer="91"/>
<junction x="48.26" y="53.34"/>
<junction x="33.02" y="53.34"/>
<wire x1="22.86" y1="53.34" x2="33.02" y2="53.34" width="0.1524" layer="91"/>
<label x="22.86" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="V+"/>
<wire x1="208.28" y1="190.5" x2="200.66" y2="190.5" width="0.1524" layer="91"/>
<label x="200.66" y="190.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="116.84" y1="193.04" x2="116.84" y2="190.5" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="83.82" y1="193.04" x2="116.84" y2="193.04" width="0.1524" layer="91"/>
<wire x1="116.84" y1="193.04" x2="121.92" y2="193.04" width="0.1524" layer="91"/>
<junction x="116.84" y="193.04"/>
<label x="121.92" y="193.04" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<wire x1="58.42" y1="68.58" x2="58.42" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SW1"/>
<wire x1="58.42" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="68.58" y1="68.58" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="99.06" y1="68.58" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SW2"/>
<wire x1="99.06" y1="58.42" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="83.82" y1="68.58" x2="99.06" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VC"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="93.98" y1="38.1" x2="96.52" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="101.6" y1="30.48" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<wire x1="93.98" y1="30.48" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
<junction x="93.98" y="38.1"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="106.68" y1="38.1" x2="109.22" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="137.16" y1="43.18" x2="137.16" y2="40.64" width="0.1524" layer="91"/>
<wire x1="137.16" y1="40.64" x2="134.62" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="RUN/SS"/>
<wire x1="48.26" y1="40.64" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<junction x="48.26" y="40.64"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VOUT"/>
<wire x1="93.98" y1="53.34" x2="121.92" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="121.92" y1="53.34" x2="137.16" y2="53.34" width="0.1524" layer="91"/>
<wire x1="137.16" y1="53.34" x2="137.16" y2="50.8" width="0.1524" layer="91"/>
<junction x="121.92" y="53.34"/>
<wire x1="137.16" y1="53.34" x2="144.78" y2="53.34" width="0.1524" layer="91"/>
<wire x1="144.78" y1="53.34" x2="144.78" y2="50.8" width="0.1524" layer="91"/>
<junction x="137.16" y="53.34"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="144.78" y1="53.34" x2="144.78" y2="55.88" width="0.1524" layer="91"/>
<junction x="144.78" y="53.34"/>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="C6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="BAT3" gate="G$1" pin="P1"/>
<pinref part="BAT1" gate="G$1" pin="M1"/>
<wire x1="27.94" y1="165.1" x2="27.94" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="109.22" y1="30.48" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="116.84" y1="38.1" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<wire x1="116.84" y1="40.64" x2="116.84" y2="43.18" width="0.1524" layer="91"/>
<wire x1="116.84" y1="43.18" x2="116.84" y2="45.72" width="0.1524" layer="91"/>
<wire x1="93.98" y1="45.72" x2="116.84" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="FB"/>
<junction x="116.84" y="38.1"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="121.92" y1="43.18" x2="116.84" y2="43.18" width="0.1524" layer="91"/>
<junction x="116.84" y="43.18"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="124.46" y1="40.64" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<junction x="116.84" y="40.64"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="124.46" y1="35.56" x2="124.46" y2="38.1" width="0.1524" layer="91"/>
<wire x1="124.46" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="38.1" x2="116.84" y2="38.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="30.48" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<junction x="119.38" y="38.1"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="J4" gate="-1" pin="S"/>
<wire x1="172.72" y1="124.46" x2="172.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="L17" gate="G$1" pin="1"/>
<wire x1="172.72" y1="137.16" x2="180.34" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="J4" gate="-4" pin="S"/>
<wire x1="172.72" y1="116.84" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
<pinref part="L20" gate="G$1" pin="2"/>
<wire x1="172.72" y1="101.6" x2="180.34" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="J4" gate="-3" pin="S"/>
<wire x1="172.72" y1="119.38" x2="177.8" y2="119.38" width="0.1524" layer="91"/>
<wire x1="177.8" y1="119.38" x2="177.8" y2="114.3" width="0.1524" layer="91"/>
<pinref part="L16" gate="G$1" pin="2"/>
<wire x1="177.8" y1="114.3" x2="180.34" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="J4" gate="-2" pin="S"/>
<wire x1="172.72" y1="121.92" x2="177.8" y2="121.92" width="0.1524" layer="91"/>
<wire x1="177.8" y1="121.92" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
<pinref part="L18" gate="G$1" pin="1"/>
<wire x1="177.8" y1="124.46" x2="180.34" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="34.29" y="62.23" size="1.27" layer="97" align="center">50 OHM @ 1575.42 MHz</text>
<text x="76.2" y="78.74" size="2.54" layer="97" align="center">GPS MODULE</text>
<text x="218.44" y="153.67" size="2.54" layer="97" align="center">PRESSURE SENSOR</text>
<text x="76.2" y="127" size="2.54" layer="97" align="center">AIR TEMPERATURE MEASURING CIRCUIT</text>
<text x="58.42" y="201.93" size="2.54" layer="97" align="center">PROBE CONNECTOR</text>
<text x="213.36" y="200.66" size="2.54" layer="97" align="center">DEFROST CIRCUIT</text>
<text x="220.98" y="7.62" size="3.175" layer="94">120809</text>
<text x="269.24" y="7.62" size="3.175" layer="94">10</text>
<text x="214.63" y="95.25" size="2.54" layer="97" align="center">LINEAR REGULATOR</text>
<text x="245.11" y="85.09" size="1.27" layer="97" align="center">INPUT MINIMUM = 3.1V
INPUT MAXIMUM = 5.5V
MAXIMUM CURRENT = 200mA</text>
<text x="229.87" y="46.99" size="1.778" layer="97">DNP U1 if noise is
not an issue</text>
<text x="175.26" y="24.13" size="2.54" layer="94">InterMet Systems
4767 Broadmoor Ave SE #7
Grand Rapids, MI 49512</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="185.42" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="259.08" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="190.5" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="U5" gate="1" x="76.2" y="50.8" smashed="yes">
<attribute name="NAME" x="55.88" y="72.39" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="55.88" y="29.21" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND27" gate="1" x="43.18" y="68.58" smashed="yes" rot="R270">
<attribute name="VALUE" x="40.64" y="71.12" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND31" gate="1" x="43.18" y="58.42" smashed="yes" rot="R270">
<attribute name="VALUE" x="40.64" y="60.96" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND28" gate="1" x="109.22" y="68.58" smashed="yes" rot="R90">
<attribute name="VALUE" x="111.76" y="66.04" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND33" gate="1" x="109.22" y="48.26" smashed="yes" rot="R90">
<attribute name="VALUE" x="111.76" y="45.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND32" gate="1" x="43.18" y="50.8" smashed="yes" rot="R270">
<attribute name="VALUE" x="40.64" y="53.34" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND36" gate="1" x="71.12" y="20.32" smashed="yes">
<attribute name="VALUE" x="68.58" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="GND37" gate="1" x="81.28" y="20.32" smashed="yes">
<attribute name="VALUE" x="78.74" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="137.16" y="55.88" smashed="yes">
<attribute name="NAME" x="132.08" y="55.88" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="132.08" y="53.34" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C13" gate="G$1" x="124.46" y="55.88" smashed="yes">
<attribute name="NAME" x="119.38" y="55.88" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="119.38" y="53.34" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND34" gate="1" x="124.46" y="48.26" smashed="yes">
<attribute name="VALUE" x="121.92" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND35" gate="1" x="137.16" y="48.26" smashed="yes">
<attribute name="VALUE" x="134.62" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="3V7" gate="G$1" x="137.16" y="60.96" smashed="yes">
<attribute name="VALUE" x="139.7" y="66.04" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R19" gate="G$1" x="33.02" y="40.64" smashed="yes">
<attribute name="NAME" x="33.02" y="43.18" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="33.02" y="38.1" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R20" gate="G$1" x="33.02" y="30.48" smashed="yes">
<attribute name="NAME" x="33.02" y="33.02" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="33.02" y="27.94" size="1.778" layer="96" align="center"/>
</instance>
<instance part="U4" gate="G$1" x="218.44" y="138.43" smashed="yes">
<attribute name="NAME" x="208.28" y="147.32" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="130.81" size="1.778" layer="96"/>
</instance>
<instance part="3V6" gate="G$1" x="193.04" y="146.05" smashed="yes">
<attribute name="VALUE" x="195.58" y="151.13" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C12" gate="G$1" x="193.04" y="138.43" smashed="yes">
<attribute name="NAME" x="187.96" y="138.43" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="187.96" y="135.89" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND29" gate="1" x="193.04" y="128.27" smashed="yes">
<attribute name="VALUE" x="190.5" y="125.73" size="1.778" layer="96"/>
</instance>
<instance part="GND30" gate="1" x="203.2" y="128.27" smashed="yes">
<attribute name="VALUE" x="200.66" y="125.73" size="1.778" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="33.02" y="176.53" smashed="yes">
<attribute name="NAME" x="25.4" y="195.58" size="1.778" layer="95" rot="MR0" align="center"/>
</instance>
<instance part="3V5" gate="G$1" x="121.92" y="176.53" smashed="yes">
<attribute name="VALUE" x="121.92" y="180.34" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="GND23" gate="1" x="45.72" y="156.21" smashed="yes">
<attribute name="VALUE" x="43.18" y="153.67" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="50.8" y="191.77" smashed="yes" rot="R90">
<attribute name="VALUE" x="53.34" y="189.23" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND18" gate="1" x="50.8" y="179.07" smashed="yes" rot="R90">
<attribute name="VALUE" x="53.34" y="176.53" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R17" gate="G$1" x="66.04" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="63.5" y="111.76" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="68.58" y="111.76" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="C11" gate="G$1" x="81.28" y="114.3" smashed="yes">
<attribute name="NAME" x="76.2" y="114.3" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="76.2" y="111.76" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND26" gate="1" x="81.28" y="104.14" smashed="yes">
<attribute name="VALUE" x="78.74" y="101.6" size="1.778" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="91.44" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="88.9" y="111.76" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="93.98" y="111.76" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="Q3" gate="G$1" x="205.74" y="180.34" smashed="yes" rot="MR0">
<attribute name="NAME" x="205.74" y="187.96" size="1.778" layer="95" rot="MR0" align="center-right"/>
<attribute name="VALUE" x="205.74" y="185.42" size="1.778" layer="96" rot="MR0" align="center-right"/>
</instance>
<instance part="GND25" gate="1" x="203.2" y="170.18" smashed="yes" rot="MR0">
<attribute name="VALUE" x="205.74" y="167.64" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="L2" gate="G$1" x="111.76" y="176.53" smashed="yes">
<attribute name="NAME" x="111.76" y="181.61" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="111.76" y="173.99" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C10" gate="G$1" x="104.14" y="171.45" smashed="yes">
<attribute name="NAME" x="109.22" y="171.45" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="109.22" y="168.91" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND22" gate="1" x="104.14" y="163.83" smashed="yes">
<attribute name="VALUE" x="101.6" y="161.29" size="1.778" layer="96"/>
</instance>
<instance part="C53" gate="G$1" x="55.88" y="163.83" smashed="yes">
<attribute name="NAME" x="50.8" y="163.83" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="50.8" y="161.29" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C54" gate="G$1" x="68.58" y="163.83" smashed="yes">
<attribute name="NAME" x="63.5" y="163.83" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="63.5" y="161.29" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND92" gate="1" x="55.88" y="156.21" smashed="yes">
<attribute name="VALUE" x="53.34" y="153.67" size="1.778" layer="96"/>
</instance>
<instance part="GND93" gate="1" x="68.58" y="156.21" smashed="yes">
<attribute name="VALUE" x="66.04" y="153.67" size="1.778" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="215.9" y="180.34" smashed="yes">
<attribute name="NAME" x="215.9" y="182.88" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="215.9" y="177.8" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND94" gate="1" x="17.78" y="166.37" smashed="yes">
<attribute name="VALUE" x="15.24" y="163.83" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="194.31" y="69.85" smashed="yes">
<attribute name="NAME" x="189.23" y="69.85" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="189.23" y="67.31" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C2" gate="G$1" x="237.49" y="69.85" smashed="yes">
<attribute name="NAME" x="232.41" y="69.85" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="232.41" y="67.31" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND8" gate="1" x="194.31" y="62.23" smashed="yes">
<attribute name="VALUE" x="191.77" y="59.69" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="237.49" y="62.23" smashed="yes">
<attribute name="VALUE" x="234.95" y="59.69" size="1.778" layer="96"/>
</instance>
<instance part="3V2" gate="G$1" x="237.49" y="74.93" smashed="yes">
<attribute name="VALUE" x="240.03" y="80.01" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U1" gate="G$1" x="214.63" y="69.85" smashed="yes">
<attribute name="NAME" x="214.63" y="80.01" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="214.63" y="77.47" size="1.778" layer="96" align="center"/>
</instance>
<instance part="P+1" gate="1" x="194.31" y="77.47" smashed="yes">
<attribute name="VALUE" x="196.85" y="80.01" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C3" gate="G$1" x="219.71" y="54.61" smashed="yes">
<attribute name="NAME" x="214.63" y="54.61" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="214.63" y="52.07" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND11" gate="1" x="219.71" y="44.45" smashed="yes">
<attribute name="VALUE" x="217.17" y="41.91" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="209.55" y="57.15" smashed="yes">
<attribute name="VALUE" x="207.01" y="54.61" size="1.778" layer="96"/>
</instance>
<instance part="L15" gate="G$1" x="214.63" y="87.63" smashed="yes">
<attribute name="NAME" x="214.63" y="91.44" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="214.63" y="85.09" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U5" gate="1" pin="GND@15"/>
<wire x1="50.8" y1="66.04" x2="48.26" y2="66.04" width="0.1524" layer="91"/>
<wire x1="48.26" y1="66.04" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="48.26" y1="68.58" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="GND@14"/>
<wire x1="50.8" y1="68.58" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
<junction x="48.26" y="68.58"/>
</segment>
<segment>
<pinref part="U5" gate="1" pin="GND@18"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="50.8" y1="58.42" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="GND@19"/>
<wire x1="48.26" y1="58.42" x2="45.72" y2="58.42" width="0.1524" layer="91"/>
<wire x1="50.8" y1="55.88" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<wire x1="48.26" y1="55.88" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<junction x="48.26" y="58.42"/>
</segment>
<segment>
<pinref part="U5" gate="1" pin="GND@21"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="50.8" y1="50.8" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="GND@22"/>
<wire x1="48.26" y1="50.8" x2="45.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="48.26" x2="48.26" y2="48.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="48.26" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<junction x="48.26" y="50.8"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="U5" gate="1" pin="GND@13"/>
<wire x1="106.68" y1="68.58" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="GND@10"/>
<wire x1="104.14" y1="68.58" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
<wire x1="101.6" y1="60.96" x2="104.14" y2="60.96" width="0.1524" layer="91"/>
<wire x1="104.14" y1="60.96" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
<junction x="104.14" y="68.58"/>
<pinref part="U5" gate="1" pin="GND@11"/>
<wire x1="104.14" y1="63.5" x2="104.14" y2="66.04" width="0.1524" layer="91"/>
<wire x1="104.14" y1="66.04" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
<wire x1="101.6" y1="63.5" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
<junction x="104.14" y="63.5"/>
<pinref part="U5" gate="1" pin="GND@12"/>
<wire x1="101.6" y1="66.04" x2="104.14" y2="66.04" width="0.1524" layer="91"/>
<junction x="104.14" y="66.04"/>
</segment>
<segment>
<pinref part="U5" gate="1" pin="GND@5"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="101.6" y1="48.26" x2="104.14" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="GND@4"/>
<wire x1="104.14" y1="48.26" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<wire x1="101.6" y1="45.72" x2="104.14" y2="45.72" width="0.1524" layer="91"/>
<wire x1="104.14" y1="45.72" x2="104.14" y2="48.26" width="0.1524" layer="91"/>
<junction x="104.14" y="48.26"/>
</segment>
<segment>
<pinref part="U5" gate="1" pin="GND@27"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="71.12" y1="25.4" x2="71.12" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="1" pin="GND@31"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="81.28" y1="25.4" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND35" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="193.04" y1="133.35" x2="193.04" y2="130.81" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="205.74" y1="138.43" x2="203.2" y2="138.43" width="0.1524" layer="91"/>
<wire x1="203.2" y1="138.43" x2="203.2" y2="135.89" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="U4" gate="G$1" pin="CSB"/>
<wire x1="203.2" y1="135.89" x2="203.2" y2="130.81" width="0.1524" layer="91"/>
<wire x1="205.74" y1="135.89" x2="203.2" y2="135.89" width="0.1524" layer="91"/>
<junction x="203.2" y="135.89"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="GND@11"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="43.18" y1="166.37" x2="45.72" y2="166.37" width="0.1524" layer="91"/>
<wire x1="45.72" y1="166.37" x2="45.72" y2="163.83" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="GND@12"/>
<wire x1="45.72" y1="163.83" x2="45.72" y2="158.75" width="0.1524" layer="91"/>
<wire x1="43.18" y1="163.83" x2="45.72" y2="163.83" width="0.1524" layer="91"/>
<junction x="45.72" y="163.83"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="GND@1"/>
<wire x1="43.18" y1="191.77" x2="45.72" y2="191.77" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="GND@2"/>
<wire x1="43.18" y1="189.23" x2="45.72" y2="189.23" width="0.1524" layer="91"/>
<wire x1="45.72" y1="189.23" x2="45.72" y2="191.77" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="45.72" y1="191.77" x2="48.26" y2="191.77" width="0.1524" layer="91"/>
<junction x="45.72" y="191.77"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="GND@6"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="43.18" y1="179.07" x2="48.26" y2="179.07" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="81.28" y1="109.22" x2="81.28" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="E"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="203.2" y1="175.26" x2="203.2" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="2"/>
<pinref part="GND92" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C54" gate="G$1" pin="2"/>
<pinref part="GND93" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="M1"/>
<pinref part="GND94" gate="1" pin="GND"/>
<wire x1="20.32" y1="176.53" x2="17.78" y2="176.53" width="0.1524" layer="91"/>
<wire x1="17.78" y1="176.53" x2="17.78" y2="171.45" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="M2"/>
<wire x1="17.78" y1="171.45" x2="17.78" y2="168.91" width="0.1524" layer="91"/>
<wire x1="20.32" y1="171.45" x2="17.78" y2="171.45" width="0.1524" layer="91"/>
<junction x="17.78" y="171.45"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="219.71" y1="46.99" x2="219.71" y2="49.53" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U5" gate="1" pin="VCC"/>
<wire x1="101.6" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<wire x1="114.3" y1="58.42" x2="124.46" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U5" gate="1" pin="V_BCKP"/>
<wire x1="124.46" y1="58.42" x2="137.16" y2="58.42" width="0.1524" layer="91"/>
<wire x1="101.6" y1="55.88" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<wire x1="114.3" y1="55.88" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<junction x="114.3" y="58.42"/>
<pinref part="C13" gate="G$1" pin="1"/>
<junction x="124.46" y="58.42"/>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="3V7" gate="G$1" pin="+3V3"/>
<wire x1="137.16" y1="58.42" x2="137.16" y2="60.96" width="0.1524" layer="91"/>
<junction x="137.16" y="58.42"/>
<pinref part="U5" gate="1" pin="VCC_IO"/>
<wire x1="101.6" y1="38.1" x2="114.3" y2="38.1" width="0.1524" layer="91"/>
<wire x1="114.3" y1="38.1" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<junction x="114.3" y="55.88"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="3V6" gate="G$1" pin="+3V3"/>
<wire x1="193.04" y1="146.05" x2="193.04" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VDD"/>
<wire x1="193.04" y1="143.51" x2="193.04" y2="140.97" width="0.1524" layer="91"/>
<wire x1="205.74" y1="143.51" x2="203.2" y2="143.51" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PS"/>
<wire x1="205.74" y1="140.97" x2="203.2" y2="140.97" width="0.1524" layer="91"/>
<wire x1="203.2" y1="140.97" x2="203.2" y2="143.51" width="0.1524" layer="91"/>
<wire x1="203.2" y1="143.51" x2="193.04" y2="143.51" width="0.1524" layer="91"/>
<junction x="203.2" y="143.51"/>
<junction x="193.04" y="143.51"/>
</segment>
<segment>
<pinref part="3V5" gate="G$1" pin="+3V3"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="121.92" y1="176.53" x2="119.38" y2="176.53" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="3V2" gate="G$1" pin="+3V3"/>
<wire x1="237.49" y1="72.39" x2="237.49" y2="74.93" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="OUT"/>
<wire x1="224.79" y1="72.39" x2="229.87" y2="72.39" width="0.1524" layer="91"/>
<junction x="237.49" y="72.39"/>
<pinref part="L15" gate="G$1" pin="2"/>
<wire x1="229.87" y1="72.39" x2="237.49" y2="72.39" width="0.1524" layer="91"/>
<wire x1="222.25" y1="87.63" x2="229.87" y2="87.63" width="0.1524" layer="91"/>
<wire x1="229.87" y1="87.63" x2="229.87" y2="72.39" width="0.1524" layer="91"/>
<junction x="229.87" y="72.39"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U5" gate="1" pin="RF_IN"/>
<wire x1="50.8" y1="60.96" x2="45.72" y2="60.96" width="1.27" layer="91"/>
<pinref part="U5" gate="1" pin="ANT"/>
<wire x1="50.8" y1="63.5" x2="45.72" y2="63.5" width="1.27" layer="91"/>
<wire x1="45.72" y1="63.5" x2="45.72" y2="60.96" width="1.27" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U5" gate="1" pin="TXD"/>
<wire x1="50.8" y1="40.64" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U5" gate="1" pin="RXD"/>
<wire x1="50.8" y1="38.1" x2="43.18" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="38.1" y1="30.48" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="43.18" y1="30.48" x2="43.18" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USART2_RX" class="0">
<segment>
<wire x1="27.94" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<label x="25.4" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R19" gate="G$1" pin="1"/>
</segment>
</net>
<net name="USART2_TX" class="0">
<segment>
<wire x1="27.94" y1="30.48" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
<label x="25.4" y="30.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="I2C1_SCL" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SCLK"/>
<wire x1="231.14" y1="143.51" x2="233.68" y2="143.51" width="0.1524" layer="91"/>
<label x="233.68" y="143.51" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="I2C1_SDA" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SDI/SDA"/>
<wire x1="231.14" y1="140.97" x2="233.68" y2="140.97" width="0.1524" layer="91"/>
<label x="233.68" y="140.97" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="I2C2_SDA" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="SDA"/>
<wire x1="43.18" y1="181.61" x2="55.88" y2="181.61" width="0.1524" layer="91"/>
<label x="73.66" y="181.61" size="1.27" layer="95" xref="yes"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="55.88" y1="181.61" x2="73.66" y2="181.61" width="0.1524" layer="91"/>
<wire x1="55.88" y1="166.37" x2="55.88" y2="181.61" width="0.1524" layer="91"/>
<junction x="55.88" y="181.61"/>
</segment>
</net>
<net name="I2C2_SCL" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="SCL"/>
<wire x1="43.18" y1="173.99" x2="68.58" y2="173.99" width="0.1524" layer="91"/>
<label x="73.66" y="173.99" size="1.27" layer="95" xref="yes"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="68.58" y1="173.99" x2="73.66" y2="173.99" width="0.1524" layer="91"/>
<wire x1="68.58" y1="166.37" x2="68.58" y2="173.99" width="0.1524" layer="91"/>
<junction x="68.58" y="173.99"/>
</segment>
</net>
<net name="PT100M" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="PT100-"/>
<wire x1="43.18" y1="168.91" x2="73.66" y2="168.91" width="0.1524" layer="91"/>
<label x="73.66" y="168.91" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="C"/>
<wire x1="203.2" y1="190.5" x2="203.2" y2="185.42" width="0.1524" layer="91"/>
<wire x1="203.2" y1="190.5" x2="198.12" y2="190.5" width="0.1524" layer="91"/>
<label x="198.12" y="190.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NTC_DRIVE" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="NTC-"/>
<wire x1="43.18" y1="171.45" x2="73.66" y2="171.45" width="0.1524" layer="91"/>
<label x="73.66" y="171.45" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="66.04" y1="106.68" x2="66.04" y2="101.6" width="0.1524" layer="91"/>
<wire x1="66.04" y1="101.6" x2="60.96" y2="101.6" width="0.1524" layer="91"/>
<label x="60.96" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NTC_COMMON" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="NTC+"/>
<wire x1="43.18" y1="184.15" x2="73.66" y2="184.15" width="0.1524" layer="91"/>
<label x="73.66" y="184.15" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="66.04" y1="116.84" x2="66.04" y2="119.38" width="0.1524" layer="91"/>
<wire x1="66.04" y1="119.38" x2="60.96" y2="119.38" width="0.1524" layer="91"/>
<label x="60.96" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="66.04" y1="119.38" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
<junction x="66.04" y="119.38"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="81.28" y1="119.38" x2="81.28" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="81.28" y1="119.38" x2="91.44" y2="119.38" width="0.1524" layer="91"/>
<wire x1="91.44" y1="119.38" x2="91.44" y2="116.84" width="0.1524" layer="91"/>
<junction x="81.28" y="119.38"/>
</segment>
</net>
<net name="NTC_REF" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="91.44" y1="106.68" x2="91.44" y2="101.6" width="0.1524" layer="91"/>
<label x="91.44" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="B"/>
<wire x1="210.82" y1="180.34" x2="208.28" y2="180.34" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DEFROST_ON" class="0">
<segment>
<wire x1="220.98" y1="180.34" x2="226.06" y2="180.34" width="0.1524" layer="91"/>
<label x="226.06" y="180.34" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="PT100+"/>
<wire x1="43.18" y1="186.69" x2="104.14" y2="186.69" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="VDD"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="43.18" y1="176.53" x2="104.14" y2="176.53" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="104.14" y1="176.53" x2="104.14" y2="173.99" width="0.1524" layer="91"/>
<junction x="104.14" y="176.53"/>
<wire x1="104.14" y1="186.69" x2="104.14" y2="176.53" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="194.31" y1="74.93" x2="194.31" y2="72.39" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<pinref part="U1" gate="G$1" pin="IN"/>
<wire x1="204.47" y1="72.39" x2="201.93" y2="72.39" width="0.1524" layer="91"/>
<junction x="194.31" y="72.39"/>
<wire x1="201.93" y1="72.39" x2="199.39" y2="72.39" width="0.1524" layer="91"/>
<wire x1="199.39" y1="72.39" x2="194.31" y2="72.39" width="0.1524" layer="91"/>
<junction x="199.39" y="72.39"/>
<wire x1="199.39" y1="72.39" x2="199.39" y2="87.63" width="0.1524" layer="91"/>
<pinref part="L15" gate="G$1" pin="1"/>
<wire x1="207.01" y1="87.63" x2="199.39" y2="87.63" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="EN"/>
<wire x1="204.47" y1="67.31" x2="201.93" y2="67.31" width="0.1524" layer="91"/>
<wire x1="201.93" y1="67.31" x2="201.93" y2="72.39" width="0.1524" layer="91"/>
<junction x="201.93" y="72.39"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="BYPASS"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="219.71" y1="59.69" x2="219.71" y2="57.15" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<wire x1="203.2" y1="82.55" x2="203.2" y2="40.64" width="0.508" layer="91" style="shortdash"/>
<wire x1="203.2" y1="40.64" x2="227.33" y2="40.64" width="0.508" layer="91" style="shortdash"/>
<wire x1="227.33" y1="40.64" x2="227.33" y2="82.55" width="0.508" layer="91" style="shortdash"/>
<wire x1="227.33" y1="82.55" x2="203.2" y2="82.55" width="0.508" layer="91" style="shortdash"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="132.08" y="53.34" size="2.54" layer="97" align="center">INDICATOR LAMPS</text>
<text x="68.58" y="38.1" size="2.54" layer="97" align="center">INTERNAL HEATERS</text>
<text x="203.2" y="88.9" size="2.54" layer="97" align="center">PRESSURE I2C BUS</text>
<text x="251.46" y="88.9" size="2.54" layer="97" align="center">PROBE I2C BUS</text>
<text x="59.69" y="111.76" size="1.27" layer="97" align="center">COMP1_INP</text>
<text x="129.54" y="67.31" size="1.27" layer="97" rot="R90" align="center">ADC_IN9/SDADC1_AIN5P</text>
<text x="139.7" y="72.39" size="1.27" layer="97" rot="R90" align="center">INPUT W/PULLUP</text>
<text x="218.44" y="7.62" size="3.175" layer="94">120809</text>
<text x="269.24" y="7.62" size="3.175" layer="94">10</text>
<text x="174.752" y="24.13" size="2.54" layer="94">InterMet Systems
4767 Broadmoor Ave SE #7
Grand Rapids, MI 49512</text>
</plain>
<instances>
<instance part="U6" gate="G$1" x="127" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="87.63" y="99.06" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="127" y="134.62" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="Y1" gate="G$1" x="25.4" y="132.08" smashed="yes">
<attribute name="NAME" x="25.4" y="138.43" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="25.4" y="135.89" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C21" gate="G$1" x="15.24" y="127" smashed="yes">
<attribute name="NAME" x="10.16" y="127" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="10.16" y="124.46" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C22" gate="G$1" x="35.56" y="127" smashed="yes">
<attribute name="NAME" x="40.64" y="127" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="40.64" y="124.46" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND47" gate="1" x="15.24" y="119.38" smashed="yes">
<attribute name="VALUE" x="12.7" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="GND50" gate="1" x="35.56" y="119.38" smashed="yes">
<attribute name="VALUE" x="33.02" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="GND49" gate="1" x="27.94" y="119.38" smashed="yes">
<attribute name="VALUE" x="25.4" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="GND48" gate="1" x="22.86" y="119.38" smashed="yes">
<attribute name="VALUE" x="20.32" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="48.26" y="137.16" smashed="yes">
<attribute name="NAME" x="48.26" y="139.7" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="48.26" y="134.62" size="1.778" layer="96" align="center"/>
</instance>
<instance part="L3" gate="G$1" x="40.64" y="185.42" smashed="yes">
<attribute name="NAME" x="40.64" y="190.5" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="40.64" y="182.88" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C15" gate="G$1" x="27.94" y="182.88" smashed="yes">
<attribute name="NAME" x="22.86" y="182.88" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="22.86" y="180.34" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C16" gate="G$1" x="55.88" y="182.88" smashed="yes">
<attribute name="NAME" x="50.8" y="182.88" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="50.8" y="180.34" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C17" gate="G$1" x="68.58" y="182.88" smashed="yes">
<attribute name="NAME" x="63.5" y="182.88" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="63.5" y="180.34" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND40" gate="1" x="27.94" y="175.26" smashed="yes">
<attribute name="VALUE" x="25.4" y="172.72" size="1.778" layer="96"/>
</instance>
<instance part="GND41" gate="1" x="55.88" y="175.26" smashed="yes">
<attribute name="VALUE" x="53.34" y="172.72" size="1.778" layer="96"/>
</instance>
<instance part="GND42" gate="1" x="68.58" y="175.26" smashed="yes">
<attribute name="VALUE" x="66.04" y="172.72" size="1.778" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="27.94" y="157.48" smashed="yes">
<attribute name="NAME" x="22.86" y="157.48" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="22.86" y="154.94" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C19" gate="G$1" x="40.64" y="157.48" smashed="yes">
<attribute name="NAME" x="35.56" y="157.48" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="35.56" y="154.94" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C20" gate="G$1" x="53.34" y="157.48" smashed="yes">
<attribute name="NAME" x="48.26" y="157.48" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="48.26" y="154.94" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND43" gate="1" x="27.94" y="149.86" smashed="yes">
<attribute name="VALUE" x="25.4" y="147.32" size="1.778" layer="96"/>
</instance>
<instance part="GND44" gate="1" x="40.64" y="149.86" smashed="yes">
<attribute name="VALUE" x="38.1" y="147.32" size="1.778" layer="96"/>
</instance>
<instance part="GND45" gate="1" x="53.34" y="149.86" smashed="yes">
<attribute name="VALUE" x="50.8" y="147.32" size="1.778" layer="96"/>
</instance>
<instance part="GND51" gate="1" x="149.86" y="88.9" smashed="yes">
<attribute name="VALUE" x="147.32" y="86.36" size="1.778" layer="96"/>
</instance>
<instance part="GND38" gate="1" x="104.14" y="180.34" smashed="yes" rot="R180">
<attribute name="VALUE" x="106.68" y="182.88" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND46" gate="1" x="81.28" y="127" smashed="yes" rot="R270">
<attribute name="VALUE" x="78.74" y="129.54" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="3V8" gate="G$1" x="27.94" y="190.5" smashed="yes">
<attribute name="VALUE" x="30.48" y="195.58" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="3V10" gate="G$1" x="27.94" y="165.1" smashed="yes">
<attribute name="VALUE" x="30.48" y="170.18" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="3V9" gate="G$1" x="99.06" y="182.88" smashed="yes">
<attribute name="VALUE" x="101.6" y="187.96" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="3V11" gate="G$1" x="119.38" y="91.44" smashed="yes" rot="R180">
<attribute name="VALUE" x="116.84" y="86.36" size="1.778" layer="96"/>
</instance>
<instance part="FRAME3" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="185.42" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="259.08" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="190.5" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="LED1" gate="G$1" x="114.3" y="10.16" smashed="yes">
<attribute name="NAME" x="109.22" y="10.16" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="116.84" y="10.16" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="LED2" gate="G$1" x="127" y="10.16" smashed="yes">
<attribute name="NAME" x="121.92" y="10.16" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="129.54" y="10.16" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="LED3" gate="G$1" x="139.7" y="10.16" smashed="yes">
<attribute name="NAME" x="134.62" y="10.16" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="142.24" y="10.16" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="LED4" gate="G$1" x="152.4" y="10.16" smashed="yes">
<attribute name="NAME" x="147.32" y="10.16" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="154.94" y="10.16" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="GND53" gate="1" x="114.3" y="5.08" smashed="yes">
<attribute name="VALUE" x="111.76" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="GND54" gate="1" x="127" y="5.08" smashed="yes">
<attribute name="VALUE" x="124.46" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="GND55" gate="1" x="139.7" y="5.08" smashed="yes">
<attribute name="VALUE" x="137.16" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="GND56" gate="1" x="152.4" y="5.08" smashed="yes">
<attribute name="VALUE" x="149.86" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="198.12" y="147.32" smashed="yes">
<attribute name="NAME" x="198.12" y="149.86" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="198.12" y="144.78" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R24" gate="G$1" x="198.12" y="137.16" smashed="yes">
<attribute name="NAME" x="198.12" y="139.7" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="198.12" y="134.62" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R25" gate="G$1" x="187.96" y="132.08" smashed="yes">
<attribute name="NAME" x="187.96" y="134.62" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="187.96" y="129.54" size="1.778" layer="96" align="center"/>
</instance>
<instance part="Q4" gate="G$1" x="45.72" y="35.56" smashed="yes">
<attribute name="NAME" x="45.72" y="43.18" size="1.778" layer="95" align="center-right"/>
<attribute name="VALUE" x="45.72" y="40.64" size="1.778" layer="96" align="center-right"/>
</instance>
<instance part="GND52" gate="1" x="48.26" y="22.86" smashed="yes">
<attribute name="VALUE" x="45.72" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="R26" gate="G$1" x="198.12" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="195.58" y="68.58" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="200.66" y="68.58" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R27" gate="G$1" x="208.28" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="205.74" y="68.58" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="210.82" y="68.58" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="3V12" gate="G$1" x="198.12" y="78.74" smashed="yes">
<attribute name="VALUE" x="200.66" y="83.82" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="3V13" gate="G$1" x="208.28" y="78.74" smashed="yes">
<attribute name="VALUE" x="210.82" y="83.82" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R28" gate="G$1" x="246.38" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="243.84" y="68.58" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="248.92" y="68.58" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R29" gate="G$1" x="256.54" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="254" y="68.58" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="259.08" y="68.58" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="3V14" gate="G$1" x="246.38" y="78.74" smashed="yes">
<attribute name="VALUE" x="248.92" y="83.82" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="3V15" gate="G$1" x="256.54" y="78.74" smashed="yes">
<attribute name="VALUE" x="259.08" y="83.82" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND39" gate="1" x="119.38" y="180.34" smashed="yes" rot="R180">
<attribute name="VALUE" x="121.92" y="182.88" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R30" gate="G$1" x="35.56" y="35.56" smashed="yes">
<attribute name="NAME" x="35.56" y="38.1" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="35.56" y="33.02" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R32" gate="G$1" x="48.26" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="50.8" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="50.8" y="50.8" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R34" gate="G$1" x="60.96" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="58.42" y="50.8" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="63.5" y="50.8" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R35" gate="G$1" x="73.66" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="50.8" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="76.2" y="50.8" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R21" gate="G$1" x="195.58" y="190.5" smashed="yes">
<attribute name="NAME" x="193.675" y="195.58" size="1.778" layer="95"/>
<attribute name="VALUE" x="193.04" y="181.102" size="1.778" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="134.62" y="27.94" smashed="yes" rot="R270">
<attribute name="NAME" x="139.7" y="29.845" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="102.362" y="27.94" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$15" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA10"/>
<wire x1="167.64" y1="137.16" x2="193.04" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA9"/>
<wire x1="182.88" y1="132.08" x2="167.64" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="1"/>
</segment>
</net>
<net name="UART1_RX" class="0">
<segment>
<wire x1="203.2" y1="137.16" x2="210.82" y2="137.16" width="0.1524" layer="91"/>
<label x="210.82" y="137.16" size="1.27" layer="95" xref="yes"/>
<pinref part="R24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="UART1_TX" class="0">
<segment>
<label x="210.82" y="132.08" size="1.27" layer="95" xref="yes"/>
<wire x1="210.82" y1="132.08" x2="193.04" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="GND47" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Y1" gate="G$1" pin="GND@4"/>
<wire x1="27.94" y1="127" x2="27.94" y2="121.92" width="0.1524" layer="91"/>
<pinref part="GND49" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Y1" gate="G$1" pin="GND@2"/>
<wire x1="22.86" y1="127" x2="22.86" y2="121.92" width="0.1524" layer="91"/>
<pinref part="GND48" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND40" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND41" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="GND42" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND43" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND44" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND45" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VSSSD"/>
<pinref part="GND51" gate="1" pin="GND"/>
<wire x1="149.86" y1="93.98" x2="149.86" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VSS_1"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="104.14" y1="177.8" x2="104.14" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VSSA/VREF-"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="83.82" y1="127" x2="86.36" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="C"/>
<pinref part="GND54" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="C"/>
<pinref part="GND55" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED4" gate="G$1" pin="C"/>
<pinref part="GND56" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="BOOT0"/>
<wire x1="119.38" y1="175.26" x2="119.38" y2="177.8" width="0.1524" layer="91"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="E"/>
<pinref part="GND52" gate="1" pin="GND"/>
<wire x1="48.26" y1="30.48" x2="48.26" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="HOT@3"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="30.48" y1="132.08" x2="35.56" y2="132.08" width="0.1524" layer="91"/>
<wire x1="35.56" y1="132.08" x2="35.56" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="35.56" y1="132.08" x2="35.56" y2="137.16" width="0.1524" layer="91"/>
<wire x1="35.56" y1="137.16" x2="43.18" y2="137.16" width="0.1524" layer="91"/>
<junction x="35.56" y="132.08"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PF1-OSC_OUT"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="86.36" y1="137.16" x2="53.34" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="HOT@1"/>
<wire x1="15.24" y1="132.08" x2="20.32" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="15.24" y1="129.54" x2="15.24" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="PF0-OSC_IN"/>
<wire x1="15.24" y1="132.08" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
<wire x1="15.24" y1="142.24" x2="86.36" y2="142.24" width="0.1524" layer="91"/>
<junction x="15.24" y="132.08"/>
</segment>
</net>
<net name="+3VA" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="2"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="48.26" y1="185.42" x2="55.88" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="55.88" y1="185.42" x2="68.58" y2="185.42" width="0.1524" layer="91"/>
<junction x="55.88" y="185.42"/>
<wire x1="68.58" y1="185.42" x2="73.66" y2="185.42" width="0.1524" layer="91"/>
<junction x="68.58" y="185.42"/>
<label x="73.66" y="185.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VDDSD"/>
<wire x1="154.94" y1="93.98" x2="154.94" y2="91.44" width="0.1524" layer="91"/>
<label x="154.94" y="91.44" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VREFSD+"/>
<wire x1="167.64" y1="106.68" x2="170.18" y2="106.68" width="0.1524" layer="91"/>
<label x="170.18" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VDDA/VREF+"/>
<wire x1="86.36" y1="121.92" x2="83.82" y2="121.92" width="0.1524" layer="91"/>
<label x="83.82" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="27.94" y1="185.42" x2="33.02" y2="185.42" width="0.1524" layer="91"/>
<wire x1="27.94" y1="190.5" x2="27.94" y2="185.42" width="0.1524" layer="91"/>
<junction x="27.94" y="185.42"/>
<pinref part="3V8" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="27.94" y1="165.1" x2="27.94" y2="160.02" width="0.1524" layer="91"/>
<junction x="27.94" y="160.02"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="27.94" y1="160.02" x2="40.64" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="40.64" y1="160.02" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<junction x="40.64" y="160.02"/>
<pinref part="3V10" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VDD_1"/>
<wire x1="99.06" y1="182.88" x2="99.06" y2="175.26" width="0.1524" layer="91"/>
<pinref part="3V9" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VDD_2"/>
<wire x1="119.38" y1="91.44" x2="119.38" y2="93.98" width="0.1524" layer="91"/>
<pinref part="3V11" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="3V12" gate="G$1" pin="+3V3"/>
<wire x1="198.12" y1="73.66" x2="198.12" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="3V13" gate="G$1" pin="+3V3"/>
<wire x1="208.28" y1="73.66" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="3V14" gate="G$1" pin="+3V3"/>
<wire x1="246.38" y1="73.66" x2="246.38" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<pinref part="3V15" gate="G$1" pin="+3V3"/>
<wire x1="256.54" y1="73.66" x2="256.54" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FREQ1_LED" class="0">
<segment>
<label x="114.3" y="38.1" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R31" gate="G$1" pin="4"/>
<wire x1="129.54" y1="33.02" x2="114.3" y2="33.02" width="0.1524" layer="91"/>
<wire x1="114.3" y1="33.02" x2="114.3" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PB14"/>
<wire x1="167.64" y1="111.76" x2="170.18" y2="111.76" width="0.1524" layer="91"/>
<label x="170.18" y="111.76" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="FREQ2_LED" class="0">
<segment>
<wire x1="127" y1="35.56" x2="127" y2="38.1" width="0.1524" layer="91"/>
<label x="127" y="38.1" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R31" gate="G$1" pin="3"/>
<wire x1="132.08" y1="33.02" x2="132.08" y2="35.56" width="0.1524" layer="91"/>
<wire x1="132.08" y1="35.56" x2="127" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PB15"/>
<wire x1="167.64" y1="116.84" x2="170.18" y2="116.84" width="0.1524" layer="91"/>
<label x="170.18" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="FREQ3_LED" class="0">
<segment>
<wire x1="139.7" y1="35.56" x2="139.7" y2="38.1" width="0.1524" layer="91"/>
<label x="139.7" y="38.1" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="134.62" y1="33.02" x2="134.62" y2="35.56" width="0.1524" layer="91"/>
<wire x1="134.62" y1="35.56" x2="139.7" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PD8"/>
<wire x1="167.64" y1="121.92" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
<label x="170.18" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="FREQ4_LED" class="0">
<segment>
<label x="152.4" y="38.1" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="137.16" y1="33.02" x2="152.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="152.4" y1="33.02" x2="152.4" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PA8"/>
<wire x1="167.64" y1="127" x2="170.18" y2="127" width="0.1524" layer="91"/>
<label x="170.18" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="USART2_RX" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA3"/>
<wire x1="99.06" y1="93.98" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<label x="99.06" y="91.44" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="USART2_TX" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA2"/>
<wire x1="86.36" y1="106.68" x2="83.82" y2="106.68" width="0.1524" layer="91"/>
<label x="83.82" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI1_SCK" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PB3"/>
<wire x1="144.78" y1="175.26" x2="144.78" y2="177.8" width="0.1524" layer="91"/>
<label x="144.78" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="182.88" y1="185.42" x2="180.34" y2="185.42" width="0.1524" layer="91"/>
<label x="180.34" y="185.42" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R21" gate="G$1" pin="3"/>
<wire x1="190.5" y1="187.96" x2="182.88" y2="187.96" width="0.1524" layer="91"/>
<wire x1="182.88" y1="187.96" x2="182.88" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI1_MISO" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PB4"/>
<wire x1="139.7" y1="175.26" x2="139.7" y2="177.8" width="0.1524" layer="91"/>
<label x="139.7" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="182.88" y1="193.04" x2="180.34" y2="193.04" width="0.1524" layer="91"/>
<label x="180.34" y="193.04" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="190.5" y1="190.5" x2="182.88" y2="190.5" width="0.1524" layer="91"/>
<wire x1="182.88" y1="190.5" x2="182.88" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI1_MOSI" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PB5"/>
<wire x1="134.62" y1="175.26" x2="134.62" y2="177.8" width="0.1524" layer="91"/>
<label x="134.62" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="180.34" y="200.66" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="190.5" y1="193.04" x2="187.96" y2="193.04" width="0.1524" layer="91"/>
<wire x1="187.96" y1="193.04" x2="187.96" y2="200.66" width="0.1524" layer="91"/>
<wire x1="187.96" y1="200.66" x2="180.34" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX_MOSI" class="0">
<segment>
<wire x1="213.36" y1="200.66" x2="215.9" y2="200.66" width="0.1524" layer="91"/>
<label x="215.9" y="200.66" size="1.27" layer="95" xref="yes"/>
<pinref part="R21" gate="G$1" pin="8"/>
<wire x1="203.2" y1="193.04" x2="205.74" y2="193.04" width="0.1524" layer="91"/>
<wire x1="205.74" y1="193.04" x2="205.74" y2="200.66" width="0.1524" layer="91"/>
<wire x1="205.74" y1="200.66" x2="213.36" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX_MISO" class="0">
<segment>
<label x="215.9" y="193.04" size="1.27" layer="95" xref="yes"/>
<pinref part="R21" gate="G$1" pin="7"/>
<wire x1="203.2" y1="190.5" x2="210.82" y2="190.5" width="0.1524" layer="91"/>
<wire x1="210.82" y1="190.5" x2="210.82" y2="193.04" width="0.1524" layer="91"/>
<wire x1="210.82" y1="193.04" x2="215.9" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX_SCK" class="0">
<segment>
<label x="215.9" y="185.42" size="1.27" layer="95" xref="yes"/>
<pinref part="R21" gate="G$1" pin="6"/>
<wire x1="203.2" y1="187.96" x2="210.82" y2="187.96" width="0.1524" layer="91"/>
<wire x1="210.82" y1="187.96" x2="210.82" y2="185.42" width="0.1524" layer="91"/>
<wire x1="210.82" y1="185.42" x2="215.9" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX_CSN" class="0">
<segment>
<wire x1="213.36" y1="177.8" x2="215.9" y2="177.8" width="0.1524" layer="91"/>
<label x="215.9" y="177.8" size="1.27" layer="95" xref="yes"/>
<pinref part="R21" gate="G$1" pin="5"/>
<wire x1="203.2" y1="185.42" x2="205.74" y2="185.42" width="0.1524" layer="91"/>
<wire x1="205.74" y1="185.42" x2="205.74" y2="177.8" width="0.1524" layer="91"/>
<wire x1="205.74" y1="177.8" x2="213.36" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI1_CSN" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA15"/>
<wire x1="149.86" y1="175.26" x2="149.86" y2="177.8" width="0.1524" layer="91"/>
<label x="149.86" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<label x="180.34" y="177.8" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R21" gate="G$1" pin="4"/>
<wire x1="190.5" y1="185.42" x2="187.96" y2="185.42" width="0.1524" layer="91"/>
<wire x1="187.96" y1="185.42" x2="187.96" y2="177.8" width="0.1524" layer="91"/>
<wire x1="187.96" y1="177.8" x2="180.34" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA12"/>
<wire x1="167.64" y1="147.32" x2="193.04" y2="147.32" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="MOD_DATA" class="0">
<segment>
<wire x1="203.2" y1="147.32" x2="210.82" y2="147.32" width="0.1524" layer="91"/>
<label x="210.82" y="147.32" size="1.27" layer="95" xref="yes"/>
<pinref part="R22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="NTC_COMMON" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA1"/>
<wire x1="86.36" y1="111.76" x2="83.82" y2="111.76" width="0.1524" layer="91"/>
<label x="83.82" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="B"/>
<wire x1="43.18" y1="35.56" x2="40.64" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<wire x1="48.26" y1="55.88" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
<wire x1="60.96" y1="55.88" x2="73.66" y2="55.88" width="0.1524" layer="91"/>
<wire x1="48.26" y1="55.88" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<label x="48.26" y="60.96" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R32" gate="G$1" pin="2"/>
<junction x="48.26" y="55.88"/>
<pinref part="R34" gate="G$1" pin="2"/>
<junction x="60.96" y="55.88"/>
<pinref part="R35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="HEATER_ON" class="0">
<segment>
<wire x1="30.48" y1="35.56" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<label x="25.4" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R30" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PE9"/>
<wire x1="144.78" y1="93.98" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<label x="144.78" y="91.44" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="I2C1_SCL" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="198.12" y1="63.5" x2="198.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="198.12" y1="60.96" x2="193.04" y2="60.96" width="0.1524" layer="91"/>
<label x="193.04" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PB8"/>
<wire x1="114.3" y1="175.26" x2="114.3" y2="182.88" width="0.1524" layer="91"/>
<label x="114.3" y="182.88" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="I2C1_SDA" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="208.28" y1="63.5" x2="208.28" y2="55.88" width="0.1524" layer="91"/>
<wire x1="208.28" y1="55.88" x2="193.04" y2="55.88" width="0.1524" layer="91"/>
<label x="193.04" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PB9"/>
<wire x1="109.22" y1="175.26" x2="109.22" y2="182.88" width="0.1524" layer="91"/>
<label x="109.22" y="182.88" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="I2C2_SCL" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="246.38" y1="63.5" x2="246.38" y2="60.96" width="0.1524" layer="91"/>
<wire x1="246.38" y1="60.96" x2="241.3" y2="60.96" width="0.1524" layer="91"/>
<label x="241.3" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PF6"/>
<wire x1="167.64" y1="157.48" x2="170.18" y2="157.48" width="0.1524" layer="91"/>
<label x="170.18" y="157.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="I2C2_SDA" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="256.54" y1="63.5" x2="256.54" y2="55.88" width="0.1524" layer="91"/>
<wire x1="256.54" y1="55.88" x2="241.3" y2="55.88" width="0.1524" layer="91"/>
<label x="241.3" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PF7"/>
<wire x1="167.64" y1="162.56" x2="170.18" y2="162.56" width="0.1524" layer="91"/>
<label x="170.18" y="162.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA13"/>
<wire x1="167.64" y1="152.4" x2="170.18" y2="152.4" width="0.1524" layer="91"/>
<label x="170.18" y="152.4" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA14"/>
<wire x1="154.94" y1="175.26" x2="154.94" y2="177.8" width="0.1524" layer="91"/>
<label x="154.94" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="1/3_VIN" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PB1"/>
<wire x1="129.54" y1="93.98" x2="129.54" y2="91.44" width="0.1524" layer="91"/>
<label x="129.54" y="91.44" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="PWR_ON" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PB2"/>
<wire x1="134.62" y1="93.98" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<label x="134.62" y="91.44" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="PB_IN" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PE8"/>
<wire x1="139.7" y1="93.98" x2="139.7" y2="91.44" width="0.1524" layer="91"/>
<label x="139.7" y="91.44" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="RFPA_G16" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PB7"/>
<wire x1="124.46" y1="175.26" x2="124.46" y2="177.8" width="0.1524" layer="91"/>
<label x="124.46" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="RFPA_G8" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PB6"/>
<wire x1="129.54" y1="175.26" x2="129.54" y2="177.8" width="0.1524" layer="91"/>
<label x="129.54" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="RFPA_VPD" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA11"/>
<wire x1="167.64" y1="142.24" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
<label x="170.18" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NTC_DRIVE" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA0"/>
<wire x1="86.36" y1="116.84" x2="83.82" y2="116.84" width="0.1524" layer="91"/>
<label x="83.82" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NTC_REF" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA4"/>
<wire x1="104.14" y1="93.98" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<label x="104.14" y="91.44" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="DEFROST_ON" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="PA5"/>
<wire x1="109.22" y1="93.98" x2="109.22" y2="91.44" width="0.1524" layer="91"/>
<label x="109.22" y="91.44" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="C"/>
<wire x1="73.66" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="60.96" y1="45.72" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<wire x1="48.26" y1="40.64" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
<junction x="48.26" y="45.72"/>
<pinref part="R34" gate="G$1" pin="1"/>
<junction x="60.96" y="45.72"/>
<pinref part="R35" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="5"/>
<wire x1="129.54" y1="20.32" x2="114.3" y2="20.32" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="A"/>
<wire x1="114.3" y1="20.32" x2="114.3" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="6"/>
<wire x1="132.08" y1="20.32" x2="132.08" y2="17.78" width="0.1524" layer="91"/>
<wire x1="132.08" y1="17.78" x2="127" y2="17.78" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="A"/>
<wire x1="127" y1="17.78" x2="127" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="7"/>
<wire x1="134.62" y1="20.32" x2="134.62" y2="17.78" width="0.1524" layer="91"/>
<wire x1="134.62" y1="17.78" x2="139.7" y2="17.78" width="0.1524" layer="91"/>
<pinref part="LED3" gate="G$1" pin="A"/>
<wire x1="139.7" y1="17.78" x2="139.7" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="8"/>
<wire x1="137.16" y1="20.32" x2="152.4" y2="20.32" width="0.1524" layer="91"/>
<pinref part="LED4" gate="G$1" pin="A"/>
<wire x1="152.4" y1="20.32" x2="152.4" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="60.96" y="76.2" size="2.54" layer="97" align="center">TRANSMITTER POWER DECOUPLING</text>
<text x="58.42" y="203.2" size="2.54" layer="97" align="center">TRANSMITTER IC</text>
<text x="152.4" y="167.64" size="1.905" layer="97" rot="R90" align="center">50 OHM @ 403 MHz</text>
<text x="200.66" y="86.36" size="2.54" layer="97" align="center">CHEBYSHEV LOW PASS FILTER</text>
<text x="162.56" y="91.44" size="1.905" layer="97" rot="R90" align="center">50 OHM @ 403 MHz</text>
<text x="241.3" y="91.44" size="1.905" layer="97" rot="R90" align="center">50 OHM @ 403 MHz</text>
<text x="243.84" y="193.04" size="2.54" layer="97" align="center">RF PROGRAMMABLE AMPLIFIER</text>
<text x="218.44" y="7.62" size="3.175" layer="94">120809</text>
<text x="269.24" y="7.62" size="3.175" layer="94">10</text>
<text x="174.244" y="24.13" size="2.54" layer="94">InterMet Systems
4767 Broadmoor Ave SE #7
Grand Rapids, MI 49512</text>
</plain>
<instances>
<instance part="U8" gate="G$1" x="55.88" y="152.4" smashed="yes">
<attribute name="NAME" x="41.91" y="176.53" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="55.88" y="152.4" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="U7" gate="G$1" x="208.28" y="154.94" smashed="yes">
<attribute name="NAME" x="195.58" y="176.53" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="207.01" y="154.94" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="C36" gate="G$1" x="27.94" y="137.16" smashed="yes">
<attribute name="NAME" x="22.86" y="137.16" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="22.86" y="134.62" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND72" gate="1" x="27.94" y="129.54" smashed="yes">
<attribute name="VALUE" x="25.4" y="127" size="1.778" layer="96"/>
</instance>
<instance part="GND60" gate="1" x="50.8" y="180.34" smashed="yes" rot="R180">
<attribute name="VALUE" x="53.34" y="182.88" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND61" gate="1" x="66.04" y="180.34" smashed="yes" rot="R180">
<attribute name="VALUE" x="68.58" y="182.88" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C46" gate="G$1" x="68.58" y="58.42" smashed="yes">
<attribute name="NAME" x="63.5" y="58.42" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="63.5" y="55.88" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C47" gate="G$1" x="78.74" y="58.42" smashed="yes">
<attribute name="NAME" x="73.66" y="58.42" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="73.66" y="55.88" size="1.778" layer="96" align="center"/>
</instance>
<instance part="L14" gate="G$1" x="53.34" y="63.5" smashed="yes">
<attribute name="NAME" x="53.34" y="68.58" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="53.34" y="60.96" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND84" gate="1" x="68.58" y="50.8" smashed="yes">
<attribute name="VALUE" x="66.04" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND85" gate="1" x="78.74" y="50.8" smashed="yes">
<attribute name="VALUE" x="76.2" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="C48" gate="G$1" x="43.18" y="38.1" smashed="yes">
<attribute name="NAME" x="38.1" y="38.1" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="38.1" y="35.56" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C49" gate="G$1" x="53.34" y="38.1" smashed="yes">
<attribute name="NAME" x="48.26" y="38.1" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="48.26" y="35.56" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C50" gate="G$1" x="63.5" y="38.1" smashed="yes">
<attribute name="NAME" x="58.42" y="38.1" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="58.42" y="35.56" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C51" gate="G$1" x="73.66" y="38.1" smashed="yes">
<attribute name="NAME" x="68.58" y="38.1" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="68.58" y="35.56" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND86" gate="1" x="43.18" y="30.48" smashed="yes">
<attribute name="VALUE" x="40.64" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="GND87" gate="1" x="53.34" y="30.48" smashed="yes">
<attribute name="VALUE" x="50.8" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="GND88" gate="1" x="63.5" y="30.48" smashed="yes">
<attribute name="VALUE" x="60.96" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="GND89" gate="1" x="73.66" y="30.48" smashed="yes">
<attribute name="VALUE" x="71.12" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="R37" gate="G$1" x="60.96" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="58.42" y="187.96" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="63.5" y="187.96" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="GND57" gate="1" x="60.96" y="195.58" smashed="yes" rot="R180">
<attribute name="VALUE" x="63.5" y="198.12" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="Y2" gate="G$1" x="60.96" y="104.14" smashed="yes">
<attribute name="NAME" x="60.96" y="110.49" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="60.96" y="107.95" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND78" gate="1" x="58.42" y="93.98" smashed="yes">
<attribute name="VALUE" x="55.88" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="GND79" gate="1" x="63.5" y="93.98" smashed="yes">
<attribute name="VALUE" x="60.96" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="C41" gate="G$1" x="50.8" y="101.6" smashed="yes">
<attribute name="NAME" x="45.72" y="101.6" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="45.72" y="99.06" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C42" gate="G$1" x="73.66" y="101.6" smashed="yes">
<attribute name="NAME" x="68.58" y="101.6" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="68.58" y="99.06" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND77" gate="1" x="50.8" y="93.98" smashed="yes">
<attribute name="VALUE" x="48.26" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="GND80" gate="1" x="73.66" y="93.98" smashed="yes">
<attribute name="VALUE" x="71.12" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="L10" gate="G$1" x="93.98" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="88.9" y="139.7" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="88.9" y="137.16" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C37" gate="G$1" x="93.98" y="129.54" smashed="yes">
<attribute name="NAME" x="88.9" y="129.54" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="88.9" y="127" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND74" gate="1" x="93.98" y="121.92" smashed="yes">
<attribute name="VALUE" x="91.44" y="119.38" size="1.778" layer="96"/>
</instance>
<instance part="C29" gate="G$1" x="93.98" y="157.48" smashed="yes">
<attribute name="NAME" x="88.9" y="157.48" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="88.9" y="154.94" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C31" gate="G$1" x="101.6" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="104.14" y="142.24" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="104.14" y="139.7" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="L5" gate="G$1" x="101.6" y="152.4" smashed="yes">
<attribute name="NAME" x="101.6" y="157.48" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="101.6" y="154.94" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="GND65" gate="1" x="93.98" y="162.56" smashed="yes" rot="R180">
<attribute name="VALUE" x="96.52" y="165.1" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L6" gate="G$1" x="119.38" y="152.4" smashed="yes">
<attribute name="NAME" x="119.38" y="156.21" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="119.38" y="149.86" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="L7" gate="G$1" x="134.62" y="152.4" smashed="yes">
<attribute name="NAME" x="134.62" y="156.21" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="134.62" y="149.86" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="C32" gate="G$1" x="127" y="147.32" smashed="yes">
<attribute name="NAME" x="121.92" y="147.32" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="121.92" y="144.78" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C33" gate="G$1" x="144.78" y="147.32" smashed="yes">
<attribute name="NAME" x="139.7" y="147.32" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="139.7" y="144.78" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND68" gate="1" x="127" y="139.7" smashed="yes">
<attribute name="VALUE" x="124.46" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND69" gate="1" x="144.78" y="139.7" smashed="yes">
<attribute name="VALUE" x="142.24" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND70" gate="1" x="160.02" y="139.7" smashed="yes">
<attribute name="VALUE" x="157.48" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="C30" gate="G$1" x="165.1" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="165.1" y="160.02" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="165.1" y="157.48" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="L8" gate="G$1" x="177.8" y="152.4" smashed="yes">
<attribute name="NAME" x="177.8" y="156.21" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="177.8" y="149.86" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="GND67" gate="1" x="185.42" y="157.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="182.88" y="160.02" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C26" gate="G$1" x="172.72" y="177.8" smashed="yes">
<attribute name="NAME" x="167.64" y="177.8" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="167.64" y="175.26" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C27" gate="G$1" x="185.42" y="177.8" smashed="yes">
<attribute name="NAME" x="180.34" y="177.8" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="180.34" y="175.26" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND63" gate="1" x="172.72" y="170.18" smashed="yes">
<attribute name="VALUE" x="170.18" y="167.64" size="1.778" layer="96"/>
</instance>
<instance part="GND64" gate="1" x="185.42" y="170.18" smashed="yes">
<attribute name="VALUE" x="182.88" y="167.64" size="1.778" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="172.72" y="200.66" smashed="yes">
<attribute name="NAME" x="172.72" y="203.2" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="172.72" y="198.12" size="1.778" layer="96" align="center"/>
</instance>
<instance part="L4" gate="G$1" x="210.82" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="207.01" y="190.5" size="1.778" layer="95" rot="R270" align="center"/>
<attribute name="VALUE" x="213.36" y="190.5" size="1.778" layer="96" rot="R270" align="center"/>
</instance>
<instance part="C23" gate="G$1" x="185.42" y="195.58" smashed="yes">
<attribute name="NAME" x="180.34" y="195.58" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="180.34" y="193.04" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C24" gate="G$1" x="198.12" y="195.58" smashed="yes">
<attribute name="NAME" x="193.04" y="195.58" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="193.04" y="193.04" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND58" gate="1" x="185.42" y="187.96" smashed="yes">
<attribute name="VALUE" x="182.88" y="185.42" size="1.778" layer="96"/>
</instance>
<instance part="GND59" gate="1" x="198.12" y="187.96" smashed="yes">
<attribute name="VALUE" x="195.58" y="185.42" size="1.778" layer="96"/>
</instance>
<instance part="L9" gate="G$1" x="238.76" y="147.32" smashed="yes">
<attribute name="NAME" x="238.76" y="152.4" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="238.76" y="149.86" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="L11" gate="G$1" x="231.14" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="227.33" y="137.16" size="1.778" layer="95" rot="R270" align="center"/>
<attribute name="VALUE" x="233.68" y="137.16" size="1.778" layer="96" rot="R270" align="center"/>
</instance>
<instance part="C38" gate="G$1" x="236.22" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="236.22" y="121.92" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="238.76" y="121.92" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="C40" gate="G$1" x="236.22" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="236.22" y="111.76" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="238.76" y="111.76" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="GND73" gate="1" x="243.84" y="127" smashed="yes" rot="R90">
<attribute name="VALUE" x="246.38" y="124.46" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND75" gate="1" x="243.84" y="116.84" smashed="yes" rot="R90">
<attribute name="VALUE" x="246.38" y="114.3" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C34" gate="G$1" x="254" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="255.27" y="153.67" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="255.27" y="151.13" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="C35" gate="G$1" x="248.92" y="142.24" smashed="yes">
<attribute name="NAME" x="243.84" y="142.24" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="243.84" y="139.7" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND71" gate="1" x="248.92" y="134.62" smashed="yes">
<attribute name="VALUE" x="246.38" y="132.08" size="1.778" layer="96"/>
</instance>
<instance part="C39" gate="G$1" x="205.74" y="121.92" smashed="yes">
<attribute name="NAME" x="200.66" y="121.92" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="200.66" y="119.38" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C28" gate="G$1" x="233.68" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="234.95" y="168.91" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="234.95" y="166.37" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="C25" gate="G$1" x="228.6" y="180.34" smashed="yes">
<attribute name="NAME" x="233.68" y="180.34" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="234.95" y="177.8" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND66" gate="1" x="241.3" y="162.56" smashed="yes" rot="R90">
<attribute name="VALUE" x="243.84" y="160.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND62" gate="1" x="228.6" y="172.72" smashed="yes">
<attribute name="VALUE" x="226.06" y="170.18" size="1.778" layer="96"/>
</instance>
<instance part="GND76" gate="1" x="205.74" y="114.3" smashed="yes">
<attribute name="VALUE" x="203.2" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="C43" gate="G$1" x="180.34" y="71.12" smashed="yes">
<attribute name="NAME" x="175.26" y="71.12" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="175.26" y="68.58" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C44" gate="G$1" x="200.66" y="71.12" smashed="yes">
<attribute name="NAME" x="195.58" y="71.12" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="195.58" y="68.58" size="1.778" layer="96" align="center"/>
</instance>
<instance part="C45" gate="G$1" x="223.52" y="71.12" smashed="yes">
<attribute name="NAME" x="218.44" y="71.12" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="218.44" y="68.58" size="1.778" layer="96" align="center"/>
</instance>
<instance part="L12" gate="G$1" x="190.5" y="76.2" smashed="yes">
<attribute name="NAME" x="190.5" y="80.01" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="190.5" y="73.66" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="L13" gate="G$1" x="210.82" y="76.2" smashed="yes">
<attribute name="NAME" x="210.82" y="80.01" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="210.82" y="73.66" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="GND81" gate="1" x="180.34" y="63.5" smashed="yes">
<attribute name="VALUE" x="177.8" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND82" gate="1" x="200.66" y="63.5" smashed="yes">
<attribute name="VALUE" x="198.12" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND83" gate="1" x="223.52" y="63.5" smashed="yes">
<attribute name="VALUE" x="220.98" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="ANT1" gate="G$1" x="256.54" y="76.2" smashed="yes">
<attribute name="NAME" x="256.54" y="83.82" size="1.778" layer="95" align="center"/>
</instance>
<instance part="3V17" gate="G$1" x="43.18" y="66.04" smashed="yes">
<attribute name="VALUE" x="45.72" y="71.12" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="3V16" gate="G$1" x="172.72" y="182.88" smashed="yes">
<attribute name="VALUE" x="175.26" y="187.96" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="FRAME4" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="185.42" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="259.08" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="190.5" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="R38" gate="G$1" x="160.02" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="157.48" y="147.32" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="162.56" y="147.32" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="P+3" gate="1" x="160.02" y="205.74" smashed="yes">
<attribute name="VALUE" x="162.56" y="208.28" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+4" gate="1" x="220.98" y="121.92" smashed="yes">
<attribute name="VALUE" x="223.52" y="124.46" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C52" gate="G$1" x="83.82" y="38.1" smashed="yes">
<attribute name="NAME" x="78.74" y="38.1" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="78.74" y="35.56" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND90" gate="1" x="83.82" y="30.48" smashed="yes">
<attribute name="VALUE" x="81.28" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="TP1" gate="G$1" x="266.7" y="78.74" smashed="yes">
<attribute name="NAME" x="266.7" y="83.82" size="1.778" layer="95" align="center"/>
</instance>
<instance part="GND91" gate="1" x="266.7" y="73.66" smashed="yes">
<attribute name="VALUE" x="264.16" y="71.12" size="1.778" layer="96"/>
</instance>
<instance part="L19" gate="G$1" x="241.3" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="237.49" y="66.04" size="1.778" layer="95" rot="R270" align="center"/>
<attribute name="VALUE" x="243.84" y="66.04" size="1.778" layer="96" rot="R270" align="center"/>
</instance>
<instance part="GND95" gate="1" x="241.3" y="55.88" smashed="yes">
<attribute name="VALUE" x="238.76" y="53.34" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C36" gate="G$1" pin="2"/>
<pinref part="GND72" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND@19"/>
<pinref part="GND60" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND@16"/>
<pinref part="GND61" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="2"/>
<pinref part="GND84" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="2"/>
<pinref part="GND85" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<pinref part="GND86" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="2"/>
<pinref part="GND87" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<pinref part="GND88" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="2"/>
<pinref part="GND89" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<pinref part="GND57" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Y2" gate="G$1" pin="GND@2"/>
<pinref part="GND78" gate="1" pin="GND"/>
<wire x1="58.42" y1="96.52" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Y2" gate="G$1" pin="GND@4"/>
<pinref part="GND79" gate="1" pin="GND"/>
<wire x1="63.5" y1="96.52" x2="63.5" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<pinref part="GND77" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C42" gate="G$1" pin="2"/>
<pinref part="GND80" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C37" gate="G$1" pin="2"/>
<pinref part="GND74" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="GND65" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND68" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="GND69" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="GND"/>
<pinref part="GND67" gate="1" pin="GND"/>
<wire x1="187.96" y1="157.48" x2="190.5" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND63" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="GND64" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="GND58" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="GND59" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C38" gate="G$1" pin="2"/>
<pinref part="GND73" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C40" gate="G$1" pin="2"/>
<pinref part="GND75" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="GND71" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND66" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="GND62" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="GND76" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="GND81" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C44" gate="G$1" pin="2"/>
<pinref part="GND82" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C45" gate="G$1" pin="2"/>
<pinref part="GND83" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND70" gate="1" pin="GND"/>
<pinref part="R38" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<pinref part="GND90" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="TP1" gate="G$1" pin="TP"/>
<pinref part="GND91" gate="1" pin="GND"/>
<wire x1="266.7" y1="78.74" x2="266.7" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L19" gate="G$1" pin="1"/>
<pinref part="GND95" gate="1" pin="GND"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="DCOUPL"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="38.1" y1="142.24" x2="27.94" y2="142.24" width="0.1524" layer="91"/>
<wire x1="27.94" y1="142.24" x2="27.94" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="43.18" y1="66.04" x2="43.18" y2="63.5" width="0.1524" layer="91"/>
<pinref part="L14" gate="G$1" pin="1"/>
<wire x1="45.72" y1="63.5" x2="43.18" y2="63.5" width="0.1524" layer="91"/>
<pinref part="3V17" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="172.72" y1="182.88" x2="172.72" y2="180.34" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="172.72" y1="180.34" x2="185.42" y2="180.34" width="0.1524" layer="91"/>
<junction x="172.72" y="180.34"/>
<wire x1="185.42" y1="180.34" x2="200.66" y2="180.34" width="0.1524" layer="91"/>
<junction x="185.42" y="180.34"/>
<pinref part="U7" gate="G$1" pin="VBIAS"/>
<wire x1="200.66" y1="180.34" x2="200.66" y2="177.8" width="0.1524" layer="91"/>
<pinref part="3V16" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="TX_MISO" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SO(GDO1)"/>
<wire x1="38.1" y1="157.48" x2="27.94" y2="157.48" width="0.1524" layer="91"/>
<label x="27.94" y="157.48" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX_SCK" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SCLK"/>
<wire x1="38.1" y1="162.56" x2="27.94" y2="162.56" width="0.1524" layer="91"/>
<label x="27.94" y="162.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOD_DATA" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="GDO0"/>
<wire x1="45.72" y1="127" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<wire x1="45.72" y1="121.92" x2="38.1" y2="121.92" width="0.1524" layer="91"/>
<label x="38.1" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX_CSN" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="CS_N"/>
<wire x1="50.8" y1="127" x2="50.8" y2="116.84" width="0.1524" layer="91"/>
<wire x1="50.8" y1="116.84" x2="38.1" y2="116.84" width="0.1524" layer="91"/>
<label x="38.1" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX_MOSI" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SI"/>
<wire x1="45.72" y1="177.8" x2="45.72" y2="180.34" width="0.1524" layer="91"/>
<wire x1="45.72" y1="180.34" x2="27.94" y2="180.34" width="0.1524" layer="91"/>
<label x="27.94" y="180.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX_VDD" class="0">
<segment>
<pinref part="L14" gate="G$1" pin="2"/>
<wire x1="60.96" y1="63.5" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="68.58" y1="63.5" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="78.74" y1="63.5" x2="78.74" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="68.58" y1="60.96" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
<junction x="68.58" y="63.5"/>
<label x="81.28" y="63.5" size="1.27" layer="95" xref="yes"/>
<wire x1="78.74" y1="63.5" x2="81.28" y2="63.5" width="0.1524" layer="91"/>
<junction x="78.74" y="63.5"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="73.66" y1="40.64" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<wire x1="73.66" y1="43.18" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="63.5" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="43.18" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<wire x1="43.18" y1="43.18" x2="38.1" y2="43.18" width="0.1524" layer="91"/>
<wire x1="43.18" y1="40.64" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<junction x="43.18" y="43.18"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="53.34" y1="40.64" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<junction x="53.34" y="43.18"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="63.5" y1="40.64" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<junction x="63.5" y="43.18"/>
<label x="38.1" y="43.18" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="73.66" y1="43.18" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="83.82" y1="43.18" x2="83.82" y2="40.64" width="0.1524" layer="91"/>
<junction x="73.66" y="43.18"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="AVDD@14"/>
<wire x1="73.66" y1="157.48" x2="78.74" y2="157.48" width="0.1524" layer="91"/>
<wire x1="78.74" y1="157.48" x2="78.74" y2="162.56" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="AVDD@15"/>
<wire x1="78.74" y1="162.56" x2="78.74" y2="167.64" width="0.1524" layer="91"/>
<wire x1="73.66" y1="162.56" x2="78.74" y2="162.56" width="0.1524" layer="91"/>
<junction x="78.74" y="162.56"/>
<label x="78.74" y="167.64" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="AVDD@11"/>
<wire x1="73.66" y1="142.24" x2="78.74" y2="142.24" width="0.1524" layer="91"/>
<label x="78.74" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="AVDD@9"/>
<wire x1="60.96" y1="127" x2="60.96" y2="124.46" width="0.1524" layer="91"/>
<label x="60.96" y="124.46" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="DVDD"/>
<wire x1="38.1" y1="147.32" x2="35.56" y2="147.32" width="0.1524" layer="91"/>
<label x="35.56" y="147.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="DGUARD"/>
<wire x1="55.88" y1="177.8" x2="55.88" y2="180.34" width="0.1524" layer="91"/>
<label x="55.88" y="180.34" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="RBIAS"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="60.96" y1="182.88" x2="60.96" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XOSC_Q1" class="0">
<segment>
<pinref part="Y2" gate="G$1" pin="HOT@1"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="55.88" y1="104.14" x2="50.8" y2="104.14" width="0.1524" layer="91"/>
<wire x1="50.8" y1="104.14" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
<wire x1="50.8" y1="114.3" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="XOSC_Q1"/>
<wire x1="55.88" y1="114.3" x2="55.88" y2="127" width="0.1524" layer="91"/>
<junction x="50.8" y="104.14"/>
</segment>
</net>
<net name="XOSC_Q2" class="0">
<segment>
<pinref part="Y2" gate="G$1" pin="HOT@3"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="66.04" y1="104.14" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
<wire x1="73.66" y1="104.14" x2="73.66" y2="114.3" width="0.1524" layer="91"/>
<wire x1="73.66" y1="114.3" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="XOSC_Q2"/>
<wire x1="66.04" y1="114.3" x2="66.04" y2="127" width="0.1524" layer="91"/>
<junction x="73.66" y="104.14"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="RF_P"/>
<pinref part="L10" gate="G$1" pin="2"/>
<wire x1="73.66" y1="147.32" x2="93.98" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="93.98" y1="147.32" x2="99.06" y2="147.32" width="0.1524" layer="91"/>
<junction x="93.98" y="147.32"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="L10" gate="G$1" pin="1"/>
<pinref part="C37" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="RF_N"/>
<wire x1="73.66" y1="152.4" x2="93.98" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="L5" gate="G$1" pin="1"/>
<junction x="93.98" y="152.4"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="106.68" y1="147.32" x2="111.76" y2="147.32" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="111.76" y1="147.32" x2="111.76" y2="152.4" width="0.1524" layer="91"/>
<wire x1="111.76" y1="152.4" x2="109.22" y2="152.4" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="1"/>
<junction x="111.76" y="152.4"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="2"/>
<pinref part="L7" gate="G$1" pin="1"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="127" y1="149.86" x2="127" y2="152.4" width="0.1524" layer="91"/>
<junction x="127" y="152.4"/>
</segment>
</net>
<net name="RF_IN" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="L7" gate="G$1" pin="2"/>
<wire x1="160.02" y1="152.4" x2="162.56" y2="152.4" width="0.1524" layer="91"/>
<wire x1="142.24" y1="152.4" x2="144.78" y2="152.4" width="0.1524" layer="91"/>
<wire x1="144.78" y1="152.4" x2="144.78" y2="149.86" width="0.1524" layer="91"/>
<wire x1="144.78" y1="152.4" x2="160.02" y2="152.4" width="1.27" layer="91"/>
<junction x="144.78" y="152.4"/>
<pinref part="R38" gate="G$1" pin="2"/>
<junction x="160.02" y="152.4"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="L8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="L8" gate="G$1" pin="2"/>
<pinref part="U7" gate="G$1" pin="RF_IN"/>
<wire x1="190.5" y1="152.4" x2="185.42" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="VCC1"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="210.82" y1="182.88" x2="210.82" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="177.8" y1="200.66" x2="185.42" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="185.42" y1="200.66" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<wire x1="185.42" y1="198.12" x2="185.42" y2="200.66" width="0.1524" layer="91"/>
<junction x="185.42" y="200.66"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="198.12" y1="198.12" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="210.82" y1="198.12" x2="210.82" y2="200.66" width="0.1524" layer="91"/>
<wire x1="210.82" y1="200.66" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<junction x="198.12" y="200.66"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="RF_OUT@9"/>
<pinref part="L9" gate="G$1" pin="1"/>
<wire x1="226.06" y1="147.32" x2="231.14" y2="147.32" width="0.1524" layer="91"/>
<pinref part="L11" gate="G$1" pin="2"/>
<wire x1="231.14" y1="144.78" x2="231.14" y2="147.32" width="0.1524" layer="91"/>
<junction x="231.14" y="147.32"/>
<pinref part="U7" gate="G$1" pin="RF_OUT@10"/>
<wire x1="226.06" y1="152.4" x2="231.14" y2="152.4" width="0.1524" layer="91"/>
<wire x1="231.14" y1="152.4" x2="231.14" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RF_OUT" class="0">
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="259.08" y1="147.32" x2="261.62" y2="147.32" width="0.1524" layer="91"/>
<label x="261.62" y="147.32" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="1"/>
<pinref part="L12" gate="G$1" pin="1"/>
<wire x1="180.34" y1="73.66" x2="180.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="180.34" y1="76.2" x2="182.88" y2="76.2" width="0.1524" layer="91"/>
<wire x1="180.34" y1="76.2" x2="149.86" y2="76.2" width="1.27" layer="91"/>
<junction x="180.34" y="76.2"/>
<label x="149.86" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RFPA_G16" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="G16"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="215.9" y1="177.8" x2="215.9" y2="182.88" width="0.1524" layer="91"/>
<wire x1="215.9" y1="182.88" x2="228.6" y2="182.88" width="0.1524" layer="91"/>
<label x="238.76" y="182.88" size="1.27" layer="95" xref="yes"/>
<wire x1="228.6" y1="182.88" x2="238.76" y2="182.88" width="0.1524" layer="91"/>
<junction x="228.6" y="182.88"/>
</segment>
</net>
<net name="RFPA_G8" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="G8"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="226.06" y1="162.56" x2="228.6" y2="162.56" width="0.1524" layer="91"/>
<label x="228.6" y="157.48" size="1.27" layer="95" xref="yes"/>
<wire x1="228.6" y1="162.56" x2="231.14" y2="162.56" width="0.1524" layer="91"/>
<wire x1="228.6" y1="162.56" x2="228.6" y2="157.48" width="0.1524" layer="91"/>
<junction x="228.6" y="162.56"/>
</segment>
</net>
<net name="RFPA_VPD" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="VPD"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="205.74" y1="132.08" x2="205.74" y2="127" width="0.1524" layer="91"/>
<label x="200.66" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="205.74" y1="127" x2="205.74" y2="124.46" width="0.1524" layer="91"/>
<wire x1="205.74" y1="127" x2="200.66" y2="127" width="0.1524" layer="91"/>
<junction x="205.74" y="127"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="L9" gate="G$1" pin="2"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="246.38" y1="147.32" x2="248.92" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="248.92" y1="147.32" x2="251.46" y2="147.32" width="0.1524" layer="91"/>
<wire x1="248.92" y1="144.78" x2="248.92" y2="147.32" width="0.1524" layer="91"/>
<junction x="248.92" y="147.32"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="L12" gate="G$1" pin="2"/>
<pinref part="L13" gate="G$1" pin="1"/>
<wire x1="198.12" y1="76.2" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="200.66" y1="76.2" x2="203.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="200.66" y1="73.66" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
<junction x="200.66" y="76.2"/>
</segment>
</net>
<net name="ANT_IN" class="0">
<segment>
<pinref part="L13" gate="G$1" pin="2"/>
<wire x1="218.44" y1="76.2" x2="223.52" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="223.52" y1="76.2" x2="223.52" y2="73.66" width="0.1524" layer="91"/>
<junction x="223.52" y="76.2"/>
<pinref part="ANT1" gate="G$1" pin="ANTENNA"/>
<wire x1="223.52" y1="76.2" x2="241.3" y2="76.2" width="1.27" layer="91"/>
<pinref part="L19" gate="G$1" pin="2"/>
<wire x1="241.3" y1="76.2" x2="256.54" y2="76.2" width="1.27" layer="91"/>
<wire x1="241.3" y1="73.66" x2="241.3" y2="76.2" width="0.1524" layer="91"/>
<junction x="241.3" y="76.2"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="160.02" y1="203.2" x2="160.02" y2="200.66" width="0.1524" layer="91"/>
<wire x1="160.02" y1="200.66" x2="167.64" y2="200.66" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="L11" gate="G$1" pin="1"/>
<wire x1="231.14" y1="129.54" x2="231.14" y2="127" width="0.1524" layer="91"/>
<wire x1="231.14" y1="127" x2="231.14" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="233.68" y1="127" x2="231.14" y2="127" width="0.1524" layer="91"/>
<junction x="231.14" y="127"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="233.68" y1="116.84" x2="231.14" y2="116.84" width="0.1524" layer="91"/>
<wire x1="220.98" y1="119.38" x2="220.98" y2="116.84" width="0.1524" layer="91"/>
<wire x1="220.98" y1="116.84" x2="231.14" y2="116.84" width="0.1524" layer="91"/>
<junction x="231.14" y="116.84"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="202,4,38.1,152.4,U8,GDO2,,,,"/>
<approved hash="104,4,38.1,147.32,U8,DVDD,TX_VDD,,,"/>
<approved hash="104,4,38.1,142.24,U8,DCOUPL,N$2,,,"/>
<approved hash="104,4,60.96,127,U8,AVDD,TX_VDD,,,"/>
<approved hash="104,4,210.82,177.8,U7,VCC1,N$12,,,"/>
<approved hash="204,3,86.36,162.56,U6,VBAT,,,,"/>
<approved hash="104,3,86.36,127,U6,VSSA/VREF-,GND,,,"/>
<approved hash="104,3,86.36,121.92,U6,VDDA/VREF+,+3VA,,,"/>
<approved hash="104,3,119.38,93.98,U6,VDD_2,+3V3,,,"/>
<approved hash="104,3,149.86,93.98,U6,VSSSD,GND,,,"/>
<approved hash="104,3,154.94,93.98,U6,VDDSD,+3VA,,,"/>
<approved hash="104,3,167.64,106.68,U6,VREFSD+,+3VA,,,"/>
<approved hash="104,3,104.14,175.26,U6,VSS_1,GND,,,"/>
<approved hash="104,3,99.06,175.26,U6,VDD_1,+3V3,,,"/>
<approved hash="104,2,101.6,38.1,U5,VCC_IO,+3V3,,,"/>
<approved hash="202,2,101.6,53.34,U5,EXTINT,,,,"/>
<approved hash="104,2,101.6,55.88,U5,V_BCKP,+3V3,,,"/>
<approved hash="104,2,101.6,58.42,U5,VCC,+3V3,,,"/>
<approved hash="104,1,93.98,38.1,U2,VC,N$40,,,"/>
<approved hash="104,1,63.5,53.34,U2,PVIN,VIN,,,"/>
<approved hash="104,1,93.98,53.34,U2,VOUT,+5V,,,"/>
<approved hash="104,1,93.98,27.94,U2,PGND1,GND,,,"/>
<approved hash="104,1,93.98,25.4,U2,PGND2,GND,,,"/>
<approved hash="104,1,93.98,22.86,U2,FRAME,GND,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
